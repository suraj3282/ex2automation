# Steps For Setting Up Appium On Machine

### Set up android sdk on machine
1. Download Android SDK zip file from [Android SDK](http://developer.android.com/sdk/index.html)
2. Unzip the downloaded SDk and place the android-sdk-windows folder at any location on your local system (suppose D:\AndroidSdk\android-sdk-windows)
3. Set a new System Environmental Variable (Right click Computer icon and go to Properties > Advanced System settings) with the name "ANDROID_HOME" and set it's value to the root folder of Android SDK (D:\AndroidSdk\sdk) - this would be required for launching the Selendroid Server
4. Modify the existing Environmental Variable "Path" by adding the ADB path present in the platform-tools folder (ADB is Android Debug Bridge - for enabling connection between your system and the Android Device/Emulator) - in the above case, path would be (D:\AndroidSdk\sdk\platform-tools)

### Setting up Appium
1. Download appium zip from [Appium.io](https://bitbucket.org/appium/appium.app/downloads/AppiumForWindows-1.3.4.1.zip)
2. Unzip the downloaded file and place it in a location on your system
3. Set a new System Enviroment Variable with name "APPIUM_HOME" and set it's value to appium folder. e.g. C:/Appium
![alt text](	 "Setting up appium home in system varables")
	Refer Appium_home.png
	
### Steps for running appium test cases 

#### Using emulator
1. Start AVD Manager.exe in android sdk folder
2. Create a new device with settings such as shown in emulator.png
3. Click on start button and wait for emulator to start

#### Using real device
1. Activate developers option and enable USB debugging option in it
2. Connect device using USB
3. Check if device is connected by running command ADB devices in command line

### Steps for writing test cases for mobile application

1.Annonate test class with Test Variable. e.g.
```
@TestVariables(environment = "Test", applicationName = "PayuMoneyApp")
```

where environment is Test and applicationName is name of application.
A config file by name of applicationName needs to be present to pick up properties for starting application such as 
* APK name 
* Activity name
* Application package

e.g. PayuMoneyApp.config file is present for picking up properties for PayuMoney app.

2. Use MobileAction class for performing actions such as click,tap and zoom. e.g.
```
MobileActions.enterData(testConfig, oldPasswordInput, oldPassword, "Old Password");
MobileActions.enterData(testConfig, newPasswordInput, newPassword, "New Password");
MobileActions.enterData(testConfig, confirmPassword, ConfirmPassword, "Confirm Password");
MobileActions.click(testConfig, updatePasswordButton, "Update Password Button");
```is