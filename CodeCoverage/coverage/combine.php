<?php
	include_once("vendor/autoload.php");
	
	$coverages = glob("coverages/*.php");

    #increase the memory in multiples of 128M in case of memory error
    ini_set('memory_limit', '12800M');

	$final_coverage = new PHP_CodeCoverage;
	$count = count($coverages);
	$i = 0;
	foreach ($coverages as $coverage_file)
	{
		$i++;
		echo "Processing coverage ($i/$count) from $coverage_file". PHP_EOL;
		require_once($coverage_file);
		$final_coverage->merge($coverage);
	}

    #add the directories where source code files exists
    $folders = array("apps", "batch", "common", "lib", "scripts", "web", "tests");
    foreach ($folders as $folder)
	{
	    $final_coverage->filter()->addDirectoryToWhitelist("/usr/local/apache2/htdocs/payu/$folder");
	}
    echo "Generating final report..." . PHP_EOL;
    $report = new PHP_CodeCoverage_Report_HTML();
    $report->process($final_coverage,"reports");
    echo "Report generated succesfully". PHP_EOL;
?>



