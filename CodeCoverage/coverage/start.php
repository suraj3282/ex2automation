<?php
require_once "vendor/autoload.php";
define("COVERAGE_CAPTURE", 1);
define("COVERAGE_CAPTURE_END", __DIR__ . "/end.php");

$coverage = new PHP_CodeCoverage;
$filter   = $coverage->filter();

$filter->addFileToBlacklist(__DIR__ . '/start.php');
$filter->addFileToBlacklist(__DIR__ . '/end.php');
$filter->addDirectoryToBlacklist(__DIR__);


$test_name = (isset($_COOKIE['test_name']) && !empty($_COOKIE['test_name'])) ? $_COOKIE['test_name'] : 'unknown_test_' . time();
$coverage->start($test_name);

//file_put_contents(__DIR__ . "/coverages/{$test_name}.start", "");

function end_coverage()
{
    global $test_name;
    global $coverage;
    global $filter;
    
    $coverageName = __DIR__ . '/coverages/coverage-' . $test_name . '-' . microtime(true);
    //file_put_contents($coverageName . '.stop', "");
    
    try
    {
        $coverage->stop();
        $filter->removeDirectoryFromBlacklist(__DIR__);
        $filter->removeFileFromBlacklist(__DIR__ . "/start.php");
        $filter->removeFileFromBlacklist(__DIR__ . "/end.php");
        
        //$writer = new PHP_CodeCoverage_Report_Clover();
        //$writer->process($coverage,__DIR__ .'/report/clover.xml');
        //$writer = new PHP_CodeCoverage_Report_HTML;
        //$writer->process($coverage, __DIR__ . '/report/');
        $writer = new PHP_CodeCoverage_Report_PHP();
        //$coverageName =  __DIR__ .'/coverages/coverage-'. $test_name . '-' . microtime(true);
        $writer->process($coverage, $coverageName . '.php');
        //$log = __FILE__ .PHP_EOL . __DIR__ . PHP_EOL . var_export($coverage->getData(True),True);
        //file_put_contents($coverageName . '.corlog', $log);
        //file_put_contents($coverageName . '.ser', serialize($coverage));
    }
    catch (Exception $ex)
    {
        file_put_contents($coverageName . '.ex', $ex);
    }
}

class coverage_dumper
{
	function __destruct()
	{
		end_coverage();
	}
}

$_coverage_dumper = new coverage_dumper();