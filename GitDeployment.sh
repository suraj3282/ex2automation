gitBranchCommon=$1
gitBranchMoney=$2
gitBranchProduct=$3
cd ../Common
git stash
git gc --prune=now
git remote prune origin
git fetch origin
git checkout origin/$gitBranchCommon
git pull origin $gitBranchCommon
cd ../Money
git stash
git gc --prune=now
git remote prune origin
git fetch origin
git checkout origin/$gitBranchMoney
git pull origin $gitBranchMoney
cd ../Product
git stash
git gc --prune=now
git remote prune origin
git fetch origin
git checkout origin/$gitBranchProduct
git pull origin $gitBranchProduct