cd ..
cd Common
git gc --prune=now & git remote prune origin & mvn install:install-file -Dfile=./lib/serialized-php-parser.jar -DgroupId=org.lorecraft.phparser -DartifactId=serialized-php-parser -Dversion=0.4.5 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/harlib-jackson-1.1.2.jar -DgroupId=edu.umass.cs.benchlab.har -DartifactId=harlib-jackson -Dversion=1.1.2 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/aes.jar -DgroupId=aes -DartifactId=aes -Dversion=0.0.1 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/test.jar -DgroupId=test -DartifactId=test -Dversion=0.0.1 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/DB.jar -DgroupId=DB -DartifactId=DB -Dversion=0.0.1 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/Product-1.0.0.jar -DgroupId=Product -DartifactId=Product -Dversion=2.0.0 -Dpackaging=jar -DdownloadSources=false
mvn install:install-file -Dfile=./lib/Money-1.0.0.jar -DgroupId=Money -DartifactId=Money -Dversion=1.0.0 -Dpackaging=jar -DdownloadSources=false
mvn install:install-file -Dfile=./lib/AccuracyDB.jar -DgroupId=AccuracyDB -DartifactId=AccuracyDB -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/ImageComparison-1.0.jar -DgroupId=mukesh.rajput -DartifactId=ImageComparison -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/AutoItX4Java.jar -DgroupId=AutoItX4Java -DartifactId=AutoItX4 -Dversion=1.0 -Dpackaging=jar
mvn install:install-file -Dfile=./lib/jacob.jar -DgroupId=jacob -DartifactId=Jacob -Dversion=1.0 -Dpackaging=jar
mvn -Dmaven.test.skip=true clean install
cd ..
cd Money
mvn -Dmaven.test.skip=true clean install
cd ..
cd Product
mvn -Dmaven.test.skip=true clean install
cp ~/.m2/repository/Product/Product/1.0.0/Product-1.0.0.jar ../Common/lib/Product-1.0.0.jar
cd ..
cd Money
mvn -Dmaven.test.skip=true clean install
cp ~/.m2/repository/Money/Money/1.0.0/Money-1.0.0.jar ../Common/lib/Money-1.0.0.jar