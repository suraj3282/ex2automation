1. Make folder in your user directory. Run following command on cmd : mkdir .hudson

2. Set JENKINS_HOME in user environment variables and assign it value C:\Users\\{your system user}\\.hudson

3. Copy content of Jenkins folder from path "\\10.100.75.17\Softwares\.hudson" and place in local .hudson folder

3a. - Clone the repo in new folder somewhere on your laptop with .ssh
i) Go to http://gitlab.payu.in/help/ssh
ii) follow the steps, instead of cat use notepad, and copy the content.
iii) Paste them in http://gitlab.payu.in/profile/keys
iV) Go to your .ssh folder (eg. C:\Users\nitin.mukhija.IDC\.ssh) and copy the contents in git folder (eg. C:\Program Files (x86)\Git\.ssh) as well.


4. Copy config.xml files of different jobs from "Jenkins\{Product/Money}_Jobs\Local Machine jobs Config" folder to different jobs folder in .hudson folder

5. Copy jenkins.war file from path "\\10.100.75.17\Softwares" and copy in some local folder.

6. On Command prompt, navigate to folder where Jenkins.war file is located and execute command :
java -jar jenkins.war --ajp13Port=-1 --httpPort=8082

e.g., java -jar "C:\Jenkins\jenkins.war" --ajp13Port=-1 --httpPort=8082. Wait until it shows message "Jenkins is fully up and started"

5. Open URL localhost:8082 and navigate to 'Manage Jenkins' > 'Configure System'

6. Configure "JDK Installation", "Maven Installation", "Git Installation" and "Test-Link Installation" with the help of 
"JDK-Git configuration", "Maven-Jenkins configuration" and "TestLink configuration" screen shots in "PayU\Jenkins" folder

7. Click save/apply Button.

8. Configure git option in InternalExecution job with help of "git configuration in InternalExecution-job" 
& "gitlab-ssh configuration in InternalExecution -job" screenshot

10. Click save/Apply Button.

Jenkins is configured and ready to run

11. Run the Run by test plan job for BVT and kill it once it reaches cloning git repo step. This will create workspace folder for you (eg. C:\Users\nitin.mukhija.IDC\.hudson\jobs\InternalExecution\workspace)

12. Go to the repo where you have done the cloning in step 3a. And copy its contents (including hidden .git folder) to jenkins workspace (eg. C:\Users\nitin.mukhija.IDC\.hudson\jobs\InternalExecution\workspace)