#!/usr/bin/perl

use strict;
use Chart::StackedBars;
use Chart::Composite;
use Chart::Lines;

# Absolutely clueless how this works, but it does.

my @files = grep {!/^-/ && -s "$_"} @ARGV;
our @args = grep {/^-/ && !-f "$_"} @ARGV;
shift(@files);

chdir $ARGV[1];

my $DEBUG = 1;
my $INDEXFILE; 

my %entire = ();
my %glabels = ();
my %gthreads = ();
my $atflag = 0; 

# 15 seconds interval.
my $collectinterval = 45;   

our @cusps = (500, 1000, 5000, 10000, 25000, 59999, 60000);

our @labels = ();
our @threads = ();

our %timestamps = ();
our $respcount = 0;
our $measures = 0;

my ($entireta,$entirecnt,$entireby);
my ($respcount,$sumresptimes,$sumSQresptimes); 

our %colors = (
  dataset0 => "green",
  dataset1 => [0, 139, 139], # dark cyan
  dataset2 => [255, 215,0],  # gold
  dataset3 => "DarkOrange",
  dataset4 => "red",
  dataset5 => [139, 0, 139], # dark magenta
  dataset6 => [255, 0, 0],   # red
  dataset7 => [0, 0, 0],     # black
);

open ($INDEXFILE, '>>index.html');

while(my $file = shift(@files)) {
  print "Opening file $file\n" if $DEBUG;
  open(IN, "<$file") || do  {
    print $file, " ", $!, "\n";
    next;
  };

  print "Parsing data from $file\n" if $DEBUG;
  while(<IN>) {
    my ($time,$timestamp,$success,$label,$thread,$latency,$bytes,$DataEncoding,$DataType,$ErrorCount,$Hostname,$NumberOfActiveThreadsAll,$NumberOfActiveThreadsGroup,$ResponseCode,$ResponseMessage,$SampleCount);
    if(/^<(sample|httpSample)\s/) {

      ($time) = (/\st="(\d+)"/o);
      ($timestamp) = (/\sts="(\d+)"/o);
      ($success) = (/\ss="(.+?)"/o);
      ($label) = (/\slb="(.+?)"/o);
      ($thread) = (/\stn="(.+?)"/o);
      ($latency) = (/\slt="(\d+)"/o);
      ($bytes) = (/\sby="(\d+)"/o);
      ($DataEncoding) = (/\sde="(\d+)"/o);
      ($DataType) = (/\sdt="(.+?)"/o);
      ($ErrorCount) = (/\sec="(\d+)"/o);
      ($Hostname) = (/\shn="(.+?)"/o);
      ($NumberOfActiveThreadsAll) = (/\sna="(\d+)"/o);
      ($NumberOfActiveThreadsGroup) = (/\sng="(\d+)"/o);
      ($ResponseCode) = (/\src="(.+?)"/o);
      ($ResponseMessage) = (/\srm="(.+?)"/o);
      ($SampleCount) = (/\ssc="(\d+)"/o);

    } elsif(/^<sampleResult/) {
      ($time) = (/\stime="(\d+)"/o);
      ($timestamp) = (/timeStamp="(\d+)"/o);
      ($success) = (/success="(.+?)"/o);
      ($label) = (/label="(.+?)"/o);
      ($thread) = (/threadName="(.+?)"/o);
    } else {
      next;
    }

    $label =~ s/\s+$//g;
    $label =~ s/^\s+//g;
    $label =~ s/[\W\s]+/_/g;

    next if($label =~ /^garbage/i); # garbage

    if(!grep(/^$label$/, @labels)) {
      push(@labels, $label);
      print "Found new label: $label\n" if $DEBUG;
    }
    $glabels{$label}{'respcount'} += 1;
    $entire{'respcount'} += 1;

    my $tstmp = int($timestamp / (1000 * $collectinterval)) * $collectinterval;
    $timestamps{$tstmp} += 1;

    for(my $i = 0; $i <= $#cusps; $i++) {
      if(($time <= $cusps[$i]) || (($i == $#cusps) && ($time > $cusps[$i]))) {
        $glabels{$label}{$cusps[$i]} += 1;
        $entire{$cusps[$i]} += 1;
        last;
      }
    }
    
    $respcount += 1;
    $sumresptimes += $time;
    $sumSQresptimes += ($time ** 2);
    if($respcount > 1) {
      my $stddev = sqrt(($respcount * $sumSQresptimes - $sumresptimes ** 2) /
        ($respcount * ($respcount - 1)));

      $entire{$tstmp, 'stddev'} = $glabels{$label}{$tstmp, 'stddev'} = $stddev;

    }

    $entire{$tstmp, 'avg'} = $sumresptimes / $respcount;

    $glabels{$label}{$tstmp, 'responsetime'} += $time;
    $glabels{$label}{$tstmp, 'respcount'} += 1;
    $glabels{$label}{$tstmp, 'avg'} = int($glabels{$label}{$tstmp, 'responsetime'} / $glabels{$label}{$tstmp, 'respcount'});

    if(!$entire{$tstmp, 'activethreads'}) {
      $entireta = 0;
      $entirecnt = 0;
      $entireby = 0;
    }

    if($NumberOfActiveThreadsAll > 0) {
      $atflag = 1;
    }

    $entirecnt += 1;

    if($atflag == 1) {
      $entireta += $NumberOfActiveThreadsAll;
      $entire{$tstmp, 'activethreads'} = int($entireta / $entirecnt);
  
      if(!$glabels{$label}{$tstmp, 'activethreads'}) {
        $glabels{$label}{$tstmp, 'lbta'} = 0;
        $glabels{$label}{$tstmp, 'lbby'} = 0;
      }
      $glabels{$label}{$tstmp, 'lbta'} += $NumberOfActiveThreadsAll;
      $glabels{$label}{$tstmp, 'activethreads'} = sprintf("%.0f", $glabels{$label}{$tstmp, 'lbta'} / $glabels{$label}{$tstmp, 'respcount'});

    } else {
      if($NumberOfActiveThreadsAll eq '') {
              if(!$gthreads{$thread}{'first'}) {
          $gthreads{$thread}{'first'} = $tstmp;
          push(@threads, $thread);
        }
  
        $gthreads{$thread}{'last'} = $tstmp;
      }
    }

    if($bytes > 0) {
      $entireby += $bytes;
      $entire{$tstmp, 'throughput'} = int($entireby / $entirecnt);
  
      $glabels{$label}{$tstmp, 'lbby'} += $bytes;
      $glabels{$label}{$tstmp, 'throughput'} = $glabels{$label}{$tstmp, 'lbby'}; # counts per $collectinterval
	  $glabels{'entire'} = \%entire;
    }

  }
  print "Closing $file\n" if $DEBUG;
  close(IN);
}

print "Found $#labels labels\n" if $DEBUG;

# Sort the labels.
#print "Sorting labels\n" if $DEBUG;
#my @tmplabels = sort @labels;
#@labels = @tmplabels;

if($atflag == 0) {
  print "using timestamps to calculate active threads\n";
  my @tstmps = sort { $a <=> $b } keys(%timestamps);
  foreach my $label ('entire', @labels) {
    print "tracking $label\n";
    foreach my $thread (@threads) {
      foreach my $tstmp (@tstmps) {
        if($gthreads{$thread}{'first'} <= $tstmp && $gthreads{$thread}{'last'} >= $tstmp) {
          $glabels{$label}{$tstmp, 'activethreads'} += 1;
        }
      }
    }
  }
}

if($respcount > 0) {
  $measures = scalar(keys(%timestamps));

  print $INDEXFILE <<EndOfHTML;
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
  <head>

  <title>Load Test Graphs</title>
			<style type="text/css">
				body {
					font:normal 68% verdana,arial,helvetica;
					color:#000000;
				}
				table tr td, table tr th {
					font-size: 68%;
				}
				table.details tr th{
					font-weight: bold;
					text-align:left;
					background:#a6caf0;
					white-space: nowrap;
				}
				table.details tr td{
					background:#eeeee0;
					white-space: nowrap;
				}
				h1 {
					margin: 0px 0px 5px; font: 165% verdana,arial,helvetica
				}
				h2 {
					margin-top: 1em; margin-bottom: 0.5em; font: bold 125% verdana,arial,helvetica
				}
				h3 {
					margin-bottom: 0.5em; font: bold 115% verdana,arial,helvetica
				}
				h4 {
					margin: 0px 0px 5px; font: 135% verdana,arial,helvetica
				}
				.Failure {
					font-weight:bold; color:red;
				}
				
	
				img
				{
				  border-width: 0px;
				}
				
				.expand_link
				{
				   position=absolute;
				   right: 0px;
				   width: 27px;
				   top: 1px;
				   height: 27px;
				}
				
				.page_details
				{
				   display: none;
				}
                                
                                .page_details_expanded
                                {
                                    display: block;
                                    display/* hide this definition from  IE5/6 */: table-row;
                                }


			</style>
  </head>
  <body>
	<h1 align="center">Load Test Graphs </h1>
	<table width="100%" border="0">
		<tr>
			<td align="left"><h4>$ARGV[0]</h4></td>
			<td align="right" valign="middle"><h4>Generated using <a href="http://jakarta.apache.org/jmeter">JMeter</a> and <a href="http://search.cpan.org/~mverb/GDGraph-1.43/Graph.pm">Graph</a>.<h4></td>
		</tr>
	</table>
	<hr size="1" />

EndOfHTML


  print "Generating stacked bars absolute\n" if $DEBUG;
  &ChartStackedBars();

  print "Generating stacked bars relative\n" if $DEBUG;
  &ChartStackedPct();
  
  foreach my $label (@labels) {
    if($entireby > 0) {
      &ChartLines($label, 'throughput');
    }
    &ChartLines($label, 'users');
  }
  print $INDEXFILE "</body>\n";
  print $INDEXFILE "</html>\n";
  close($INDEXFILE);
}

sub ChartStackedPct {

  if(scalar(@labels) == 0) {
    return undef;
  }

  my $ChartStacked = Chart::StackedBars->new(1024, 768);

  my @xaxis = ();
  my @xlabels = ();
  foreach my $label (@labels) {
    print "Attempting to add $label to StackedPCT graph\n" if $DEBUG;
    if(($glabels{$label}{'respcount'} > ($respcount / 100)) || grep(/-alllb/i, @args)) {
      push(@xaxis, $label);
      push(@xlabels, $label);
      print " Added $label\n" if $DEBUG;
    }
  }
  $ChartStacked->add_dataset(@xlabels);

  my ($value,$i,$label);
  my @data = ();
  my @legend_labels = ();

  for($i = 0; $i <= $#cusps; $i++) {
    @data = ();
    foreach my $label (@xaxis) {
      $value = $glabels{$label}{$cusps[$i]};
      if(!defined $value) {
        $value = 0;
      }
      $value = (100 * $value) / $glabels{$label}{'respcount'};
      push(@data, $value);
    }
    $ChartStacked->add_dataset(@data);

    push(@legend_labels, "< " . $cusps[$i] . " msec");
  }

  my %settings = (
    transparent => 'true',
    title => 'Response Time %',
    y_grid_lines => 'true',
    legend => 'right',
    legend_labels => \@legend_labels,
    precision => 0,
    y_label => 'Requests %',
    max_val => 100,
    include_zero => 'true',
    point => 0,
    colors => \%colors,
    x_ticks => 'vertical',
    precision => 0,
  );

  $ChartStacked->set(%settings);

  my $filetm = &dyn_filename;

  print "Generated ChartStackedPct_$filetm.png\n" if $DEBUG;
  $ChartStacked->png("ChartStackedPct_$filetm.png");
  print $INDEXFILE "<div align=\"center\"><img src=\"ChartStackedPct_$filetm.png\"/></div>\n";
}

sub ChartStackedBars {

  if(scalar(@labels) == 0) {
    return undef;
  }

  my $ChartStacked = Chart::StackedBars->new(1024, 768);

  my @xaxis = ();
  my @xlabels = ();
  foreach my $label (@labels) {
    print "Added $label to StackedPCT graph\n" if $DEBUG;
    if(($glabels{$label}{'respcount'} > ($respcount / 100)) || grep(/-alllb/i, @args)) {
      push(@xaxis, $label);
      if(length($label) > 30) {
        push(@xlabels, substr($label, -30));
      } else {
        push(@xlabels, $label);
      }
    }
  }
  $ChartStacked->add_dataset(@xlabels);

  my ($value,$i,$label);
  my @data = ();
  my @legend_labels = ();
  for($i = 0; $i <= $#cusps; $i++) {
    @data = ();
    foreach my $label (@xaxis) {
      $value = $glabels{$label}{$cusps[$i]};
      if($value == undef) {
        $value = 0;
      }
      push(@data, $value);
    }
    $ChartStacked->add_dataset(@data);

    push(@legend_labels, "< " . $cusps[$i] . " msec");
  }

  my %settings = (
    transparent => 'true',
    title => 'Response Time',
    y_grid_lines => 'true',
    legend => 'right',
    legend_labels => \@legend_labels,
    precision => 0,
    y_label => 'Requests',
    include_zero => 'true',
    point => 0,
    colors => \%colors,
    x_ticks => 'vertical',
    precision => 0,
  );

  $ChartStacked->set(%settings);

  my $filetm = &dyn_filename;

  print "Generating ChartStacked_$filetm.png\n" if $DEBUG;
  $ChartStacked->png("ChartStacked_$filetm.png");
  print $INDEXFILE "<div align=\"center\"><img src=\"ChartStacked_$filetm.png\"/></div>\n";
}

sub ChartLines {
  my ($label, $mode) = @_;

  my %labelmap = (
    'entire' => 'total',
  );

  my $title = $label;
        $title = $labelmap{$label} if($labelmap{$label});

  my $ChartComposite = Chart::Composite->new(1024, 768);

  my @tstmps = sort { $a <=> $b } keys(%timestamps);
  my @responsetimes = ();
  my @plusstddev = ();
  my @minusstddev = ();
  my @users = ();
  my @throughput = ();
  my @xaxis = ();
  my $y2label;


  my $tstmp;
  my ($pstd, $mstd) = (0, 0);
  foreach my $tstmp (@tstmps) {
    if($glabels{$label}{$tstmp, 'avg'}) {
      push(@xaxis, $tstmp);
      push(@responsetimes, $glabels{$label}{$tstmp, 'avg'});

      $mstd = $glabels{$label}{$tstmp, 'avg'} - $glabels{$label}{$tstmp, 'stddev'};
      $pstd = $glabels{$label}{$tstmp, 'avg'} + $glabels{$label}{$tstmp, 'stddev'};
      $mstd = 1 if($mstd < 0);  # supress lines below 0
      push(@plusstddev, $pstd);
      push(@minusstddev, $mstd);
    }
  }
  $ChartComposite->add_dataset(@xaxis);
  $ChartComposite->add_dataset(@responsetimes);

  my %colors = (
    dataset0 => "green",
    dataset1 => "red",
  );
  my @ds1 = (1);
  my @ds2 = (2);
  if(grep(/-stddev/ || /-range/, @args)) {
    $ChartComposite->add_dataset(@plusstddev);
    $ChartComposite->add_dataset(@minusstddev);
    @ds1 = (1, 2, 3);
    @ds2 = (4);

    %colors = (
      dataset0 => "green",
            dataset1 => [189, 183, 107],  # dark khaki
            dataset2 => [189, 183, 107],  # dark khaki
      dataset3 => "red",
    );
  }

  if($mode eq 'users') {
    foreach my $tstmp (@xaxis) {
      push(@users, $glabels{$label}{$tstmp, 'activethreads'});
    }
  
    $ChartComposite->add_dataset(@users);
    $y2label = "active threads";
  } else {
    foreach my $tstmp (@xaxis) {
      push(@throughput, $glabels{$label}{$tstmp, 'throughput'});
    }
    $ChartComposite->add_dataset(@throughput);
    $y2label = "throughput bytes/min";
  }

  my $skip = 0;
  if(scalar(@xaxis) > 40) {
    $skip = int(scalar(@xaxis) / 40) + 1;
  }

  my @labels = ($label, $mode);
  if(grep(/-stddev/, @args)) {
    @labels = ($label, "+stddev", "-stddev", $mode);
  }

  my $type = 'Lines';
  if(grep(/-range/i, @args)) {
    @labels = ($label, "n.a", "n.a", $mode);
    $type = 'ErrorBars';
  }

  my %settings = (
    composite_info => [ [$type, \@ds1], ['Lines', \@ds2 ]],
    transparent => 'true',
    title => 'Response Time ' . $title,
    y_grid_lines => 'true',
    legend => 'bottom',
    y_label => 'Response Time msec',
    y_label2 => $y2label,
    legend_labels => \@labels,
    legend_example_height => 1,
    legend_example_height0 => 10,
    legend_example_height1 => 2,
    legend_example_height2 => 2,
    legend_example_height3 => 10,
    legend_example_height4 => 10,
    include_zero => 'true',
    x_ticks => 'vertical',
    skip_x_ticks => $skip,
    brush_size1 => 3,
    brush_size2 => 3,
    pt_size => 6,
    point => 0,
    line => 1,
    f_x_tick => \&formatTime,
    colors => \%colors,
    precision => 0,
  );

  $ChartComposite->set(%settings);

  my $filetm = &dyn_filename;
  my $filename = $label;
  $filename=~ s/\W/_/g;
  $filename .= '_' . $mode . '_' . $filetm . '.png';
  print $filename, "\n";

  $ChartComposite->png($filename);
  print $INDEXFILE '<div align="center"><img src="' . $filename . "\"/></div>\n";

}

sub formatTime {
  my ($tstmp) = @_;

  my $string = scalar(localtime($tstmp));
  my ($rc) = ($string =~ /\s(\d\d:\d\d:\d\d)\s/);

  return $rc;
}

sub dyn_filename {
  use Time::localtime;
  my $tm_obj=localtime;
  my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
  my ($sec,$min,$hour,$day,$month,$year)=($tm_obj->sec,$tm_obj->min,$tm_obj->hour,$tm_obj->mday,$tm_obj->mon,$tm_obj->year);
  $year+=1900;
  $month=$monthNames[$month];
  my $file_timestamp = $year . "_" . $month . "_" . $day . "_" . $hour . "_" . $min . "_" . $sec;

  return $file_timestamp;
}

