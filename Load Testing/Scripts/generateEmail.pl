#! /usr/bin/perl -w

print <<ENDHTML;
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html lang="en-US" xml:lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
  <head>

  <title>Load Test Results</title>
<style type="text/css">
				body {
					font:normal 68% verdana,arial,helvetica;
					color:#000000;
				}
				table tr td, table tr th {
					font-size: 100%;
				}
				table.details tr th{
					font-weight: bold;
					text-align:center;
					background:#a6caf0;
					white-space: nowrap;
				}
				table.details tr td{
					background:#eeeee0;
					white-space: nowrap;
					text-align:center;
				}
				h1 {
					margin: 0px 0px 5px; font: 165% verdana,arial,helvetica
				}
				h2 {
					margin-top: 1em; margin-bottom: 0.5em; font: bold 125% verdana,arial,helvetica
				}
				h3 {
					margin-bottom: 0.5em; font: bold 115% verdana,arial,helvetica
				}
				h4 {
					margin: 0px 0px 5px; font: 115% verdana,arial,helvetica
				}
				.Failure {
					font-weight:bold; color:red;
				}
				
	
				img
				{
				  border-width: 0px;
				}
				
				.expand_link
				{
				   position=absolute;
				   right: 0px;
				   width: 27px;
				   top: 1px;
				   height: 27px;
				}
				
				.page_details
				{
				   display: none;
				}
                                
                                .page_details_expanded
                                {
                                    display: block;
                                    display/* hide this definition from  IE5/6 */: table-row;
                                }


			</style>
  </head>
  <body>
	<h1 align="center">Load Test Results</h1>
	<table width="100%" border="0">
		<tr>
			<td align="left"><h4>$ARGV[0]</h4></td>
			<td align="right" valign="middle"><h4>Generated using <a href="http://jakarta.apache.org/jmeter">JMeter</a> and <a href="http://www.perl.org/">perl</a>.<h4></td>
		</tr>
	</table>
	<hr size="1" />

<h2 align="center">Summary</h2>
	<table class="details" border="0" cellpadding="5" cellspacing="2" width="90%" align="center">
		<tr valign="top">
			<th>Script</th>
			<th>User</th>
			<th>Attempt</th>
			<th>Ramp Up</th>
			<th>Raw Data</th>
			<th>Formatted Data</th>
			<th>Visual Data</th>
		</tr>
		$ARGV[1]
	</table>
</body>
</html>
ENDHTML

#print $html;
