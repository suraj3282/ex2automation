#! /usr/bin/perl

{
	package MyWebServer;

	use HTTP::Server::Simple::CGI;
 	use base qw(HTTP::Server::Simple::CGI);
 
 	my %dispatch = (
		'/runLoadTest' => \&runLoadTest,
		'/modifyLoad' => \&modifyLoad,
		'/createCase' => \&createCase,
		'/deleteCase' => \&deleteCase,
		'/' => \&mainPage,
		'' => \&mainPage,
		'/mainPage' => \&mainPage,
		'/uploadFile' => \&uploadFile,
		'/loadRunner' => \&loadRunner,
		'/runLoadRunner' => \&runLoadRunner,
		'/modifyCron' => \&modifyCron,
		'/createCron' => \&createCron,
		'/deleteCron' => \&deleteCron,
		'/pastResults' => \&pastResults,
 	);
 
	my $table = 'loadtestplan.xls';
	my $crontable = 'crontimings.xls';
	my $httpServer = '10.100.17.46:54321';

	sub handle_request {
     		my $self = shift;
     		my $cgi  = shift;
   
     		my $path = $cgi->path_info();
     		my $handler = $dispatch{$path};
 
     		if (ref($handler) eq "CODE") {
         		print "HTTP/1.0 200 OK\r\n";
         		$handler->($cgi);
         
     		} else {
         		print "HTTP/1.0 404 Not found\r\n";
         		print $cgi->header,
               		$cgi->start_html('Not found'),
               		$cgi->h1('Not found'), 
			$cgi->hr, $cgi->br,
			"This aint Apache stupid.",
			$cgi->br, $cgi->a({href=>'/'}, 'Main Page'),
               		$cgi->end_html;
     		}
 	}
 
	sub mainPage {
		my $cgi = shift;
		return if !ref $cgi;

		print $cgi->header,
		      $cgi->start_html('Load Test'),
		      $cgi->h1('Load Test'),
		      $cgi->hr, $cgi->br,
		      $cgi->a({href=>'/runLoadTest'}, 'Run Load Test Now.'), $cgi->br,
		      $cgi->a({href=>'/modifyLoad'}, 'Modify Load Test Params.'), $cgi->br,
		      $cgi->a({href=>'/uploadFile'}, 'Upload JMeter Script.'), $cgi->br,
		      $cgi->a({href=>'/modifyCron'}, 'Modify Cron Timings.'), $cgi->br,
		      $cgi->a({href=>'/pastResults'}, 'View Past Results.'), $cgi->br,
		      $cgi->br, $cgi->br,
		      $cgi->a({href=>'/runLoadRunner'}, 'Start complete load test now.'), $cgi->br,
		      $cgi->br, $cgi->hr,
		      $cgi->end_html;
	}

	sub pastResults {
		my $cgi = shift;
		return if !ref $cgi;

		my @files = glob("*");
		my @dirs, @tmp, $date, $time, @dates, @times, $maxResults=0;
		foreach $file (@files) {
			if ( -d $file  && index($file, "reports_") != -1) {
				@tmp = split('_', $file);
				$date = "$tmp[3] / $tmp[2] / $tmp[1]";
				$time = "$tmp[4] : $tmp[5] : $tmp[6]";
				$file = "http://$httpServer/$file/email.html";
				push (@dirs, $file);
				push (@dates, $date);
				push (@times, $time);
				$maxResults++;
			}
		}
		
		@dirs = reverse(@dirs);
		@dates = reverse(@dates);
		@times = reverse(@times);
		my $count=0;
		print $cgi->header,
		      $cgi->start_html('Previous Results'),
		      $cgi->h1('Previous Results'), $cgi->hr, 
		      $cgi->table({-border=>0}), $cgi->Tr({-align=>'center'}, 
					      $cgi->td({-width=>150}, $cgi->h4('Serial')), 
					      $cgi->td({-width=>150}, $cgi->h4('Date')), 
					      $cgi->td({-width=>150}, $cgi->h4('Time')), 
					      $cgi->td({-width=>150}, $cgi->h4('File')));
		foreach $report (@dirs) {
			print $cgi->Tr({-align=>'center'}, $cgi->td($maxResults-$count), $cgi->td($dates[$count]), 
					$cgi->td($times[$count]), $cgi->td("<a href='$report'>Report</a>"));
			$count++;
		}

		print $cgi->end_table, 
		      $cgi->br, $cgi->hr,
		      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
		      $cgi->end_html;
	}

	sub runLoadRunner {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth = $dbh->prepare("select distinct cgroup from $table");
		$sth->execute;
		my @result, @rows, $count=1;
		$result[0]='*';
		while(@rows = $sth->fetchrow_array) {
			$result[$count] = $rows[0];
			$count++;
		}

		if(!$cgi->param('group')) {
			print $cgi->header,
			      $cgi->start_html('Load Test'),
			      $cgi->h1('Load Test'),
			      $cgi->hr, $cgi->br,
			      $cgi->start_form,
			      "Choose group to run : ",
			      $cgi->popup_menu('group', [@result]), $cgi->submit('Run'),
			      $cgi->br, 
			      $cgi->end_form,
			      $cgi->h3({-align=>'center'}, "Cron Script Table"),
			      $cgi->table({-border=>0, -align=>CENTER}), 
			      $cgi->Tr({-align=>'center'}, 
					      $cgi->td({-width=>150}, $cgi->h4('Script')), 
					      $cgi->td({-width=>100}, $cgi->h4('User')), 
					      $cgi->td({-width=>100}, $cgi->h4('Attempt')),
					      $cgi->td({-width=>100}, $cgi->h4('Ramp Up')),
					      $cgi->td({-width=>100}, $cgi->h4('Group')));
			     			
			my $sth = $dbh->prepare("select * from $table group by cgroup");
			$sth->execute;
			my $count = 0;
			my @result, @rows, $id, $hour, $min, $rampup, $group;
	
			while(@rows = $sth->fetchrow_array) {
				$id = $rows[0];
				$script = $rows[1];
				$user = $rows[2];
				$attempt = $rows[3];
				$rampup = $rows[4]/1000;
				$group = $rows[5];
				print $cgi->Tr({-align=>'center'}, $cgi->td($script), $cgi->td($user), 
						$cgi->td($attempt), $cgi->td($rampup), $cgi->td($group));
			}
	
			print $cgi->end_table, 
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		} else {
			$group = $cgi->param('group');

			print $cgi->header,
			      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'1;URL=/'})),
			      $cgi->h1('Load Test'),
			      $cgi->hr, $cgi->br,
			      "Complete Load Test has been started. Results will be emailed to you as soon as load test is finished.",
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), " | ", $cgi->a({href=>'/runLoadRunner'}, 'Load Runner'), $cgi->br,
			      $cgi->end_html;
			&loadRunner($group);
		}
	}

	sub uploadFile {
		my $cgi = shift;
		return if !ref $cgi;
		
		if(!$cgi->param) {
			print $cgi->header,
			      $cgi->start_html('Script Upload'),
			      $cgi->h1('Script Upload'),
			      $cgi->hr, $cgi->br,
			      $cgi->start_form,  
			      'Choose script to upload : ',
			      $cgi->filefield('script'), $cgi->br,
			      $cgi->submit('Upload'),
			      $cgi->end_form,
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'),
			      $cgi->end_html;
		} else {
			my $file = $cgi->param('script');
			if ( -e "./$file" ) {
				print $cgi->header, $cgi->start_html('Oops'),
				      $cgi->h1('Oops'),
				      $cgi->hr, $cgi->br,
				      "$file Already exits.",
				      $cgi->br, $cgi->a({href=>'/uploadFile'}, 'Script Upload'),
				      $cgi->end_html;
			} else {
				open ( FILE, ">$file" );
				binmode FILE;

				while ( <$file> ) {
					print FILE;
				}
				close FILE;

				print $cgi->header, 
				      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'1;URL=/uploadFile'})),
				      'Thank you for uploading yet another script :( .',
				      $cgi->end_html;
			}
		}
	}
	
	sub modifyCron {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth = $dbh->prepare("select distinct cgroup from $table");
		$sth->execute;
		my @result, @rows, $count=1;
		my @hoursField = qw( 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 );
		my @minsField = qw( 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 );
		$result[0]='*';
		while(@rows = $sth->fetchrow_array) {
			$result[$count] = $rows[0];
			$count++;
		}

		if(!$cgi->param) {
			print $cgi->header,
			      $cgi->start_html('Modify Cron'),
			      $cgi->h1('Modify Cron'),
			      $cgi->hr, 
			      $cgi->table({-border=>0}),
			      $cgi->Tr({-align=>'center'}, $cgi->td($cgi->h3('Hour')), $cgi->td($cgi->h3('Minute')), 
					      $cgi->td($cgi->h3('Group')), $cgi->td($cgi->h3('Type'))),
			      $cgi->start_form(-method=>POST, -action=>'/createCron'),
			      $cgi->Tr({-align=>'center'},  $cgi->td($cgi->popup_menu('hours', [@hoursField])),
					      $cgi->td($cgi->popup_menu('minutes', [@minsField])),
					      $cgi->td($cgi->popup_menu('group', [@result])),
					      $cgi->td($cgi->popup_menu('runtype', ["Sequential", "Parallel"])),
					      $cgi->td($cgi->submit('  Add  '))),
			      $cgi->end_form;
			
			my $sth = $dbh->prepare("select * from $crontable group by cgroup");
			$sth->execute;
			my $count = 0;
			my @result, @rows, $id, $hour, $min, $group;
	
			while(@rows = $sth->fetchrow_array) {
				$id = $rows[0];
				$hour = $rows[1];
				$min = $rows[2];
				$group = $rows[3];
				$runtype = $rows[4];

				print $cgi->Tr({-align=>'center'}, $cgi->td($hour), $cgi->td($min), $cgi->td($group), $cgi->td($runtype), 
						$cgi->td($cgi->start_form(-method=>'POST', -action=>'/deleteCron'), 
						$cgi->hidden('id', $id), $cgi->submit('Delete'), $cgi->end_form)); 
			}
	
			print $cgi->end_table, 
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		}
		$dbh->disconnect;
	}

	sub deleteCron {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

		if($cgi->param) {
			my $id = $cgi->param('id');
			$dbh->do("delete from $crontable where id=?", undef, $id);
		}
		$dbh->disconnect;

		print $cgi->header,
		      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyCron'})),
		      "Doing grunt work.",
		      $cgi->end_html;
	}

	sub createCron {
	       my $cgi = shift;
	       return if !ref $cgi;

	       use DBI;
	       my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

	       if($cgi->param) {
		       my $hours = $cgi->param('hours');
		       my $mins = $cgi->param('minutes');
		       my $group = $cgi->param('group');
		       my $runtype = $cgi->param('runtype');

		       if($hours>=0 && $mins>=0) { 
			      $dbh->{RaiseError} = 1;
			      my $sth = $dbh->prepare("select max(id) from $crontable");
			      $sth->execute;
			      my @row = $sth->fetchrow_array;
			      my $id = $row[0];

			       $dbh->do("insert into $crontable (id, hours, minutes, cgroup, runtype) values (?, ?, ?, ?, ?)", 
					       undef, $id+1, $hours, $mins, $group, $runtype);
		       }
	       }
	       
	       $dbh->disconnect;
	       
	       print $cgi->header,
		     $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyCron'})),
		     "Doing grunt work.",
		     $cgi->end_html;
	}


	sub modifyLoad {
		my $cgi = shift;
		return if !ref $cgi;
	
		my @jmeterFiles = glob("*.jmx");
	
		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		if(!$cgi->param) {
			print $cgi->header,
			      $cgi->start_html('Modify Load'),
			      $cgi->h1('Modify Load'),
			      $cgi->hr, 
			      $cgi->table({-border=>0}),
			      $cgi->Tr({-align=>'center'}, $cgi->td($cgi->h3('Script')), $cgi->td($cgi->h3('User')), 
					      $cgi->td($cgi->h3('Attempt')), $cgi->td($cgi->h3('Ramp up')),
					      $cgi->td($cgi->h3('Group'))),
			      $cgi->start_form(-method=>POST, -action=>'/createCase'),
			      $cgi->Tr({-align=>'center'},  $cgi->td($cgi->popup_menu('scripts', [@jmeterFiles])), 
					      $cgi->td($cgi->textfield('users', '', 5)),
					      $cgi->td($cgi->textfield('attempt', '', 5)),
					      $cgi->td($cgi->textfield('rampup', '0.1', 5)),
					      $cgi->td($cgi->textfield('group', '', 5)),
					      $cgi->td($cgi->submit('  Add  '))),
			      $cgi->end_form;
			
			$dbh->{RaiseError} = 1;
			my $sth = $dbh->prepare("select * from $table group by script");
			$sth->execute;
			my $count = 0;
			my @result, @rows, $id, $script, $user, $attempt;
	
			while(@rows = $sth->fetchrow_array) {
				$id = $rows[0];
				$script = $rows[1];
				$user = $rows[2];
				$attempt = $rows[3];
				$rampup = $rows[4]/1000;
				$group = $rows[5];
				print $cgi->Tr({-align=>'center'}, $cgi->td({-align=>'left'}, $script), $cgi->td($user), $cgi->td($attempt), 
						$cgi->td($rampup), $cgi->td($group), 
						$cgi->td($cgi->start_form(-method=>'POST', -action=>'/deleteCase'), 
						$cgi->hidden('id', $id), $cgi->submit('Delete'), $cgi->end_form)); 
			}
	
			print $cgi->end_table, 
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		}
		$dbh->disconnect;
	}

	sub deleteCase {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

		if($cgi->param) {
			my $id = $cgi->param('id');
			$dbh->do("delete from $table where id=?", undef, $id);
		}
		$dbh->disconnect;

		print $cgi->header,
		      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyLoad'})),
		      "Doing grunt work.",
		      $cgi->end_html;
	}

	sub createCase {
	       my $cgi = shift;
	       return if !ref $cgi;

	       use DBI;
	       my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

	       if($cgi->param) {
		       my $scripts = $cgi->param('scripts');
		       my $users = $cgi->param('users');
		       my $attempts = $cgi->param('attempt');
		       my $rampup = $cgi->param('rampup')*1000;
		       my $group = $cgi->param('group');

		       if($scripts && $users && $attempts && $group) { 
			      $dbh->{RaiseError} = 1;
			      my $sth = $dbh->prepare("select max(id) from $table");
			      $sth->execute;
			      my @row = $sth->fetchrow_array;
			      my $id = $row[0];

			       $dbh->do("insert into $table (id, script, user, attempt, rampup, cgroup) values (?, ?, ?, ?, ?, ?)", 
					       undef, $id+1, $scripts, $users, $attempts, $rampup, $group);
		       }
	       }
	       
	       $dbh->disconnect;
	       
	       print $cgi->header,
		     $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyLoad'})),
		     "Doing grunt work.",
		     $cgi->end_html;
	}
	
	sub runLoadTest {
		my $cgi = shift;
		return if !ref $cgi;
		
		my @jmeterFiles = glob("*.jmx");

		if(!$cgi->param) {
			print	$cgi->header,
				$cgi->start_html('Run Load Test'),
				$cgi->h1('Run Load Test'),
				$cgi->hr, $cgi->br,
				$cgi->start_form,
				"Select Script to run    : ", $cgi->popup_menu('scripts', [@jmeterFiles]), $cgi->br,
				"Enter Number of users   : ", $cgi->textfield('users'), $cgi->br,
				"Enter Attempts per user : ", $cgi->textfield('attempts'), $cgi->br,
				"Enter Ramp up period	 : ", $cgi->textfield('rampup', '0.036'), $cgi->br,
				"Enter EmailId : ", $cgi->textfield('emailid', 'qc-payu@ibibogroup.com'), $cgi->br,
				$cgi->submit, $cgi->br, $cgi->br,
				$cgi->end_form,
				$cgi->hr, $cgi->br, 
				$cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
				$cgi->end_html;
		} else {
			my $scripts = $cgi->param('scripts');
			my $users = $cgi->param('users');
			my $attempts = $cgi->param('attempts');
			my $rampup = $cgi->param('rampup');
			my $emailid = $cgi->param('emailid');

			print $cgi->header,
			      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'1;URL=/'})),
			      $cgi->h1('Load Test'),
			      $cgi->hr, $cgi->br,
			      "Load Test has been started. Results will be emailed to you as soon as load test is finished.",
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		
			# Run JMeter.	
			&webLoadRunner($scripts, $users, $attempts, $rampup, $emailid);
		}
	}

	sub webLoadRunner {
		# They made me write this against my will, so it ain't gonna be pretty.
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		my $pid = fork;
		return if ( $pid );

		use Time::localtime;

		my $script = $_[0];
		my $user = $_[1];
		my $retry = $_[2];
		my $rampup = $_[3];
		my $emailid = $_[4];

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($sec,$min,$hour,$day,$month,$year)=($tm_obj->sec,$tm_obj->min,$tm_obj->hour,$tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];

		my $resultXml = 'result.xml';
		my $resultName = 'report.html';
		my $resultGraph = 'graph.html';
		my $folder = 'reports_' .$year.'_'.$month.'_'.$day.'_'.$hour.'_'.$min.'_'.$sec;
		my $emailBody = '';

		print `mkdir $folder`;
		#print "Running with $user users, $retry threads each.\n";
		print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -JuseRampUp=$rampup -l $folder/$resultXml`;
		#print "End of Run with $user users, $retry threads each.\n";

		# Generate report.
		print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $folder/$resultXml > $folder/$resultName`;
		print `perl generateCharts.pl 'Script: $script, Users: $user, Attempt: $retry' '$folder' -alllb  $resultXml`;
		print `mv $folder/index.html $folder/$resultGraph`;

		# Email Individual reports.
		$emailBody .= "<tr><td><a href='http://$httpServer/$folder/email.html'>$script</a></td><td>$user</td><td>$retry</td><td>$rampup</td><td><a href='http://$httpServer/$folder/$resultXml'>Collected Data</a></td><td><a href='http://$httpServer/$folder/$resultName'>Pretty Table</a></td><td><a href='http://$httpServer/$folder/$resultGraph'>Ugly Graph</a></td></tr>";
	
		# Send Email.
		print `perl generateEmail.pl '$day/$month/$year' '$emailBody' > $folder/email.html`;
		print `mutt -e "set content_type=text/html" -s "Load Test Results." -- $emailid < $folder/email.html`;
		
		exit;
	}

	sub loadRunner {
		# They made me write this against my will, so it ain't gonna be pretty.
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		$pid = fork;
		return if ($pid);

		$group = $_[0];
		return if $group eq '';
	
		use Time::localtime;
		use DBI;

		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth, @row, $count=0;

		if ($group eq '*') {
			$sth = $dbh->prepare("select * from $table group by cgroup");
			$sth->execute;
		} else {
			$sth = $dbh->prepare("select * from $table where cgroup = ?", undef);
			$sth->execute($group);
		}

		while(@row = $sth->fetchrow_array) {
			$scripts[$count] = $row[1];
			$users[$count] = $row[2];
			$retries[$count] = $row[3];
			$rampups[$count] = $row[4]/1000;
			$count++;
		}

		$dbh->disconnect;

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($sec,$min,$hour,$day,$month,$year)=($tm_obj->sec,$tm_obj->min,$tm_obj->hour,$tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];
		my $folder = 'reports_' .$year.'_'.$month.'_'.$day.'_'.$hour.'_'.$min.'_'.$sec;
		my $emailid = 'tech-payu@ibibogroup.com LinuxOPS@ibibogroup.com';
#		my $emailid = 'qc-payu@ibibogroup.com';
		my $emailBody = '';

		print `mkdir $folder`;
		
		$count = 0;
		foreach $script (@scripts) {
			$user = $users[$count]; 
			$retry = $retries[$count];
			$rampup = $rampups[$count];
			$count++;
	
			my $resultXml = 'result_' . $count . '.xml';
			my $resultName = 'report_' . $count . '.html';
			my $resultGraph = 'graph_'. $count . '.html';
		
			print "Running with $user users, $retry threads each.\n";
			print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -JuseRampUp=$rampup -l $folder/$resultXml`;
			print "End of Run with $user users, $retry threads each.\n";

			# Generate report.
			print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $folder/$resultXml > $folder/$resultName`;
			print `perl generateCharts.pl 'Script: $script, Users: $user, Attempt: $retry' '$folder' -alllb $resultXml`;
#			print `mv $folder/index.html $folder/$resultGraph`;

			# Email Individual reports.
			$emailBody .= "<tr><td><a href='http://$httpServer/$folder/email.html'>$script</a></td><td>$user</td><td>$retry</td><td>$rampup</td><td><a href='http://$httpServer/$folder/$resultXml'>Collected Data</a></td><td><a href='http://$httpServer/$folder/$resultName'>Pretty Table</a></td><td><a href='http://$httpServer/$folder/$resultGraph'>Ugly Graph</a></td></tr>";
		}

		# Send Email.
		print `perl generateEmail.pl '$day/$month/$year' '$emailBody' > $folder/email.html`;
		print `mutt -e "set content_type=text/html" -s "Load Test Results." -- $emailid < $folder/email.html`;

		exit;
	}
}

# Create damn excel file.
{
	use DBI;
	my $table = 'loadtestplan.xls';
	my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
	unless ( -e "$table" ) {
		$dbh->do ("CREATE TABLE $table (id integer primary key, script varchar(50), user int, attempt int, rampup int, 
			cgroup int)") or
	       	die "Cannot prepare: " . $dbh->errstr ();
	}
	$table = 'crontimings.xls';
	unless ( -e $table ) {
		$dbh->do("CREATE TABLE $table (id integer primary key, hours int, minutes int, cgroup int, runtype varchar(10))") or 
			die "Cannot prepare : " . $dbh->errstr();
	}
	$dbh->disconnect;
}

# Start the damn cron. 
{
	my $pid = fork;
	if ($pid) {
		print "Use 'kill $pid' to kill cron.\n";
		&init;
		exit;
	} else {
		use DBI;
		use Time::localtime;
		my $table = 'crontimings.xls';
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;

		my $tm_obj, $hour, $min, $sth, @row;
		while(1) {
			$tm_obj=localtime;
			($hour, $min)=($tm_obj->hour,$tm_obj->min);
			$sth = $dbh->prepare("select cgroup, runtype from $table where hours = ? and minutes = ?", undef);
			$sth->execute($hour, $min);
			
			while(@row = $sth->fetchrow_array) {
				if(defined($row[0])) {
					if("Parallel" eq $row[1]) {
						&loadrun_parallel($row[0]);
					} 
					if("Sequential" eq $row[1]) {
						&loadrun_sequential($row[0]);
					}
				}
				sleep(1);
			}
			sleep(30);
		}
		$dbh->disconnect;
		exit;
	}

	sub loadrun_sequential {
		# They made me write this against my will, so it ain't gonna be pretty.
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		$group = $_[0];
		$pid = fork;
		exit if ( $pid );
	
		use Time::localtime;
		use DBI;
		
		my $table = 'loadtestplan.xls';
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth, @row, $count=0;
		
		if($group eq '*') {
			$sth = $dbh->prepare("select * from table group by cgroup");
			$sth->execute;
		} else {
			$sth = $dbh->prepare("select * from $table where cgroup = ?", undef);
			$sth->execute($group);
		}

		while(@row = $sth->fetchrow_array) {
			$scripts[$count] = $row[1];
			$users[$count] = $row[2];
			$retries[$count] = $row[3];
			$rampups[$count] = $row[4]/1000;
			$count++;
		}

		$dbh->disconnect;

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($sec,$min,$hour,$day,$month,$year)=($tm_obj->sec,$tm_obj->min,$tm_obj->hour,$tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];
		my $folder = 'reports_' .$year.'_'.$month.'_'.$day.'_'.$hour.'_'.$min.'_'.$sec;
		my $emailid = 'tech-payu@ibibogroup.com LinuxOPS@ibibogroup.com';
#		my $emailid = 'animesh.kundu@ibibogroup.com';
		my $emailBody = '';

		print `mkdir $folder`;

		$count = 0;
		foreach $script (@scripts) {
			$user = $users[$count];
			$retry = $retries[$count];
			$rampup = $rampups[$count];
			$count++;

			my $resultXml = 'result_' . $count . '.xml';
			my $resultName = 'report_' . $count . '.html';
			my $resultGraph = 'graph_'. $count . '.html';
			my $httpServer = '10.100.17.46:54321';

			print "Running with $user users, $retry threads each.\n";
			print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -JuseRampUp=$rampup -l $folder/$resultXml`;
			print "End of Run with $user users, $retry threads each.\n";

			print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $folder/$resultXml > $folder/$resultName`;
			print `perl generateCharts.pl 'Script: $script, Users: $user, Attempt: $retry' '$folder' -alllb $resultXml`;
			print `mv $folder/index.html $folder/$resultGraph`;

			$emailBody .= "<tr><td><a href='http://$httpServer/$folder/email.html'>$script</a></td><td>$user</td><td>$retry</td><td>$rampup</td><td><a href='http://$httpServer/$folder/$resultXml'>Collected Data</a></td><td><a href='http://$httpServer/$folder/$resultName'>Pretty Table</a></td><td><a href='http://$httpServer/$folder/$resultGraph'>Ugly Graph</a></td></tr>";
		}

		# Send Email.
		print `perl generateEmail.pl '$day/$month/$year' '$emailBody' > $folder/email.html`;
		print `mutt -e "set content_type=text/html" -s "Load Test Results." -- $emailid < $folder/email.html`;
	}

	sub loadrun_parallel {
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		$group = $_[0];
		my $pid = fork;
		exit if ( $pid );
		
		use Time::localtime;
		use DBI;
		use IPC::SysV;

		eval 'sub IPC_CREAT {0001000}' unless defined &IPC_CREAT;
		eval 'sub IPC_RMID {0}'        unless defined &IPC_RMID;
		
		my $table = 'loadtestplan.xls';
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth, @row, $count=0;
		
		if($group eq '*') {
			$sth = $dbh->prepare("select * from table group by cgroup");
			$sth->execute;
		} else {
			$sth = $dbh->prepare("select * from $table where cgroup = ?", undef);
			$sth->execute($group);
		}

		while(@row = $sth->fetchrow_array) {
			$scripts[$count] = $row[1];
			$users[$count] = $row[2];
			$retries[$count] = $row[3];
			$rampups[$count] = $row[4]/1000;
			$count++;
		}

		$dbh->disconnect;

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($sec,$min,$hour,$day,$month,$year)=($tm_obj->sec,$tm_obj->min,$tm_obj->hour,$tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];
		my $folder = 'reports_' .$year.'_'.$month.'_'.$day.'_'.$hour.'_'.$min.'_'.$sec;
		my $emailid = 'tech-payu@ibibogroup.com LinuxOPS@ibibogroup.com';
#		my $emailid = 'animesh.kundu@ibibogroup.com';
		my $emailBody = '';
		my $dkey = $day.$hour.$min.$sec;
		my $skey = 's' . $dkey;
		my $httpServer = '10.100.17.46:54321';
		my $dataSize = 3000;
		my $idshm = shmget( $dkey, $dataSize, &IPC_CREAT | 0777 ) || die "\nCreation shared memory failed $! \n";
		my $idsync = shmget( $skey, 10, &IPC_CREAT | 0777 ) || die "\nCreation shared memory failed $! \n";

		shmwrite( $idshm, $emailBody, 0, $dataSize ) || warn "\n\n shmwrite $! \n";
		shmwrite( $idsync, "0", 0, 10) || warn "\n\n shmwrite $! \n";
		print `mkdir $folder`;

		$count = 0;
		foreach $script (@scripts) {
			$user = $users[$count];
			$retry = $retries[$count];
			$rampup = $rampups[$count];
			$count++;

			my $resultXml = 'result_' . $count . '.xml';
			my $resultName = 'report_' . $count . '.html';
			my $resultGraph = 'graph_'. $count . '.html';

			my $runParallel = fork;
			if( $runParallel == 0 ) {
				print "Running with $user users, $retry threads each.\n";
				print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -JuseRampUp=$rampup -l $folder/$resultXml`;
				print "End of Run with $user users, $retry threads each.\n";

				print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $folder/$resultXml > $folder/$resultName`;
				print `perl generateCharts.pl 'Script: $script, Users: $user, Attempt: $retry' '$folder' -alllb $resultXml`;
#				print `mv $folder/index.html $folder/$resultGraph`;

				my $emailData = "<tr><td><a href=\'http://$httpServer/$folder/email.html\'>$script</a></td><td>$user</td><td>$retry</td><td>$rampup</td><td><a href=\'http://$httpServer/$folder/$resultXml\'>Collected Data</a></td><td><a href=\'http://$httpServer/$folder/$resultName\'>Pretty Table</a></td><td><a href=\'http://$httpServer/$folder/$resultGraph\'>Ugly Graph</a></td></tr>";

				my $data; my $len;
				while(1) {
					shmread( $idsync, $data, 0, 10 ) || warn "\n\n shmread $! \n";
					$data =~ s/0*$//;
					if ( $data == 0 ) {
						$len = length($emailData);
						shmwrite( $idsync, "$len", 0, 10 ) || warn "\n\n shmwrite $! \n";
						shmwrite( $idshm, $emailData, 0, length($emailData) ) || warn "\n\n shmwrite $! \n";
						last;
					}
					sleep(1);
				}

				exit();
			} 
		}

		$emailBody = '';
		my $varUndef = undef;
		my $data; my $len;
		foreach $script (@scripts) {
			while(1) {
				shmread( $idsync, $len, 0, 10 ) || warn "\n\n shmread $! \n";
				
				# Dealing with idiosyncrasies.
				$len =~ s/0*$//; 
				if ( $len > 0 ) {
					shmread( $idshm, $data, 0, $len ) || warn "\n\n shmread $! \n";
					$emailBody="$emailBody$data";
					shmwrite( $idsync, "0", 0, 10 ) || warn "\n\n shmwrite $! \n";
					last;
				}
				sleep(1);
			}
		}
	
		# Send Email.
		print `perl generateEmail.pl "$day/$month/$year" "$emailBody" > $folder/email.html`;
		print `mutt -e "set content_type=text/html" -s "Load Test Results." -- $emailid < $folder/email.html`;

		# We don't want zombies.
		while(wait() != -1) {
			sleep(1);
		}

		shmctl( $idshm, &IPC_RMID, 0 ) or die "Can't shmctl: $! ";
		shmctl( $idsync, &IPC_RMID, 0 ) or die "Can't shmctl: $! ";
	}
}

sub init {
	# Start the damn server.
	my $pid = MyWebServer->new(80)->background();
	print "Use 'kill $pid' to kill webserver. OMG!\n";
}
