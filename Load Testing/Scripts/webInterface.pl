#! /usr/bin/perl

# Understanding is subject to readers aptitude.
#    				-- Vivek.

{
	package MyWebServer;

	use HTTP::Server::Simple::CGI;
 	use base qw(HTTP::Server::Simple::CGI);
 
 	my %dispatch = (
		'/runLoadTest' => \&runLoadTest,
		'/modifyLoad' => \&modifyLoad,
		'/createCase' => \&createCase,
		'/deleteCase' => \&deleteCase,
		'/' => \&mainPage,
		'' => \&mainPage,
		'/mainPage' => \&mainPage,
		'/uploadFile' => \&uploadFile,
		'/loadRunner' => \&loadRunner,
		'/runLoadRunner' => \&runLoadRunner,
		'/modifyCron' => \&modifyCron,
		'/createCron' => \&createCron,
		'/deleteCron' => \&deleteCron,
 	);
 
	my $table = 'loadtestplan.xls';
	my $crontable = 'crontimings.xls';

	sub handle_request {
     		my $self = shift;
     		my $cgi  = shift;
   
     		my $path = $cgi->path_info();
     		my $handler = $dispatch{$path};
 
     		if (ref($handler) eq "CODE") {
         		print "HTTP/1.0 200 OK\r\n";
         		$handler->($cgi);
         
     		} else {
         		print "HTTP/1.0 404 Not found\r\n";
         		print $cgi->header,
               		$cgi->start_html('Not found'),
               		$cgi->h1('Not found'), 
			$cgi->hr, $cgi->br,
			"This aint Apache stupid.",
			$cgi->br, $cgi->a({href=>'/'}, 'Main Page'),
               		$cgi->end_html;
     		}
 	}
 
	sub mainPage {
		my $cgi = shift;
		return if !ref $cgi;

		print $cgi->header,
		      $cgi->start_html('Load Test'),
		      $cgi->h1('Load Test'),
		      $cgi->hr, $cgi->br,
		      $cgi->a({href=>'/runLoadTest'}, 'Run Load Test Now.'), $cgi->br,
		      $cgi->a({href=>'/modifyLoad'}, 'Modify Load Test Params.'), $cgi->br,
		      $cgi->a({href=>'/uploadFile'}, 'Upload JMeter Script.'), $cgi->br,
		      $cgi->a({href=>'/modifyCron'}, 'Modify Cron Timings.'), $cgi->br,
		      $cgi->br, $cgi->br,
		      $cgi->a({href=>'/runLoadRunner'}, 'Start complete load test now.'), $cgi->br,
		      $cgi->br, $cgi->hr,
		      $cgi->end_html;
	}

	sub runLoadRunner {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth = $dbh->prepare("select distinct cgroup from $table");
		$sth->execute;
		my @result, @rows, $count=1;
		$result[0]='*';
		while(@rows = $sth->fetchrow_array) {
			$result[$count] = $rows[0];
			$count++;
		}

		if(!$cgi->param('group')) {
			print $cgi->header,
			      $cgi->start_html('Load Test'),
			      $cgi->h1('Load Test'),
			      $cgi->hr, $cgi->br,
			      $cgi->start_form,
			      "Choose group to run : ",
			      $cgi->popup_menu('group', [@result]), $cgi->submit('Run'),
			      $cgi->br, 
			      $cgi->end_form,
			      $cgi->h3({-align=>'center'}, "Cron Script Table"),
			      $cgi->table({-border=>0, -align=>CENTER}), 
			      $cgi->Tr({-align=>'center'}, 
					      $cgi->td({-width=>150}, $cgi->h4('Script')), 
					      $cgi->td({-width=>150}, $cgi->h4('User')), 
					      $cgi->td({-width=>150}, $cgi->h4('Attempt')), 
					      $cgi->td({-width=>150}, $cgi->h4('Group')));
			     			
			my $sth = $dbh->prepare("select * from $table group by cgroup");
			$sth->execute;
			my $count = 0;
			my @result, @rows, $id, $hour, $min, $group;
	
			while(@rows = $sth->fetchrow_array) {
				$id = $rows[0];
				$script = $rows[1];
				$user = $rows[2];
				$attempt = $rows[3];
				$group = $rows[4];
				print $cgi->Tr({-align=>'center'}, $cgi->td($script), $cgi->td($user), $cgi->td($attempt), $cgi->td($group));
			}
	
			print $cgi->end_table, 
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		} else {
			$group = $cgi->param('group');

			print $cgi->header,
			      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'1;URL=/'})),
			      $cgi->h1('Load Test'),
			      $cgi->hr, $cgi->br,
			      "Complete Load Test has been started. Results will be emailed to you as soon as load test is finished.",
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), " | ", $cgi->a({href=>'/runLoadRunner'}, 'Load Runner'), $cgi->br,
			      $cgi->end_html;
			&loadRunner($group);
		}
	}

	sub uploadFile {
		my $cgi = shift;
		return if !ref $cgi;
		
		if(!$cgi->param) {
			print $cgi->header,
			      $cgi->start_html('Script Upload'),
			      $cgi->h1('Script Upload'),
			      $cgi->hr, $cgi->br,
			      $cgi->start_form,  
			      'Choose script to upload : ',
			      $cgi->filefield('script'), $cgi->br,
			      $cgi->submit('Upload'),
			      $cgi->end_form,
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'),
			      $cgi->end_html;
		} else {
			my $file = $cgi->param('script');
			if ( -e "./$file" ) {
				print $cgi->header, $cgi->start_html('Oops'),
				      $cgi->h1('Oops'),
				      $cgi->hr, $cgi->br,
				      "$file Already exits.",
				      $cgi->br, $cgi->a({href=>'/uploadFile'}, 'Script Upload'),
				      $cgi->end_html;
			} else {
				open ( FILE, ">$file" );
				binmode FILE;

				while ( <$file> ) {
					print FILE;
				}
				close FILE;

				print $cgi->header, 
				      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'1;URL=/uploadFile'})),
				      'Thank you for uploading yet another script :( .',
				      $cgi->end_html;
			}
		}
	}
	
	sub modifyCron {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth = $dbh->prepare("select distinct cgroup from $table");
		$sth->execute;
		my @result, @rows, $count=1;
		$result[0]='*';
		while(@rows = $sth->fetchrow_array) {
			$result[$count] = $rows[0];
			$count++;
		}

		if(!$cgi->param) {
			print $cgi->header,
			      $cgi->start_html('Modify Cron'),
			      $cgi->h1('Modify Cron'),
			      $cgi->hr, 
			      $cgi->table({-border=>0}),
			      $cgi->Tr({-align=>'center'}, $cgi->td($cgi->h3('Hour')), 
					      $cgi->td($cgi->h3('Minute')), $cgi->td($cgi->h3('Group'))),
			      $cgi->start_form(-method=>POST, -action=>'/createCron'),
			      $cgi->Tr({-align=>'center'},  $cgi->td($cgi->textfield('hours', '', 5)),
					      $cgi->td($cgi->textfield('minutes', '', 5)),
					      $cgi->td($cgi->popup_menu('group', [@result])),
					      $cgi->td($cgi->submit('  Add  '))),
			      $cgi->end_form;
			
			my $sth = $dbh->prepare("select * from $crontable group by cgroup");
			$sth->execute;
			my $count = 0;
			my @result, @rows, $id, $hour, $min, $group;
	
			while(@rows = $sth->fetchrow_array) {
				$id = $rows[0];
				$hour = $rows[1];
				$min = $rows[2];
				$group = $rows[3];
				print $cgi->Tr({-align=>'center'}, $cgi->td($hour), $cgi->td($min), $cgi->td($group), 
						$cgi->td($cgi->start_form(-method=>'POST', -action=>'/deleteCron'), 
						$cgi->hidden('id', $id), $cgi->submit('Delete'), $cgi->end_form)); 
			}
	
			print $cgi->end_table, 
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		}
		$dbh->disconnect;
	}

	sub deleteCron {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

		if($cgi->param) {
			my $id = $cgi->param('id');
			$dbh->do("delete from $crontable where id=?", undef, $id);
		}
		$dbh->disconnect;

		print $cgi->header,
		      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyCron'})),
		      "Doing grunt work.",
		      $cgi->end_html;
	}

	sub createCron {
	       my $cgi = shift;
	       return if !ref $cgi;

	       use DBI;
	       my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

	       if($cgi->param) {
		       my $hours = $cgi->param('hours');
		       my $mins = $cgi->param('minutes');
		       my $group = $cgi->param('group');

		       if($hours>=0 && $mins>=0) { 
			      $dbh->{RaiseError} = 1;
			      my $sth = $dbh->prepare("select max(id) from $crontable");
			      $sth->execute;
			      my @row = $sth->fetchrow_array;
			      my $id = $row[0];

			       $dbh->do("insert into $crontable (id, hours, minutes, cgroup) values (?, ?, ?, ?)", 
					       undef, $id+1, $hours, $mins, $group);
		       }
	       }
	       
	       $dbh->disconnect;
	       
	       print $cgi->header,
		     $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyCron'})),
		     "Doing grunt work.",
		     $cgi->end_html;
	}


	sub modifyLoad {
		my $cgi = shift;
		return if !ref $cgi;
	
		my @jmeterFiles = glob("*.jmx");
	
		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		if(!$cgi->param) {
			print $cgi->header,
			      $cgi->start_html('Modify Load'),
			      $cgi->h1('Modify Load'),
			      $cgi->hr, 
			      $cgi->table({-border=>0}),
			      $cgi->Tr({-align=>'center'}, $cgi->td($cgi->h3('Script')), 
					      $cgi->td($cgi->h3('User')), $cgi->td($cgi->h3('Attempt')), $cgi->td($cgi->h3('Group'))),
			      $cgi->start_form(-method=>POST, -action=>'/createCase'),
			      $cgi->Tr({-align=>'center'},  $cgi->td($cgi->popup_menu('scripts', [@jmeterFiles])), 
					      $cgi->td($cgi->textfield('users', '', 5)),
					      $cgi->td($cgi->textfield('attempt', '', 5)),
					      $cgi->td($cgi->textfield('group', '', 5)),
					      $cgi->td($cgi->submit('  Add  '))),
			      $cgi->end_form;
			
			$dbh->{RaiseError} = 1;
			my $sth = $dbh->prepare("select * from $table group by script");
			$sth->execute;
			my $count = 0;
			my @result, @rows, $id, $script, $user, $attempt;
	
			while(@rows = $sth->fetchrow_array) {
				$id = $rows[0];
				$script = $rows[1];
				$user = $rows[2];
				$attempt = $rows[3];
				$group = $rows[4];
				print $cgi->Tr({-align=>'center'}, $cgi->td({-align=>'left'}, $script), $cgi->td($user), $cgi->td($attempt), 
						$cgi->td($group), $cgi->td($cgi->start_form(-method=>'POST', -action=>'/deleteCase'), 
						$cgi->hidden('id', $id), $cgi->submit('Delete'), $cgi->end_form)); 
			}
	
			print $cgi->end_table, 
			      $cgi->br, $cgi->hr,
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		}
		$dbh->disconnect;
	}

	sub deleteCase {
		my $cgi = shift;
		return if !ref $cgi;

		use DBI;
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

		if($cgi->param) {
			my $id = $cgi->param('id');
			$dbh->do("delete from $table where id=?", undef, $id);
		}
		$dbh->disconnect;

		print $cgi->header,
		      $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyLoad'})),
		      "Doing grunt work.",
		      $cgi->end_html;
	}

	sub createCase {
	       my $cgi = shift;
	       return if !ref $cgi;

	       use DBI;
	       my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";

	       if($cgi->param) {
		       my $scripts = $cgi->param('scripts');
		       my $users = $cgi->param('users');
		       my $attempts = $cgi->param('attempt');
		       my $group = $cgi->param('group');

		       if($scripts && $users && $attempts && $group) { 
			      $dbh->{RaiseError} = 1;
			      my $sth = $dbh->prepare("select max(id) from $table");
			      $sth->execute;
			      my @row = $sth->fetchrow_array;
			      my $id = $row[0];

			       $dbh->do("insert into $table (id, script, user, attempt, cgroup) values (?, ?, ?, ?, ?)", 
					       undef, $id+1, $scripts, $users, $attempts, $group);
		       }
	       }
	       
	       $dbh->disconnect;
	       
	       print $cgi->header,
		     $cgi->start_html(-head => $cgi->meta({-http_equiv=>'refresh', -content=>'0;URL=/modifyLoad'})),
		     "Doing grunt work.",
		     $cgi->end_html;
	}
	
	sub runLoadTest {
		my $cgi = shift;
		return if !ref $cgi;
		
		my @jmeterFiles = glob("*.jmx");

		if(!$cgi->param) {
			print	$cgi->header,
				$cgi->start_html('Run Load Test'),
				$cgi->h1('Run Load Test'),
				$cgi->hr, $cgi->br,
				$cgi->start_form,
				"Select Script to run    : ", $cgi->popup_menu('scripts', [@jmeterFiles]), $cgi->br,
				"Enter Number of users   : ", $cgi->textfield('users'), $cgi->br,
				"Enter Attempts per user : ", $cgi->textfield('attempts'), $cgi->br,
				"Enter EmailId : ", $cgi->textfield('emailid', 'abc@xyz.com'), $cgi->br,
				$cgi->submit, $cgi->br, $cgi->br,
				$cgi->end_form,
				$cgi->hr, $cgi->br, 
				$cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
				$cgi->end_html;
		} else {
			my $scripts = $cgi->param('scripts');
			my $users = $cgi->param('users');
			my $attempts = $cgi->param('attempts');
			my $emailid = $cgi->param('emailid');

			print $cgi->header,
			      $cgi->start_html('Load Test'),
			      $cgi->h1('Load Test'),
			      $cgi->hr, $cgi->br,
			      "Load Test has been started. Results will be emailed to you as soon as load test is finished.",
			      $cgi->br, $cgi->a({href=>'/'}, 'Main Page'), $cgi->br,
			      $cgi->end_html;
		
			# Run JMeter.	
			&webLoadRunner($scripts, $users, $attempts, $emailid);
		}
	}

	sub webLoadRunner {
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		my $pid = fork;
		return if ($pid);

		use Time::localtime;

		my $script = $_[0];
		my $user = $_[1];
		my $retry = $_[2];
		my $emailid = $_[3];

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($day,$month,$year)=($tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];

		my $resultFile = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt.xml';
		#print "Running with $user users, $retry threads each.\n";
		print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -l $resultFile`;
		#print "End of Run with $user users, $retry threads each.\n";

		# Generate report.
		my $resultName = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt.html';
		my $emailBody = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt_Email.html';
		print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $resultFile > $resultName`;
		print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateEmailBody.xsl $resultFile > $emailBody`;
		print `rm -f webresult.tar.gz`;
		print `tar -zcf webresult.tar.gz $resultName`;

		# Email Individual reports.
		print `mutt -e "set content_type=text/html" -s "Load Test Results for $script having $user Users, $retry Attempts each." -a webresult.tar.gz -- $emailid < $emailBody`;
		print `rm -f $emailBody $resultName $resultFile webresult.tar.gz`;

		exit;
	}

	sub loadRunner {
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		$pid = fork;
		return if ($pid);

		$group = $_[0];
		return if $group eq '';
	
		use Time::localtime;
		use DBI;

		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth, @row, $count=0;

		if ($group eq '*') {
			$sth = $dbh->prepare("select * from $table group by cgroup");
			$sth->execute;
		} else {
			$sth = $dbh->prepare("select * from $table where cgroup = ?", undef);
			$sth->execute($group);
		}

		while(@row = $sth->fetchrow_array) {
			$scripts[$count] = $row[1];
			$users[$count] = $row[2];
			$retries[$count] = $row[3];
			$count++;
		}

		$dbh->disconnect;

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($day,$month,$year)=($tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];
		my $folder = 'reports_' . int(rand(1000));
		my $archive = "reports_".$day."_".$month."_".$year.".tar.gz";
		my $emailid = 'tech-payu@ibibogroup.com LinuxOPS@ibibogroup.com';

		print `rm -rf $folder`;
		print `mkdir $folder`;
		print `cp expand.png collapse.png $folder/`;
		
		$count = 0;
		foreach $script (@scripts) {
			$user = $users[$count]; 
			$retry = $retries[$count];
			$count++;
	
			my $resultFile = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt.xml';
			#print "Running with $user users, $retry threads each.\n";
			print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -l $folder/$resultFile`;
			#print "End of Run with $user users, $retry threads each.\n";

			# Generate report.
			$resultName = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt.html';
			$emailBody = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt_Email.html';
			print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $folder/$resultFile > $folder/$resultName`;
			print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateEmailBody.xsl $folder/$resultFile > $folder/$emailBody`;
			print `rm -f $folder/result.tar.gz`;
			print `tar -zcf $folder/result.tar.gz $folder/$resultName`;

			# Email Individual reports.
			print `mutt -e "set content_type=text/html" -s "Load Test Results for $script having $user Users, $retry Attempts each." -a $folder/result.tar.gz -- $emailid < $folder/$emailBody`;
			print `rm -f $folder/$emailBody $folder/result.tar.gz`;
		}

		print `tar -zcf $archive $folder`;
		print `rm -rf $folder`;

		# Send Email.
		print `echo "Load test has completed" | mutt -s "Load Test Results" -a $archive -- $emailid`;
		print `rm -rf $archive`;
		exit;
	}
}

# Create damn excel file.
{
	use DBI;
	my $table = 'loadtestplan.xls';
	my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
	unless ( -e "$table" ) {
		$dbh->do ("CREATE TABLE $table (id integer primary key, script varchar(50), user int, attempt int, cgroup int)") or
	       	die "Cannot prepare: " . $dbh->errstr ();
	}
	$table = 'crontimings.xls';
	unless ( -e $table ) {
		$dbh->do("CREATE TABLE $table (id integer primary key, hours int, minutes int, cgroup int)") or 
			die "Cannot prepare : " . $dbh->errstr();
	}
	$dbh->disconnect;
}

# Start the damn cron. 
{
	my $pid = fork;
	if ($pid) {
		print "Use 'kill $pid' to kill cron.\n";
		&init;
		exit;
	} else {
		use DBI;
		use Time::localtime;
		my $table = 'crontimings.xls';
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;

		my $tm_obj, $hour, $min, $sth, @row;
		while(1) {
			$tm_obj=localtime;
			($hour, $min)=($tm_obj->hour,$tm_obj->min);
			$sth = $dbh->prepare("select cgroup from $table where hours = ? and minutes = ?", undef);
			$sth->execute($hour, $min);
			
			while(@row = $sth->fetchrow_array) {
				if(defined($row[0])) {
					&loadrun($row[0]);
				}
			}
			sleep(30);
		}
		$dbh->disconnect;
		exit;
	}

	sub loadrun {
		# EXEMPLARY example of HOW NOT TO WRITE SUBROUTINE.

		$group = $_[0];
		$pid = fork;
		exit if ($pid);
	
		use Time::localtime;
		use DBI;
		
		my $table = 'loadtestplan.xls';
		my $dbh = DBI->connect ("dbi:CSV:") or die "Cannot connect: $DBI::errstr";
		$dbh->{RaiseError} = 1;
		my $sth, @row, $count=0;
		
		if($group eq '*') {
			$sth = $dbh->prepare("select * from table group by cgroup");
			$sth->execute;
		} else {
			$sth = $dbh->prepare("select * from $table where cgroup = ?", undef);
			$sth->execute($group);
		}

		while(@row = $sth->fetchrow_array) {
			$scripts[$count] = $row[1];
			$users[$count] = $row[2];
			$retries[$count] = $row[3];
			$count++;
		}

		$dbh->disconnect;

		my $tm_obj=localtime;
		my @monthNames = qw( Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec );
		my ($day,$month,$year)=($tm_obj->mday,$tm_obj->mon,$tm_obj->year);
		$year+=1900;
		$month=$monthNames[$month];
		my $folder = 'reports_' . int(rand(1000));
		my $archive = "reports_".$day."_".$month."_".$year.".tar.gz";
		my $emailid = 'tech-payu@ibibogroup.com LinuxOPS@ibibogroup.com';

		print `rm -rf $folder`;
		print `mkdir $folder`;

		$count = 0;
		foreach $script (@scripts) {
			$user = $users[$count];
			$retry = $retries[$count];
			$count++;

			my $resultFile = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt.xml';
			print "Running with $user users, $retry threads each.\n";
			print `sh /home/agix/jmeter/bin/jmeter -n -t $script -JuseThreads=$user -JuseLoops=$retry -l $folder/$resultFile`;
			print "End of Run with $user users, $retry threads each.\n";

			# Generate report.
			$resultName = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt.html';
			$emailBody = 'report_'.$script.'_'.$user.'_user_'.$retry.'_attempt_Email.html';
			print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateDetails.xsl $folder/$resultFile > $folder/$resultName`;
			print `xsltproc --stringparam str 'Script: $script, Users: $user, Attempt: $retry' generateEmailBody.xsl $folder/$resultFile > $folder/$emailBody`;
			print `rm -f $folder/result.tar.gz`;
			print `tar -zcf $folder/result.tar.gz $folder/$resultName`;

			# Email Individual reports.
			print `mutt -e "set content_type=text/html" -s "Load Test Results for $script having $user Users, $retry Attempts each." -a $folder/result.tar.gz -- $emailid < $folder/$emailBody`;
			print `rm -f $folder/$emailBody $folder/result.tar.gz`;
		}

		print `tar -zcf $archive $folder`;
		print `rm -rf $folder`;

		# Send Email.
		print `echo "Load test has completed" | mutt -s "Load Test Results" -a $archive -- $emailid`;
		print `rm -rf $archive`;
	}

}

sub init {
	# Start the damn server.
	my $pid = MyWebServer->new(80)->background();
	print "Use 'kill $pid' to kill webserver. OMG!\n";
}
