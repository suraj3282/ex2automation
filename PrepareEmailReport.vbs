Const ForReading = 1
Const ForWriting = 2

if WScript.Arguments.Count <> 2 then
    WScript.Echo "Missing results path parameter"
else
	resultpath = WScript.Arguments(0)
	IP_Address = WScript.Arguments(1)
end if

Dim IP_Address
Dim FQName
Dim path

WScript.Echo IP_Address


If IP_Address = "10.70.8.236" Then
	FQName = "http://10.100.17.39/" & resultpath

Else
	FQName = "file:///C:/Users/Test User/.hudson/jobs/PPReleaseDailyRun/workspace/" & resultpath
End If

path = "C:/Users/Test User/.hudson/jobs/PPReleaseDailyRun/workspace/" & resultpath
path = Replace(path, "/" , "\" )
WScript.Echo path
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set objFile = objFSO.OpenTextFile(path &"\overview.html", ForReading)

strText = objFile.ReadAll
objFile.Close
strNewText = Replace(strText, "href=" & chr(34) & "output.html", "href=" & chr(34) & FQName & "/index.html" )
strNewText = Replace(strNewText, "href=" & chr(34) & "suite", "href=" & chr(34) & FQName & "/suite" )
strNewText = Replace(strNewText, "Log Output", "Full Report" )

objFSO.CopyFile path & "\overview.html", path & "\EmailReport.html"

Set objFile = objFSO.OpenTextFile(path &"\EmailReport.html", ForWriting)
objFile.WriteLine strNewText

objFile.Close