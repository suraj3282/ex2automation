1. Download "Maven <latest version> (Binary zip)" from:- \\10.100.75.17\Softwares\apache-maven-3.2.5-bin.zip
2. Extract the archive and place the contents in some path like:- C:\Program Files\apache-maven-3.2.5
3. Create a System Variable name=M2_HOME , value=C:\Program Files\apache-maven-3.2.5
4. Create User Variable name=M2 , value=%M2_HOME%\bin\
5. Edit the Path variable (under System variable) by appending - %M2_HOME%\bin;
6. Edit the path varible (under System variable) by appending location of your JDK, eg. C:\Program Files\Java\jdk1.8.0_31\bin;; Make sure you have JDK 1.8, if not then install from:- 
\\10.100.75.17\Softwares\jdk-8u31-windows-<32bit/64bit>
7. Create a System Variable name=JAVA_HOME , value=C:\Program Files\Java\jdk1.8.0_31
8. Open a new command prompt (cmd) and run "mvn --version" to verify that maven is correctly installed.

Sample Output
-----------
D:\PayU>mvn --version
Apache Maven 3.2.5 (12a6b3acb947671f09b81f49094c53f426d8cea1; 2014-12-14T22:59:23+05:30)
Maven home: C:\Program Files\apache-maven-3.2.5
Java version: 1.8.0_31, vendor: Oracle Corporation
Java home: C:\Program Files\Java\jdk1.8.0_31\jre
Default locale: en_IN, platform encoding: Cp1252
OS name: "windows 7", version: "6.1", arch: "amd64", family: "dos"
-----------

8. In the cmd, Go to the folder where you have cloned the repo, and run the commands mentioned in "InstallCustomLibs.txt".
9. Use Eclipse kepler - 64 bit version is availble at - \\10.100.75.17\Softwares\eclipse-java-luna-SR1a-win32-x86_64
Download 32 bit version from eclise kelper downloads.

10. Import Common/Product/Money projects in eclipse

11. Run and ensure it works fine. mvn test -DProject=Product -DtestCategory=BVT

12. Ensure that eclipse has no compilation errors


Jenkins config:- follow the screenshots for testlink, maven config

Generate .ssk keys, and ensure they are in:-
1. C:\Users\nitin.mukhija.IDC\.ssh
2. C:\Program Files (x86)\Git\.ssh
 