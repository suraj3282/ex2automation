@ECHO OFF
FOR /F "tokens=* USEBACKQ" %%F IN (`adb devices`) DO (
SET var=%%F
)
set last6=%var:~-6%
for /f "delims=		" %%a in ("%var%") do set part=%%a

if "%last6%" == "device" (goto :CONDITION) ELSE (goto :EMPTY)

:CONDITION
if defined part (goto :NOTEMPTY) ELSE (goto :EMPTY)

:NOTEMPTY
tasklist /fi "windowtitle eq NODE" 2>nul |find /i "cmd.exe" >nul
 If not errorlevel 1 (echo Mobile node is already started) ELSE (start "" "C:\Selenium\MobileNode\Nexus5Test\%part%.bat")
goto :EOF

:EMPTY
tasklist /fi "windowtitle eq NODE" 2>nul |find /i "cmd.exe" >nul
 If not errorlevel 1 (taskkill /fi "windowtitle eq NODE" ) else (echo already stopped)
goto :EOF

:EOF
exit /b

 