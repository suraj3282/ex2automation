set DeviceIds=1f9189c1,0123456789ABCDEF,3230890b972bb01f
echo off
echo.
echo *************************************************************************
echo.
adb devices
echo *************************************************************************

echo.
echo.
echo Checking mobile nodes for connected devices.................................................................
setlocal EnableDelayedExpansion
set devicePresent="false"
set processRunning="false"
for %%i in (%DeviceIds%) do (
echo.
echo *************************************************************************
echo Checking Node For device - %%i

set devicePresent="false"
set processRunning="false"
for /f "tokens=1,2" %%a in ('adb devices') do (
if %%i == %%a (
if %%b == device (
set devicePresent="true"
)
)
)

if !devicePresent! == "true" (

for /f "tokens=1 delims=]" %%a in ('adb -s %%i shell dumpsys wifi ^| find "mCurrent"') do (
for /f "tokens=1,2 delims=[" %%b in ("%%a") do (
if %%c == 0 (
echo Internet is not working on device - %%i

FOR /F "tokens=2 delims=," %%m in ('tasklist /v /fo csv ^| find "%%i"') do (
echo Node is running for %%i
set processRunning="true"
)

if !processRunning! == "true" (
FOR /F "tokens=2 delims=," %%m in ('tasklist /v /fo csv ^| find "%%i"') do (
echo Shutting down the node %%i as Wifi on Device %%i is not working
taskkill /pid %%m
)
)

echo Enabling wifi on Device %%i
for /f "tokens=3 delims= " %%x in ('adb -s %%i shell dumpsys wifi ^| grep "Wi-Fi is"') do (
if %%x == enabled (
          adb -s %%i shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings
          adb -s %%i shell input keyevent 19 & adb -s %%i shell input keyevent 19
          adb -s %%i shell input keyevent 23
)

adb -s %%i shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings
adb -s %%i shell input keyevent 19 & adb -s %%i shell input keyevent 19
adb -s %%i shell input keyevent 23
)

echo WifiStatus on Device - %%i is Now
adb -s %%i shell dumpsys wifi ^| grep "Wi-Fi is"
)
)
)

for /f "tokens=3 delims= " %%x in ('adb -s %%i shell dumpsys wifi ^| grep "Wi-Fi is"') do (
if %%x == disabled (
		
		  echo Enabling wifi on Device %%i
		  
          adb -s %%i shell am start -a android.intent.action.MAIN -n com.android.settings/.wifi.WifiSettings
          adb -s %%i shell input keyevent 19 & adb -s %%i shell input keyevent 19
          adb -s %%i shell input keyevent 23
		  
		  echo WifiStatus on Device - %%i is Now
		  adb -s %%i shell dumpsys wifi ^| grep "Wi-Fi is"
)
)

set processRunning="false"

FOR /F "tokens=2 delims=," %%m in ('tasklist /v /fo csv ^| find "%%i"') do (
echo Node is already running for %%i
set processRunning="true"
)

if !processRunning! == "false" (
echo Starting mobile node for %%i
start C:\Selenium\MobileNode\%%i.bat
)
)

if !devicePresent! == "false" (
   echo Device %%i is not connected or not online
FOR /F "tokens=2 delims=," %%m in ('tasklist /v /fo csv ^| find "%%i"') do (
echo Shutting down the node %%i as device is not connected or device is not online
taskkill /pid %%m
)
)
echo *************************************************************************
)
echo.
echo.