package Reporting;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.json.JSONException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import TestLink.GenerateTestNGXmlAndRun;
import Utils.DBHelperForAccuracyDB;
import Utils.DataBase.DatabaseType;
import Utils.ForAccuracy;
import Utils.ReadResultXMLAndInsertInDB;

/**
 * This Class will be used for sending mail,determining the IP of the Machine which has generated the results, modifying the generated reports
 * sending Email after a test Suit has been executed.
 
 */

public class SendEMail 
{
	public static String  = "";  
	public static String strPercentage = "";
	public static String buildId;
	public static Boolean executionOnRemoteAddress = false;
	public static String projectNameToSendEmail = "";
	public static String osType = System.getProperty("os.name");
	public static enum EmailContentType {HTML,NormalText};
	public static enum ReportType {Consolidate,Automation,Normal}
	public static enum QueryType{insertDataForTodayBuild,selectDataForAreaName,selectDataForBuildId,selectDataForPreviousRun,selectDataForbuildIdAndAreaName,
		selectDataForbuildIdAndGroupNameForPreviousBuild, updateResultLinkField, getTCCountForGroupName}

	/**
	 * This Method is used to Send Email 
	 * @param to		: Comma Separated List of Email Id to which we want to send Email 
	 * @param replyTo	: Comma Separated List of Email Id which we want the person the reply to
	 * @param subject	: Subject of the Email
	 * @param messageContent	: Content that we want to Send
	 * @param EmailContentType	: Defines the type of Content we want to send in the Email
	 * @return			: True/False (Based on Mail has been send or not)
	 */
	public static boolean SendMail(String to, String replyTo, String subject,String messageContent,EmailContentType emailContentType, ReportType reportType) 
	{ 
		final String username = "sgupta@ex2india.com";
		final String password = "sonia@123";

		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.ssl.trust", "smtp.gmail.com"); 
		properties.put("mail.smtp.port", "587");


		Session session = Session.getInstance(properties, new javax.mail.Authenticator(){
			protected javax.mail.PasswordAuthentication getPasswordAuthentication(){
				return new javax.mail.PasswordAuthentication(username, password);
			}});
		try 
		{
			String reportFrom = "";
			switch(reportType)
			{
			case Automation:
				reportFrom = "Automation Report";
				break;
			case Consolidate:
				reportFrom = "Consolidated Report";
				break;
			case Normal:
				reportFrom = "PayU-Report";
				break;
			default:
				System.out.println("Invliad Report Type Passed --> Taking default Report Type as PayU-Report");
				reportFrom = "PayU-Report";
				break;

			}
			Message message = new MimeMessage(session); 		// email message 
			message.setFrom(new InternetAddress("sgupta@ex2india.com",reportFrom)); 
			//message.setFrom(new InternetAddress("Automation Report<testautomation@payu.in>"));
			message.setReplyTo(InternetAddress.parse(replyTo));
			message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to)); 
			message.setSubject(subject);
			switch(emailContentType)
			{
			case HTML:
				String filePath = "";
				String remoteIP = SendEMail.remoteIP;
				
				//For Money's Production machine
		 		if(remoteIP == null || remoteIP.equalsIgnoreCase("null"))
		 		{
		 			filePath = System.getenv("JENKINS_HOME") + "/jobs/InternalExecution/workspace/Common/Parameters";
		 		}
		 		else
		 		{
		 			filePath = "\\\\"+remoteIP + "\\Users\\payu\\.hudson\\jobs\\InternalExecution\\workspace\\Common\\Parameters";
		 		}
			 				 	
				MimeMultipart multipart = new MimeMultipart("related");
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(messageContent, "text/html; charset=UTF-8");
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource fds1 = new FileDataSource(filePath+File.separator+"up.png");
				messageBodyPart.setDataHandler(new DataHandler(fds1));
				messageBodyPart.addHeader("Content-ID", "<image1>");
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource fds2 = new FileDataSource(filePath+File.separator+"down.png");
				messageBodyPart.setDataHandler(new DataHandler(fds2));
				messageBodyPart.addHeader("Content-ID", "<image2>");
				multipart.addBodyPart(messageBodyPart);
				messageBodyPart = new MimeBodyPart();
				DataSource fds3 = new FileDataSource(filePath+File.separator+"equal.png");
				messageBodyPart.setDataHandler(new DataHandler(fds3));
				messageBodyPart.addHeader("Content-ID", "<image3>");
				multipart.addBodyPart(messageBodyPart);
				message.setContent(multipart);
				break;
			case NormalText:
				message.setText(messageContent); 
				break;
			default:
				System.out.println("Email Content type not defined");
			}


			try
			{
				Transport.send(message); 
			}
			catch(Exception e)
			{
				e.printStackTrace();
				System.out.println("********** This is the Message we were trying to send via Email **********\n");
				System.out.println("Subject : "+message.getSubject());
				try 
				{
					System.out.println("Message : "+message.getContent().toString());
				} 
				catch (IOException e1){}
				return false;
			}

			return true;
		} 
		catch (MessagingException | UnsupportedEncodingException mex) 
		{ 
			mex.printStackTrace();
			return false;
		} 
	}

	/**
	 * This Method is used to determine IP of the Machine on which Test Cases are running by using its VPN IP
	 * @param ipAddress	-- This is the VPN IP's of the machine
	 * @param resultPath -- This variable will have the path where results are stored
	 * @return
	 */
	public static String determineMachineIPAddress(String ipAddress, String resultPath)
	{
		String FQName = null;
		switch(ipAddress)
		{
		case "10.50.9.165":
			FQName = "http://10.100.75.17/" + resultPath;
			break;
		case "10.50.6.190":
			FQName = "http://10.100.76.116/" + resultPath;
			break;
		case "10.50.9.253":
			FQName = "http://10.100.32.25/" + resultPath;
			break;
		default:
			FQName = "http://10.100.75.64/" +  resultPath;
		}
		return FQName;
	}

	/**
	 * This method is used for Generating and Sending Email Report of the Automation Run after it is completed
	 * @param resultpath
	 * @param IP_Address
	 * @param projectName
	 * @param suiteName
	 * @param environment
	 * @param toEmail
	 * @param testerName
	 * @param platformName
	 * @param browserName
	 * @param browserVersion
	 * @param platformRowNumber
	 * @param remoteExecution
	 * @return
	 * @throws SQLException 
	 */
	public static boolean emailAfterTLExecution(String xmlPath, String resultpath, String IP_Address, String projectName, String suiteName, String environment,String toEmail, String testerName, String platformName, String browserName, String browserVersion, String platformRowNumber,boolean remoteExecution,String parentJobName, String parentBuildId) throws SQLException
	{
		String strText = null;
		String replyTo = null;
		String subject = null;
		String FQName = SendEMail.determineMachineIPAddress(IP_Address, resultpath);
		String savedResultPath = null;
		String resultLink = FQName + "/index.html";
		buildId = null;
		
		// If we are generating report for a remote Execution then select resultPath accordingly
		if (remoteExecution)
			savedResultPath = "\\\\" + remoteIP + "\\RegressionResults\\";
		else if (!SendEMail.osType.startsWith("Window"))
			savedResultPath = "/Volumes/RegressionResults/";
		else
			savedResultPath = "C:/RegressionResults/";
		
		Pattern p = Pattern.compile("(\\d+)");
		Matcher m = p.matcher(resultpath);
		if (m.find())
			buildId = m.group(1);
		else
			buildId = "";
		int id = Integer.parseInt(buildId);

		savedResultPath = savedResultPath + resultpath;
		
	 	if (osType.startsWith("Window"))
	 		savedResultPath = savedResultPath.replaceAll("/","\\\\");
		
		File file = new File(savedResultPath+ File.separator +"overview.html");
		try
		{
			// Modifying Email Body Message
			strText = FileUtils.readFileToString(file);
			strText = strText.replaceAll("output.html", resultLink);
			strText = strText.replaceAll("href=\"suite", "href=\"" +FQName + "/suite");
			strText = strText.replaceAll("Log Output", "Full Report");
			//Modify emailContent to add Accuracy related columns
			strText = ForAccuracy.addAccuracyColumns(strText, projectName);
			String dbTableName = "";

			// Run only for Regression Run
			if(parentJobName != null && parentJobName.equalsIgnoreCase("RegressionRun"))
			{
				//SendEMail.fetchAndSaveSuitResults(strText,resultLink,suiteName,testerName,environment,browserName,previousDayFile);
				SendEMail.fetchAndSaveSuitResults(strText,resultLink,suiteName,testerName,environment,browserName,projectName,id);
			}			
			// Fetch Suit Pass Percentage
			Pattern pattern = Pattern.compile("class=\"passRate suite\">\\s+(\\d+)");
			Matcher matcher = pattern.matcher(strText);
			if (matcher.find())
			{
				//strPercentage = matcher.group(1);
				strPercentage = ForAccuracy.accuracyPercent.replace("%", "");
				
				// Determine the reply to email id
				if(projectName.equalsIgnoreCase("Product"))
				{
					projectName = "Biz";
					dbTableName = "productclasslevelreport";
					replyTo  = "PayUPG_QC@payu.in";
				}
				else if (projectName.equalsIgnoreCase("bizmobileautomation"))
				{
					projectName = "Biz Mobile";
					dbTableName = "bizmobileclasslevelreport";
					replyTo  = "PayUPG_QC@payu.in";
				}
				else if (projectName.equalsIgnoreCase("bankingproduct"))
				{
					projectName = "Biz Banking Product";
					dbTableName = "bankingproductclasslevelreport";
					replyTo  = "PayUPG_QC@payu.in";
				}
				else
				{
					dbTableName = "moneyclasslevelreport";
					replyTo  = "payumoneyqc@payu.in";
				}

				if(parentJobName.equalsIgnoreCase("RegressionRun") || parentJobName.equalsIgnoreCase("RunByArea"))
				{
					// code to read and parse result xml files and insert into automation db
					ReadResultXMLAndInsertInDB readResultXMLAndInsertInDB = new ReadResultXMLAndInsertInDB();
					readResultXMLAndInsertInDB.readFileAndPerformInsertion(xmlPath, suiteName,  dbTableName,testerName, parentJobName,parentBuildId);

					// Get hashmap containing (key,value) pair for (groupName,changePercent)
					HashMap<String, String> resultHashMap = getMapDataForClassLevelReport(String.valueOf(parentBuildId),suiteName, dbTableName, parentJobName);

					// Add change percent column in the report
					strText = addColumnForClassLevelChange(strText, resultHashMap, dbTableName,parentJobName, parentBuildId,suiteName );
				}				
				// Create Subject for Email
				if (platformRowNumber.equalsIgnoreCase("-1"))
					subject = strPercentage + "%: PayU" + projectName + "-" + suiteName + " on " + environment + " (Owner-" + testerName +")";
				else if(platformRowNumber.equalsIgnoreCase("-1") && platformName.equalsIgnoreCase("NULL"))
					subject = strPercentage + "%: PayU" + projectName + "-" + suiteName + " on " + environment + " IP:" +  IP_Address  + "_" +  browserName  + " (Owner-" + testerName + ")"; 
				else
					subject = strPercentage + "%: PayU" + projectName + "-" + suiteName + " on " + environment + " Platform:" + platformName + "_" + browserName + "" + browserVersion + " (Owner-" + testerName + ")";

				// Send Email
				return SendEMail.SendMail(toEmail, replyTo, subject, strText,EmailContentType.HTML,ReportType.Automation);
			}
			else
			{
				System.out.println("Not able to read percentage from report");
			}
		}
		catch (IOException e)
		{
			System.out.println(e.getStackTrace());
			e.printStackTrace();
		}
		catch (Exception e) {
			System.out.println(e.getStackTrace());
			e.printStackTrace();
		}
		return false;
	}


	/**
	 * Method to get hashmap containing group name and its area change percentage of today run and previous run
	 * 
	 * @param parentJobBuildId
	 * @param suiteName
	 * @param dbTableName
	 * @param parentJobName
	 * @return
	 * @throws IOException
	 * @throws SQLException
	 */
	private static HashMap<String, String> getMapDataForClassLevelReport(String parentJobBuildId,
			String suiteName, String dbTableName, String parentJobName) throws IOException, SQLException {

		String previousdayBuildIdOfParentJob = "";
		String groupName = "";
		String queryForPreviousBuild = "";
		int id = Integer.parseInt(parentJobBuildId);
		String query = getQueryToExecute(QueryType.selectDataForbuildIdAndAreaName);
		query = query.replace("tableName", dbTableName).replace("suiteName", suiteName).replace("buildId", parentJobBuildId).replace("parentJobName", parentJobName);
		ResultSet resultSetForTodayBuild = DBHelperForAccuracyDB.executeSelectQuery(query, DatabaseType.AutomationDB);

		HashMap<String, String> hashMapForTodayBuild = new HashMap<String, String>();
		HashMap<String, String> hashMapForPreviousBuild = new HashMap<String, String>();
		HashMap<String, String> resultantHashMap = new HashMap<String, String>();
		ResultSet resultSetForTestGroup = null;
		previousdayBuildIdOfParentJob = getPreviousdayBuildIdOfParentJob(id, dbTableName, suiteName, parentJobName);

		ArrayList<String> arrayListForTodayBuild = new ArrayList<String>(); 

		while(resultSetForTodayBuild.next())
		{
			arrayListForTodayBuild.add(resultSetForTodayBuild.getString("GroupName"));
			String queryToGetTCDataForGroup = getQueryToExecute(QueryType.getTCCountForGroupName);
			queryToGetTCDataForGroup = queryToGetTCDataForGroup.replace("tableName", dbTableName).replace("groupName", resultSetForTodayBuild.getString("GroupName")).replace("buildId", parentJobBuildId).replace("parentJobName", parentJobName);
			resultSetForTestGroup = DBHelperForAccuracyDB.executeSelectQuery(queryToGetTCDataForGroup, DatabaseType.AutomationDB);

			double passPer = getPassPercentage(resultSetForTestGroup);

			hashMapForTodayBuild.put(resultSetForTodayBuild.getString("GroupName"), String.valueOf(passPer));
		}
		
		
		for(int i = 0; i < arrayListForTodayBuild.size() ; i++)
		{
			groupName = arrayListForTodayBuild.get(i);
			queryForPreviousBuild = getQueryToExecute(QueryType.getTCCountForGroupName);
			queryForPreviousBuild = queryForPreviousBuild.replace("tableName", dbTableName).replace("groupName", groupName).replace("buildId", previousdayBuildIdOfParentJob).replace("parentJobName", parentJobName);
			ResultSet resultSetForPreviousBuildTCCount = DBHelperForAccuracyDB.executeSelectQuery(queryForPreviousBuild, DatabaseType.AutomationDB);

			double passPer = getPassPercentage(resultSetForPreviousBuildTCCount);
			hashMapForPreviousBuild.put(groupName, String.valueOf(passPer));

		}	

		for(int i = 0; i < arrayListForTodayBuild.size() ; i++)
		{
			groupName = arrayListForTodayBuild.get(i);
			Double todayPassPer =  Double.parseDouble(hashMapForTodayBuild.get(groupName));
			Double previousPassPer = Double.parseDouble(hashMapForPreviousBuild.get(groupName));
			Double areaPassPer = todayPassPer - previousPassPer;
			try{
				DecimalFormat df = new DecimalFormat("###.##");
				areaPassPer = Double.parseDouble(df.format(areaPassPer));
			} catch (NumberFormatException e) {
				//Set default value of 0.00 in case of Number format exception
				areaPassPer = 0.00;
			}
			resultantHashMap.put(groupName, String.valueOf(areaPassPer));
		}

		return resultantHashMap;
	}

	/**
	 * Method to get the area pass percentage on the basis of result set
	 * 
	 * @param resultSet
	 * @throws NumberFormatException
	 * @throws SQLException
	 */
	private static double getPassPercentage(ResultSet resultSet) throws NumberFormatException, SQLException{

		int passCount = 0, skipCount = 0, failedCount = 0, totalCount = 0;
		double passPer = 0.00;
		try{
			resultSet.absolute(1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		try
		{
			passCount = Integer.parseInt(resultSet.getObject(1).toString());
			skipCount = Integer.parseInt(resultSet.getObject(2).toString());
			failedCount = Integer.parseInt(resultSet.getObject(3).toString());
			totalCount = passCount + skipCount + failedCount;

			DecimalFormat df = new DecimalFormat("###.##");
			passPer = (passCount * 100.00)/totalCount ;

			passPer = Double.parseDouble(df.format(passPer));
		}
		catch(Exception e)
		{
			passPer = 0.00;
			e.printStackTrace();
		}

		return passPer;
	}

	/**
	 * Code to get previous day Build Id Of Parent Job
	 * 
	 * @param id
	 * @param dbTableName
	 * @param suiteName
	 * @param parentJobName
	 * @throws IOException
	 * @throws SQLException
	 */
	private static String getPreviousdayBuildIdOfParentJob(int id,
			String dbTableName, String suiteName, String parentJobName) throws IOException, SQLException {

		String previousdayBuildIdOfParentJob = "";
		String queryForPreviousBuild = "";
		while(id>0)
		{
			--id;
			previousdayBuildIdOfParentJob = String.valueOf(id);
			queryForPreviousBuild = getQueryToExecute(QueryType.selectDataForbuildIdAndAreaName);
			queryForPreviousBuild = queryForPreviousBuild.replace("tableName", dbTableName).replace("suiteName", suiteName).replace("buildId", previousdayBuildIdOfParentJob).replace("parentJobName", parentJobName);	
			ResultSet rs = DBHelperForAccuracyDB.executeSelectQuery(queryForPreviousBuild, DatabaseType.AutomationDB);
			if(rs.next())
			{
				break;
			}

		}
		return previousdayBuildIdOfParentJob;
	}

	/**
	 * This Method is used to fetch current Total result for a suit and save it in a Excel File
	 * @param reportText
	 * @param consolidateReportFilePath
	 * @param resultLink
	 * @param suiteName
	 * @param testerName
	 * @param environment
	 * @param browserName
	 * @param previousDayFile
	 * @throws IOException
	 * @throws SQLException 
	 */
	public static void fetchAndSaveSuitResults(String reportText,String resultLink, String suiteName, String testerName, String environment, String browserName, String projectName, int buildId) throws IOException, SQLException
	{
		/**
		 * Step 1: Extract Data from String reportText
		 * Below code is used to fetch Total Passed / Skipped / Failed /Pass Per count of a Test Suit from reportText using pattern passCountRefText
		 */
		String passCountRefText = "class=\"totalLabel\">";
		int a = reportText.lastIndexOf(passCountRefText);
		String str = reportText.substring(a);
		Pattern p = Pattern.compile("(\\d+)");
		Matcher m = p.matcher(str);
		String passCount = null;
		String skippedCount = null;
		String failedCount = null;
		String passPer = null;
		String KnownFailures = null;
		String accuracy = null;
		int i = 1;
		while(m.find())
		{
			switch(i)
			{
			case 1:
				passCount = m.group(1);
				break;
			case 2:
				skippedCount = m.group(1);
				break;
			case 3:
				failedCount = m.group(1);
				break;
			case 4:
				passPer = m.group(1);
				break;
			case 5:
				KnownFailures = m.group(1);
				break;
			case 6:
				accuracy = m.group(1);
				break;
			default:
				System.out.println("Error in fetching Result Count : "+ i);
			}
			i++;
		}

		String tableName = "";
		if(projectName.equalsIgnoreCase("Product"))
			tableName = "productconsolidatedreport";
		else if (projectName.equalsIgnoreCase("bizmobileautomation"))
			tableName = "bizmobileconsolidatedreport";
		else if (projectName.equalsIgnoreCase("bankingproduct"))
			tableName = "bankingproductconsolidatedreport";
		else
			tableName  = "moneyconsolidatedreport";
			

		String queryToExecute = getQueryToExecute(QueryType.insertDataForTodayBuild);

		queryToExecute = queryToExecute.replace("TableName", tableName).replace("suiteName", suiteName).replace("passCount", passCount).replace("skippedCount", skippedCount).
				replace("failedCount", failedCount).replace("passPer", passPer).replace("areaChange", "").replace("knownFailures", KnownFailures).
				replace("accuracy", accuracy+"%").replace("testerName", testerName).
				replace("browserName", browserName).replace("environment", environment).replace("resultLink", resultLink).replace("buildId", String.valueOf(buildId));

		DBHelperForAccuracyDB.executeQuery(queryToExecute , DatabaseType.AutomationDB);

	}


	/**
	 * Getting query to be executed :- either insert to enter today's result or select previous day results 
	 * 	
	 * @param queryType
	 * @return
	 * @throws IOException
	 */
	private static String getQueryToExecute(QueryType queryType) throws IOException {

		String query = null;
		String filePath = "";
		if(SendEMail.executionOnRemoteAddress)
		{
			filePath = "\\\\"+SendEMail.remoteIP + "\\Users\\payu\\"+"."+"hudson\\jobs\\InternalExecution\\workspace\\Common" + "\\" + "Parameters\\" + "CommonTestData.xls";
		}
		else
		{
			filePath = System.getenv("JENKINS_HOME") + "/jobs/InternalExecution/workspace/Common/Parameters/CommonTestData.xls";
		}

		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);

		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet = workbook.getSheet("SQL");

		switch (queryType) {
		case insertDataForTodayBuild:
			query = sheet.getRow(6).getCell(2).toString();
			break;

		case selectDataForAreaName: 
			query = sheet.getRow(7).getCell(2).toString();
			break;

		case selectDataForBuildId:
			query = sheet.getRow(8).getCell(2).toString();
			break;

		case selectDataForPreviousRun:
			query = sheet.getRow(9).getCell(2).toString();
			break;

		case selectDataForbuildIdAndAreaName:
			query = sheet.getRow(12).getCell(2).toString();
			break;

		case selectDataForbuildIdAndGroupNameForPreviousBuild:
			query = sheet.getRow(13).getCell(2).toString();
			break;

		case updateResultLinkField:
			query = sheet.getRow(14).getCell(2).toString();
			break;

		case getTCCountForGroupName:
			query = sheet.getRow(15).getCell(2).toString();
			break;

		default:
			break;
		}

		return query;
	}	

	/**
	 * This Method is used for generating and Sending Consolidate Report for Complete Regression Run
	 * @param resultpath
	 * @param projectName
	 * @param testPlanName
	 * @param intialValueOfArea
	 * @param remoteExecution
	 * @return
	 * @throws IOException 
	 * @throws SQLException 
	 */
	public static boolean generateAndSendConsolidateReportEmail(String projectName,boolean remoteExecution,String groupAreaSheetName) throws IOException, SQLException
	{
		String replyTo = null;
		String toEmail = null;
		String bodyHeader = "";
		String subject = "";
		String body  = "";
		String previousdayBuildId = "";
		String tableName = "";
		
		if(!remoteExecution)
		{
			SendEMail.remoteIP = null;
			SendEMail.executionOnRemoteAddress = false;
		}


		// Deciding the to/Replyto Email id based on Project Name
		if(projectName.equalsIgnoreCase("Product"))
		{
			projectName = "Biz";
			toEmail = "PayUPG_QC@payu.in,PayU_PG_Tech@payu.in";
			replyTo  = "PayUPG_QC@payu.in";
			tableName = "productconsolidatedreport";
		}
		else if (projectName.equalsIgnoreCase("bizmobileautomation"))
		{
			projectName = "Biz Mobile";
			toEmail = "PayUPG_QC@payu.in,PayU_PG_Tech@payu.in";
			replyTo  = "PayUPG_QC@payu.in";
			tableName = "bizmobileconsolidatedreport";
		}
		else if (projectName.equalsIgnoreCase("bankingproduct"))
		{
			
			projectName = "Biz Banking Product";
			toEmail = "PayUPG_QC@payu.in,PayU_PG_Tech@payu.in";
			replyTo  = "PayUPG_QC@payu.in";
			tableName = "bankingproductconsolidatedreport";
		}
		else
		{
			toEmail = "mukesh.rajput@payu.in,payumoneyqc@payu.in,payumoneyproduct@payu.in";
			replyTo  = "payumoneyqc@payu.in";
			tableName = "moneyconsolidatedreport";
		}

		String parentBuildId = GenerateTestNGXmlAndRun.getParentBuildID("RegressionRun");
		int id = Integer.parseInt(parentBuildId);

		while(id>0)
		{
			--id;
			previousdayBuildId = Integer.toString(id);
			String query = getQueryToExecute(QueryType.selectDataForBuildId);
			query = query.replace("TableName", tableName).replace("buildId", previousdayBuildId);
			ResultSet rs = DBHelperForAccuracyDB.executeSelectQuery(query, DatabaseType.AutomationDB);
			if(rs.next())
				break;
		}

		ArrayList<String> arrayListForExecutedAreas = new ArrayList<String>(); 
		String query = getQueryToExecute(QueryType.selectDataForBuildId);
		query = query.replace("TableName", tableName).replace("buildId", parentBuildId);
		ResultSet resultSet1 = DBHelperForAccuracyDB.executeSelectQuery(query, DatabaseType.AutomationDB);

		while(resultSet1.next())
		{
			arrayListForExecutedAreas.add(resultSet1.getString("SuiteName"));
		}

		
		ArrayList<String> listOfAllAreaNames = getArrayListOfAllAreas(groupAreaSheetName);
		ArrayList<String> areaNamesNotExecuted = new ArrayList<>(listOfAllAreaNames);
		areaNamesNotExecuted.removeAll(arrayListForExecutedAreas);

		// Remove AndroidCustomBrowser-Version1 and IOSCustomBrowser-Version1 from consolidated report
		// These areas should be present in sheet2 for area-wise run but should not be aprt of consolidated report
		areaNamesNotExecuted.remove("AndroidCustomBrowser-Version1");
		areaNamesNotExecuted.remove("IOSCustomBrowser-Version1");

		String htmlTextColumns = "";
		String htmlTextHeader = "";
		String browserName = "";
		String enviornment = "";
		int totalPassCount = 0,totalskipCount = 0,totalfailCount = 0, totalknownFailedCases = 0;
		double totalAccuracy = 0;

		for(int j = 0 ; j <= arrayListForExecutedAreas.size() - 1; j++)
		{
			htmlTextColumns = htmlTextColumns + "<tr>";

			String queryToExecute = getQueryToExecute(QueryType.selectDataForAreaName);
			queryToExecute = queryToExecute.replace("TableName", tableName).replace("suiteName", arrayListForExecutedAreas.get(j).toString());
			ResultSet resultSet = DBHelperForAccuracyDB.executeSelectQuery(queryToExecute, DatabaseType.AutomationDB);	
			try
			{
				resultSet.absolute(1);
			}
			catch (SQLException e)
			{
				System.out.println("No result found for this query");
				e.printStackTrace();
			}

			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			int columnsCount = resultSetMetaData.getColumnCount();
			HashMap<String, String> hashMap = new HashMap<String,String>();

			for(int i=1; i <= columnsCount; i++)
			{
				hashMap.put(resultSetMetaData.getColumnName(i), resultSet.getObject(i).toString());
			}

			htmlTextColumns = htmlTextColumns + "<tr>";

			if(j == 0)
			{
				for(int i=2; i <= columnsCount - 5; i++)
				{
					htmlTextHeader = htmlTextHeader + "<th>" + resultSetMetaData.getColumnName(i) +"</th>"  + "<th></th>";
				}
				htmlTextHeader = htmlTextHeader + "</tr>";
			}
			htmlTextColumns = htmlTextColumns + "<tr>";

			for(int i=2; i <= columnsCount - 5; i++)
			{
				int passCount =  Integer.parseInt(resultSet.getObject(3).toString());
				int skippedCount = Integer.parseInt(resultSet.getObject(4).toString());
				int failedCount = Integer.parseInt(resultSet.getObject(5).toString());
				int KnownFailures = Integer.parseInt(resultSet.getObject(8).toString());
				int totalCount = passCount + skippedCount+ failedCount;

				switch (resultSetMetaData.getColumnName(i)) {
				case "SuiteName":
					String resultLink = hashMap.get("ResultLink");
					htmlTextColumns = htmlTextColumns + "<td> <a href ='" + resultLink + "'>" + resultSet.getObject(i) + "</td><td></td>";
					break;

				case "AreaPassPer":
					String curAreaName = resultSet.getObject(2).toString();
					double currPerChange = 0.00;

					DecimalFormat df = new DecimalFormat("###.##");
					double passPer = (passCount * 100.00)/totalCount ;
					passPer = Double.parseDouble(df.format(passPer));
					htmlTextColumns = htmlTextColumns + "<td align = 'right'>" + passPer + "%" + "</td><td></td>"; 

					htmlTextColumns = compareWithYesterdayReport(htmlTextColumns,passPer,curAreaName,currPerChange,tableName);

					totalPassCount += passCount;
					totalskipCount += skippedCount;
					totalfailCount += failedCount;
					totalknownFailedCases = totalknownFailedCases + KnownFailures; 
					break;

				case "AreaChangePer":
					continue;

				case "Accuracy":
					double accuracy = ((passCount+KnownFailures)*100.00)/totalCount;
					df = new DecimalFormat("0.00");
					String accuracyPercent = df.format(accuracy)+"%";
					htmlTextColumns = htmlTextColumns + "<td align = 'right'>" + accuracyPercent + "</td><td></td>";
					break;

				case "Tester":
					htmlTextColumns = htmlTextColumns + "<td>" + resultSet.getObject(i) + "</td><td></td>";
					break;
				default:
					htmlTextColumns = htmlTextColumns + "<td align = 'right'>" + resultSet.getObject(i) + "</td><td></td>";
					break;
				}

			}
			browserName = resultSet.getObject(11).toString();
			enviornment = resultSet.getObject(12).toString();
		}	
		// Calculate Consolidate Pass Percentage for Today's run
		int total = totalPassCount + totalfailCount + totalskipCount;

		//Calculate Consolidate Accurate Percentage for Today's run
		totalAccuracy = ((totalPassCount + totalknownFailedCases)*100.00)/total;
		DecimalFormat dff = new DecimalFormat("0.00");
		String accuracyPercent = dff.format(totalAccuracy)+"%";

		double passPerTotalGen = (totalPassCount * 100.00)/total;
		DecimalFormat df = new DecimalFormat("###.##");

		try{
			passPerTotalGen = Double.parseDouble(df.format(passPerTotalGen));
		} catch (NumberFormatException e) {
			//Set default value of 0.00 in case of Number format exception
			passPerTotalGen = 0.00;
		}

		String queryForPreviousBuild = getQueryToExecute(QueryType.selectDataForPreviousRun);
		queryForPreviousBuild = queryForPreviousBuild.replace("TableName", tableName).replace("previousdayBuildId", previousdayBuildId);
		ResultSet countSet = DBHelperForAccuracyDB.executeSelectQuery(queryForPreviousBuild, DatabaseType.AutomationDB);

		countSet.absolute(1); //Getting first row data of a resultSet

		int yesPassCount = Integer.parseInt(countSet.getString("passCount"));
		int yesSkippedCount = Integer.parseInt(countSet.getString("skippedCount"));
		int yesFailedCount = Integer.parseInt(countSet.getString("failedCount"));
		int yesTotalCount = yesPassCount + yesSkippedCount + yesFailedCount;
		double yesPassPer = (yesPassCount*100.00)/yesTotalCount;

		yesPassPer = passPerTotalGen - yesPassPer;
		try{
			yesPassPer = Double.parseDouble(df.format(yesPassPer));
		} catch (NumberFormatException e) {
			//Set default value of 0.00 in case of Number format exception
			yesPassPer = 0.00;
		}

		for(int x = 0; x < areaNamesNotExecuted.size(); x++)
		{
			htmlTextColumns = htmlTextColumns + "<tr> <td><B>" + areaNamesNotExecuted.get(x).toString()  +"</td><td></td> <td align = 'right'>" + "NR" + "</td><td></td> <td align = 'right'>" + "NR" + "</td><td></td> <td align = 'right'>" + "NR" + "</td><td></td> <td align = 'right'> " + "NR" + "</td><td></td><td align = 'right'>"  + "NR" + "</td><td></td><td align = 'right'> " + "NR" + "</td><td></td><td align = 'right'> " + "NR" + "</td><td></td></tr>";
		}

		if(yesPassPer>0)
		{
			htmlTextColumns = htmlTextColumns + "<tr> <td><B>Total</td><td></td> <td align = 'right'><B>" + totalPassCount + "</td><td></td> <td align = 'right'><B>" + totalskipCount + "</td><td></td> <td align = 'right'><B>" + totalfailCount + "</td><td></td> <td align = 'right'> <B>" + passPerTotalGen+"%" + "</td><td></td><td align = 'right'> <B>"  + yesPassPer+"%" + "<img src=\"cid:image1\">" + "</td><td></td><td align = 'right'> <B>" + totalknownFailedCases + "</td><td></td><td align = 'right'> <B>" + accuracyPercent + "</td><td></td></tr>";
		}
		else if(yesPassPer<0)
		{
			htmlTextColumns = htmlTextColumns + "<tr> <td><B>Total</td><td></td> <td align = 'right'><B>" + totalPassCount + "</td><td></td> <td align = 'right'><B>" + totalskipCount + "</td><td></td> <td align = 'right'><B>" + totalfailCount + "</td><td></td> <td align = 'right'> <B>" + passPerTotalGen+"%" + "</td><td></td><td align = 'right'> <B>"  + yesPassPer+"%" + "<img src=\"cid:image2\">" + "</td><td></td><td align = 'right'> <B>" + totalknownFailedCases + "</td><td></td><td align = 'right'> <B>" + accuracyPercent + "</td><td></td></tr>";
		}
		else
			htmlTextColumns = htmlTextColumns + "<tr> <td><B>Total</td><td></td> <td align = 'right'><B>" + totalPassCount + "</td><td></td> <td align = 'right'><B>" + totalskipCount + "</td><td></td> <td align = 'right'><B>" + totalfailCount + "</td><td></td> <td align = 'right'> <B>" + passPerTotalGen+"%" + "</td><td></td><td align = 'right'> <B>" + yesPassPer+"%" + "<img src=\"cid:image3\">" + "</td><td></td><td align = 'right'> <B>" + totalknownFailedCases + "</td><td></td><td align = 'right'> <B>" + accuracyPercent + "</td><td></td></tr>";

		bodyHeader = "PayU"+ projectName +" Consolidated Report";
		subject  = passPerTotalGen + "% "+ bodyHeader + " on " + enviornment + " " + browserName;
		body = "<div>" + "" + "</div><h1>" + bodyHeader + "</h1><table><tr><th colspan=18>PayU-" + projectName + "Automation</th></tr><tr>";
		body = body + htmlTextHeader + htmlTextColumns + "</table>";

		return SendEMail.SendMail(toEmail, replyTo, subject, body, EmailContentType.HTML,ReportType.Consolidate);
	}

	/**
	 * Method to get all group names from excel sheet and enter the same in ArrayList
	 * @throws IOException 
	 * 
	 */
	private static ArrayList<String> getArrayListOfAllAreas(String groupAreaSheetName) throws IOException 
	{
			
		String filePath = null;
		if(SendEMail.executionOnRemoteAddress)
			filePath = "\\\\"+SendEMail.remoteIP + "\\Users\\payu\\"+"."+"hudson\\jobs\\InternalExecution\\workspace\\"+SendEMail.projectNameToSendEmail+"\\"+SendEMail.projectNameToSendEmail+"GroupNames.xls";
		else
			filePath = System.getenv("JENKINS_HOME") + File.separator + "jobs" + File.separator + "InternalExecution" + File.separator + "workspace" + File.separator + SendEMail.projectNameToSendEmail + File.separator + SendEMail.projectNameToSendEmail + "GroupNames.xls";

		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);

		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet;
		// Reading Different Area Sheet if specified
		if(groupAreaSheetName!= null && groupAreaSheetName.equalsIgnoreCase("Sheet2"))
			sheet = workbook.getSheet("Sheet3");
		else
			sheet = workbook.getSheet("Sheet2");
			

		int rowCount = sheet.getLastRowNum();
		String areaName = "";
		ArrayList<String> listOfAllareaNames = new ArrayList<String>();

		for(int i = 1; i <= rowCount ; i++ )
		{
			areaName = sheet.getRow(i).getCell(0).toString();
			listOfAllareaNames.add(areaName);
		}

		return listOfAllareaNames;
	}

	/**
	 * Method to compare with yesterday's report
	 * 
	 * @param workbook
	 * @param htmlTextColumns
	 * @param passPer
	 * @param curAreaName
	 * @param currPerChange
	 * @return
	 * @throws SQLException
	 * @throws IOException
	 */
	private static String compareWithYesterdayReport(String htmlTextColumns, double passPer, String curAreaName, double currPerChange, String tableName) throws SQLException, IOException
	{
		String queryToExecute = getQueryToExecute(QueryType.selectDataForAreaName);
		queryToExecute = queryToExecute.replace("TableName", tableName).replace("suiteName", curAreaName);

		ResultSet resultSet = DBHelperForAccuracyDB.executeSelectQuery(queryToExecute, DatabaseType.AutomationDB);				

		int count = 0;
		String htmlcode = "";

		while(resultSet.next())
		{
			count++;
		}

		if(count == 2)
		{
			resultSet.absolute(2);
			ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
			int columnsCount = resultSetMetaData.getColumnCount();
			HashMap<String, String> hashMap = new HashMap<String,String>();

			for(int i=1; i <= columnsCount; i++)
			{
				hashMap.put(resultSetMetaData.getColumnName(i), resultSet.getObject(i).toString());
			}
			String resultLinkYesterday = hashMap.get("ResultLink");
			double passCountYesterday = Double.parseDouble(resultSet.getObject(3).toString());
			double skippedCountYesterday = Double.parseDouble(resultSet.getObject(4).toString());
			double failedCountYesterday = Double.parseDouble(resultSet.getObject(5).toString());
			double totalCountYesterday = passCountYesterday + skippedCountYesterday+ failedCountYesterday;
			double passPerCountYest =  (passCountYesterday/totalCountYesterday) * 100.00;
			
			DecimalFormat df = new DecimalFormat("###.##");
			passPerCountYest = Double.parseDouble(df.format(passPerCountYest));

			if (passPerCountYest == passPer)
			{
				currPerChange = 0.00;
				currPerChange = Double.parseDouble(df.format(currPerChange));
				htmlcode = "<td align = 'right'>" + "<a href ='" + resultLinkYesterday + "'>"+ currPerChange + "%" + "<img src=\"cid:image3\">" + "</td><td></td>"; 
			}
			else if (passPerCountYest > passPer)
			{
				currPerChange = passPerCountYest - passPer;
				currPerChange = Double.parseDouble(df.format(currPerChange));
				htmlcode = "<td align = 'right'>" + "<a href ='" + resultLinkYesterday + "'> -"+ currPerChange + "%" + "<img src=\"cid:image2\">" + "</td><td></td>";
			}
			else
			{
				currPerChange = passPer - passPerCountYest;
				currPerChange = Double.parseDouble(df.format(currPerChange));
				htmlcode = "<td align = 'right'>" + "<a href ='" + resultLinkYesterday + "'> "+ currPerChange + "%" + "<img src=\"cid:image1\">" + "</td><td></td>";
			}
		}
		else
		{
			htmlcode = "<td align = 'right'>" + "<a href ='" + "" + "'> "+ passPer + "%" + "<img src=\"cid:image1\">" + "</td><td></td>";
		}
		htmlTextColumns = htmlTextColumns +	htmlcode;	

		return htmlTextColumns;
	}

	/**
	 * This Method is for sending Consolidate Report		
	 * @throws SQLException 
	 * 
	 */
	public static void main(String args[])throws IOException, JSONException, InstantiationException, IllegalAccessException, SQLException
	{
		String projectName = args[0];
		String remoteIP = args[1];
		String areaGroupSheetName = args[2];
		boolean remoteExecution = false;
		SendEMail.projectNameToSendEmail = projectName;
		if (remoteIP!= null && !remoteIP.isEmpty() && !remoteIP.equalsIgnoreCase("local") && !remoteIP.equalsIgnoreCase("null"))
		{
			remoteExecution = true;
			SendEMail.remoteIP = remoteIP;
		}
		else
		{
			remoteExecution = false;
		}

		boolean status = generateAndSendConsolidateReportEmail(projectName,remoteExecution,areaGroupSheetName);
		if (status)
			System.out.println("Consolidated Report Send Successfully");
		else
			System.out.println("Error in Sending Consolidated Report!! Please Check logs for more details");
	}


	/**
	 * This method is use for append new column in  reporting table
	 * 
	 * @param emailContent
	 * @param hashMap   for Hash Map key should be column name and value should be change percentage
	 * @param parentBuildId 
	 * @param parentJobName 
	 * @param dbTableName 
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws NumberFormatException 
	 */
	public static String addColumnForClassLevelChange(String emailContent,HashMap<String,String> hashMap, String dbTableName, String parentJobName, String parentBuildId, String suiteName) throws IOException, NumberFormatException, SQLException
	{
		//Add a column in report after accuracy column
		emailContent = emailContent.replaceAll("colspan=\"10\"","colspan=\"12\"");

		emailContent = emailContent.replaceAll("<th>Accuracy</th>","<th>Accuracy</th>\n<th>GroupChange%</th>");

		String temp = emailContent;
		Document doc2 = Jsoup.parse(temp);
		String getPercentage = "";
		Document doc1 = Jsoup.parse(emailContent);
		Elements tableElements = doc1.select("table");
		Elements tableRowElements = tableElements.select(":not(thead) tr");

		for (int i = 2; i < tableRowElements.size() -1 ; i++) 
		{
			Element row = tableRowElements.get(i);

			Elements rowItems = row.select("td");

			// Get group name for key pair
			String groupName = rowItems.get(0).text();

			String resultLink = rowItems.get(0).getElementsByAttribute("href").toString();

			resultLink = resultLink.split("\"")[1];

			String updateQuery = getQueryToExecute(QueryType.updateResultLinkField);
			updateQuery = updateQuery.replace("tableName", dbTableName).replace("resultLink", resultLink).
					replace("buildId", parentBuildId).replace("groupName", groupName).replace("parentJobName", parentJobName);

			DBHelperForAccuracyDB.executeQuery(updateQuery, DatabaseType.AutomationDB);
			System.out.println("Update query executed.");
			String previousdayBuildIdOfParentJob = getPreviousdayBuildIdOfParentJob(Integer.parseInt(parentBuildId), dbTableName, suiteName, parentJobName);
			System.out.println("previous day build id of parent job is :- " + previousdayBuildIdOfParentJob);
			String queryForPreviousBuild = getQueryToExecute(QueryType.selectDataForbuildIdAndGroupNameForPreviousBuild);
			queryForPreviousBuild = queryForPreviousBuild.replace("tableName", dbTableName).replace("buildId", previousdayBuildIdOfParentJob).replace("groupName", groupName).replace("parentJobName", parentJobName);
			ResultSet resultSetForPreviousBuild = DBHelperForAccuracyDB.executeSelectQuery(queryForPreviousBuild, DatabaseType.AutomationDB);

			System.out.println("query for previous build is " + queryForPreviousBuild);

			try {
				resultSetForPreviousBuild.absolute(1);
			} catch (Exception e) {
				System.out.println("Empty result set.");
				e.printStackTrace();
			}

			String previousDayResultLink = "";

			try {
				previousDayResultLink = resultSetForPreviousBuild.getString("ResultLink");	
			} catch (Exception e) {
				System.out.println("Empty result set.");
				previousDayResultLink = "";
			}
			
			try{
				getPercentage = hashMap.get(groupName);
			}
			catch(NullPointerException e)
			{
				System.out.println("<--- Value not received --->");
				e.printStackTrace();
			}

			Element classLevelReportChange;
			try
			{
				if(Double.parseDouble(getPercentage)>0)
				{	
					classLevelReportChange = doc2.createElement("td").attr("align","right").html("<a href = " + previousDayResultLink + ">"+getPercentage+"</a>" +"%"+"<img src=\"cid:image1\">");
				}
				else if(Double.parseDouble(getPercentage)<0)
				{
					classLevelReportChange = doc2.createElement("td").attr("align","right").html("<a href = " + previousDayResultLink + ">"+getPercentage+"</a>" +"%"+"<img src=\"cid:image2\">");
				}
				else
				{
					classLevelReportChange = doc2.createElement("td").attr("align","right").html("<a href = " + previousDayResultLink + ">"+getPercentage+"</a>" +"%"+"<img src=\"cid:image3\">");
				}
			}
			catch(Exception e)
			{
				classLevelReportChange = doc2.createElement("td").attr("align","right").html("<a href = " + "" + ">"+getPercentage+"</a>" +"%"+"<img src=\"cid:image1\">");
			}

			Elements tableElements2 = doc2.select("table");
			Elements tableRowElements2 = tableElements2.select(":not(thead) tr");
			Element row2 = tableRowElements2.get(i);
			row2 = row2.appendChild(classLevelReportChange);
		}

		return doc2.toString();
	}

}