package Reporting;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.json.JSONException;
import org.json.JSONObject;

import Utils.Helper;

public class SendSMS {

	/**
	 * To send SMS to QA area owners in case pass percentage is below threshold pass percentage
	 * @param jobName
	 * @param projectName
	 * @param areaName
	 * @param parentJobName
	 * @param environment
	 * @param buildTag
	 */
	public static void sendSMSAfterRegressionExecution(String jobName, String projectName, String areaName, String parentJobName, String environment, String buildTag) 
	{	

		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		
		String thresholdPercentage = "50";
		List<String> mobileNo = null;
		String Pass_percentage;
		
		if(SendEMail.strPercentage==null)
			Pass_percentage = "0";
		else
			Pass_percentage = SendEMail.strPercentage;
		//To get build id from build Tag
		String IE_BuildId= buildTag.replaceAll("[^0-9]", "");
		
		//ProductGroupNames excel used to read mobile numbers and Threshold percentage
		String filePath = System.getenv("JENKINS_HOME") + File.separator + "jobs" + File.separator + jobName + File.separator + "workspace" + File.separator + projectName + File.separator + projectName + "GroupNames.xls";
		
		String IP_Address = null;
		try 
		{
			//To get machine IP address from VPN IP address
			IP_Address = InetAddress.getLocalHost().getHostAddress();
			IP_Address = getMachineIPfromVPNIP(IP_Address);
			
			// Read GroupName Excel file
			FileInputStream file = new FileInputStream(filePath);
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(1);
			int lastRow = sheet.getLastRowNum();
			String regex = "[0-9]+";

			// Iterate through the Sheet 2 of file to find mobile number for the area where current pass percentage is lower then threshold %
			for (int i = 1; i <= lastRow; i++)
			{
				//To get mobile numbers in case pass percentage is lower than threshold percentage
				if((areaName.equalsIgnoreCase(sheet.getRow(i).getCell(0).toString())))
				{
					// Validating threshold % is not null and assigning its default value to 50 if it is null or non numeric
					if ((sheet.getRow(i).getCell(4))!=null)
					{
						sheet.getRow(i).getCell(4).setCellType(Cell.CELL_TYPE_STRING);
						thresholdPercentage = sheet.getRow(i).getCell(4).toString();
						
						//Validating Threshold % to be numeric
						if(!(thresholdPercentage.matches(regex)))
						{
							System.out.println("Threshold defined for area "+ areaName + " is in improper format. Setting threshold to default value of 50.");
							thresholdPercentage = "50";
						}
						else
							thresholdPercentage = sheet.getRow(i).getCell(4).toString();
					}	
					else
					{	
						System.out.println("Threshold defined for area "+ areaName + " is null. Setting threshold to default value of 50.");
						thresholdPercentage = "50";					
					}
					// Validating pass percentage to be less than threshold % to send sms
					if(Double.parseDouble(Pass_percentage)>=Double.parseDouble(thresholdPercentage))
					{
						System.out.println("Success percentage above threshold value hence No SMS sent!");
						return;
					}
					else
					{
						// Validating mobile number to be not equal to null
						if((sheet.getRow(i).getCell(5))!=null)
						{
							mobileNo = Arrays.asList(sheet.getRow(i).getCell(5).toString().split(","));
							break;
						}
						else
						{
							System.out.println("Success percentage below threshold value but no contact details for Area Owner found!!");
							return;
						}
					}
				}
					
				else
					continue;
			}
			
			// Closing open GroupName Excel file
			file.close();
			
			if(mobileNo!=null)
			{
				//Send SMS alert to QA mobile numbers mentioned in ProductGroupNames excel
			
				if(projectName.equalsIgnoreCase("Product"))
					projectName = "Biz";
				else
					projectName = "Money";
				
				Double thresholdPercentageValue = new Double(Double.parseDouble(thresholdPercentage));
				int thrshld_prctng = thresholdPercentageValue.intValue();
				
				// Creating Message to be sent
				String message = projectName+":" + areaName + ","+"Pass%="+Pass_percentage+",Threshold% was "+thrshld_prctng+",Build id="+IE_BuildId+",Env="+environment+",IP:"+IP_Address+",Dated:"+sdf.format(date);
				System.out.println("SMS content is --> " + message);
				
				for (int i = 0; i < mobileNo.size(); i++)
				{
					String mob_num = mobileNo.get(i);
					// Validating any of the mobile number from list is not null
					if(mob_num!=null)
					{
						mob_num=mob_num.trim();
						// Calling function to send SMS
						if(sendTextMessage(mob_num,message))
							System.out.println("SMS alert sent Successfully to: " + mob_num);
						else
							System.out.println("Error in sending SMS alert on given mobile number:"+mob_num+". Please check system logs.");
					}
					else
						System.out.println("Cannot send SMS alert on null mobile number.");
				}
			}
		}
		catch (FileNotFoundException e1)
		{
			System.out.println("GroupName file not found. Please Check!!");
			e1.printStackTrace();
		}
		catch (UnknownHostException e2) 
		{
			System.out.println("Unable to read VPN IP of the Host");
			e2.printStackTrace();
		} 
		catch (IOException e) 
		{
			System.out.println("No Open file found to Close");
			e.printStackTrace();
		}
	}
	
	/**
	 * This method is used to send Text Message to a mobile number
	 * @param mobileNumber
	 * @param message
	 * @return
	 */
	public static boolean sendTextMessage(String mobileNumber, String message)
	{
		String key="C0Dr8m";
		String salt="3sf0jURk";
		String response = "";
		Object returnObj = null;
		int responseSize = 2;
		String postString, hash, request;
		String app_name = "PAYUIB";
		
		postString = key + "|" + "send_sms" + "|" + mobileNumber + "|" + salt;
		hash = Helper.calculateHash(postString);
		request = "key=" + key + "&command=" + "send_sms" + "&hash=" + hash + "&" + "var1=" + mobileNumber + "&var2=" + message + "&var3=" + app_name;
		
		try
		{
			//Create connection and post the request
			URL url = new URL("https://info.payu.in/merchant/postservice.php?form=2");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(request);
			wr.flush();
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;		
			while ((line = rd.readLine()) != null)
			{
				response += line;
			}
			
			// Closing the connection
			wr.close();
			
			returnObj = new JSONObject(response);
			JSONObject responseObj = (JSONObject) returnObj;
			if(responseObj.length() == responseSize)
			{	
				if(responseObj.get("status").toString().equalsIgnoreCase("1") && (responseObj.get("msg").toString().equalsIgnoreCase("Sms sent")))	
					return true;
				else
					return false;
			}
			else
			{
				System.out.println("Response parameters of send sms service did not match. Please check system logs.");
				return false;
			}		
		}
		catch(MalformedURLException e)
		{
			System.out.println("Malformed URL. Please check!!");
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			System.out.println("No Open Connections found to be closed!!");
			e.printStackTrace();
		}
		catch (JSONException e) 
		{
			System.out.println("Please check the Json respone !!");
			e.printStackTrace();
		}
			return true;
	}

	/**
	 * Function to return machine IP address from VPN IP address
	 * @param VPN_IP
	 * @return machine_IP
	 */
	private static String getMachineIPfromVPNIP(String VPN_IP) 
	{
		String machine_IP = null;
		
		switch(VPN_IP)
		{
			case "10.50.6.205":
				machine_IP = "10.100.75.144";
				break;
			case "10.50.6.206":
				machine_IP = "10.100.76.145";
				break;
			case "10.50.6.47":
				machine_IP = "10.100.66.195";
				break;
			case "10.50.9.253":
				machine_IP = "10.100.32.25";
				break;
			case "10.50.9.236":
				machine_IP = "10.100.32.26";
				break;
			case "10.50.9.182":
				machine_IP = "10.100.78.29";
				break;
			case "10.50.6.190":
				machine_IP = "10.100.76.116";
				break;
			case "10.50.6.180":
				machine_IP = "10.100.78.78";
				break;
			case "10.50.9.237":
				machine_IP = "10.100.32.39";
				break;

			case "10.50.9.165":
				machine_IP = "10.100.75.17";
				break;
			case "10.50.9.85":
				machine_IP = "10.100.76.16";
				break;
			case "10.50.9.140":
				machine_IP = "10.100.76.81";
				break;
			case "10.50.9.227":
				machine_IP = "10.100.32.30";
				break;
			case "10.50.6.29":
				machine_IP = "10.100.32.20";
				break;
			case "10.50.6.21":
				machine_IP = "10.100.32.18";
				break;
			case "10.50.6.207":
				machine_IP = "10.100.32.17";
				break;
			default:
				machine_IP = "10.100.75.64";
		}
		return machine_IP;
	}
	
}
