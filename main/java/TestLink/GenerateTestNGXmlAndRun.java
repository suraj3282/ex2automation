package TestLink;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.TestNG;
import org.testng.TestNGException;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import Reporting.SendEMail;
import Reporting.SendSMS;

public class GenerateTestNGXmlAndRun
{
	// Flag to on/off logging
	private boolean debugMode = true;
	
	public static void main(String args[]) throws IOException, JSONException, InstantiationException, IllegalAccessException
	{
		//These are the Parameters being passed from Jenkins
		String jobName = args[0];
		String buildTag = args[1];
		String projectName = args[2];
		String environment = args[3];
		String browser = args[4];
		String areaName = args[5];
		String testLinkTestPlan = args[6];
		String sendEmailTo = args[7];
		String parallelType = args[8];
		String platformRowNumber = args[9];
		String areaGroupSheetName = args[10];
		String runType = args[11];
		String threadCount = args[12];
		String platformName = args[13];
		String browserVersion = args[14];
		String remoteAddress = args[15];
		String parentJobName = args[16];

		//These are the extra variables being used
		String isParallel = "true";
		String runCompleteClass = "false";
		String mobileUAFlag = "false";
		String testerName = "QA";
		SendEMail.projectNameToSendEmail = projectName;
		
		//Macbook Jenkins Check:
		if (!SendEMail.osType.startsWith("Window"))
		{
			// This will be used to create result link as ISS can't be setup on Macbook 
			if (!(remoteAddress.equalsIgnoreCase("local") || remoteAddress.equalsIgnoreCase("null")) && platformName != null && browserVersion != null && remoteAddress != null && !platformName.equalsIgnoreCase("") && !browserVersion.equalsIgnoreCase("") && !remoteAddress.equalsIgnoreCase(""))
			{
				System.out.println("Paramters set for Browser/Mobile to be open remotely(using Grid) on :"+ remoteAddress +" while code will run on local machine in Mac");
			}
			else
			{
				System.out.println("Paramters set for Browser/Mobile to be open locally and code will also run on local machine in Mac");
				remoteAddress = "NULL";
			}
			
			// This will be used to create result link as ISS can't be setup on Macbook
			SendEMail.remoteIP = null; 
			SendEMail.executionOnRemoteAddress = false;
		}
		else
		{
			// Changing values of Variables respective to "RemoteExecution"(on Grid) or "Local" Run
			if (!(remoteAddress.equalsIgnoreCase("local") || remoteAddress.equalsIgnoreCase("null")) && platformName != null && browserVersion != null && remoteAddress != null && !platformName.equalsIgnoreCase("") && !browserVersion.equalsIgnoreCase("") && !remoteAddress.equalsIgnoreCase(""))
			{
				SendEMail.remoteIP = remoteAddress; 
				SendEMail.executionOnRemoteAddress = true;
			}
			else
			{
				platformName = "Local";
				browserVersion = "NULL";
				remoteAddress = "NULL";
				SendEMail.remoteIP = null;
				SendEMail.executionOnRemoteAddress = false;
			}
		}
		
		
		

		
		// Code to fetch Tester Name from GroupNames ExcelSheet
		String filePath = System.getenv("JENKINS_HOME") + File.separator + "jobs" + File.separator + jobName + File.separator + "workspace" + File.separator + projectName + File.separator + projectName + "GroupNames.xls";
		FileInputStream file = new FileInputStream(filePath);
		HSSFWorkbook groupNameWorkbook = new HSSFWorkbook(file);
		HSSFSheet groupNameSheet = groupNameWorkbook.getSheetAt(1);
		
		if(areaGroupSheetName != null && areaGroupSheetName.equalsIgnoreCase("sheet2"))
			groupNameSheet = groupNameWorkbook.getSheetAt(2);
			
		if(!areaName.equalsIgnoreCase("all") && !areaName.equalsIgnoreCase("default"))
		{
			for(int i = 1 ; i<=groupNameSheet.getLastRowNum();i++)
			{
				if (areaName.equalsIgnoreCase(groupNameSheet.getRow(i).getCell(0).toString()))
				{
					testerName = groupNameSheet.getRow(i).getCell(2).toString();
					break;
				}
			}
		}
		
		
		//Get the GroupName details for each class after reading GroupNames Sheet
		GenerateTestNGXmlAndRun generateTestNGXmlAndRun = new GenerateTestNGXmlAndRun();
		ReadGroupExcelSheetRet2Values<HashMap<String, String>, HashMap<String, ArrayList<String>>> groupDetailsObject = generateTestNGXmlAndRun.readGroupNamesExcelSheet(jobName, projectName, areaName);
		HashMap<String, String> groupNameAndInterferingDetails = groupDetailsObject.first();
		HashMap<String, ArrayList<String>> groupNameAndClassesDetails = groupDetailsObject.second();
		JSONObject groupNameAndClassesDetails_Json = new JSONObject(groupNameAndClassesDetails);

		
		//Code to get test methods (testcases) to be added in TestNG xml, If its 'Regression' then we add all the methods present in a class
		//and if its 'RunByTestPlan' then we need to add only particular testcases which are added in that TestPlan (in TestLink)
		JSONArray testSuiteXml_AllTestcasesToRun = null;
		if (testLinkTestPlan.equalsIgnoreCase("Regression"))
		{
			testSuiteXml_AllTestcasesToRun = generateTestNGXmlAndRun.genTestSuiteXmlObject(groupNameAndClassesDetails);
		}
		else
		{
			HashMap<String, ArrayList<String>> testClassAndMethodDetails = generateTestNGXmlAndRun.getTestClassesAndMethodsReadFromTestLinkPlan(jobName, buildTag.trim(), projectName, testLinkTestPlan, groupNameAndClassesDetails);
			JSONObject testClassAndMethodDetails_json = new JSONObject(testClassAndMethodDetails);
			testSuiteXml_AllTestcasesToRun = generateTestNGXmlAndRun.genTestSuiteXmlObject(groupNameAndClassesDetails_Json, testClassAndMethodDetails_json);
		}
		
		
		//Get the Build Id of RegressionRun Job
		String parentBuildId = getParentBuildID(parentJobName);
		
		
		//Code to Run the testcases on multiple Browsers depending on the 'Platform' sheet
		if(!platformRowNumber.equalsIgnoreCase("-1"))
		{
			HSSFSheet platFormSheet = groupNameWorkbook.getSheet("Platform");
			int numberOfPlatformRows = platformRowNumber.equalsIgnoreCase("All") ? platFormSheet.getLastRowNum() : 1;

			for(int j=1; j<=numberOfPlatformRows; j++)
			{
				int platformRowNumberOfPlatFormSheet = platformRowNumber.equalsIgnoreCase("All") ? j : Integer.parseInt(platformRowNumber);
				platformName = platFormSheet.getRow(platformRowNumberOfPlatFormSheet).getCell(1).toString();
				browser = platFormSheet.getRow(platformRowNumberOfPlatFormSheet).getCell(2).toString();	
				remoteAddress = platFormSheet.getRow(platformRowNumberOfPlatFormSheet).getCell(3).toString();	
				browserVersion = platFormSheet.getRow(platformRowNumberOfPlatFormSheet).getCell(4).toString();

				generateTestNGXmlAndRun.generateAndRunTestNGXml(testSuiteXml_AllTestcasesToRun, groupNameAndInterferingDetails, jobName, buildTag.trim(), projectName, environment, browser, isParallel, parallelType, runCompleteClass, mobileUAFlag, areaName, platformName, remoteAddress, browserVersion, platformRowNumber, runType , threadCount, parentJobName, parentBuildId);
				generateTestNGXmlAndRun.sendEmail(projectName, testLinkTestPlan, environment, sendEmailTo, buildTag.trim(), areaName, testerName, platformName, browser, browserVersion, platformRowNumber, remoteAddress, parentJobName, parentBuildId);
				//Send SMS function gets called only after send Email since there is dependency
				//Also, SMS is sent only when Regression job is triggered on Jenkins
				if(parentJobName.equalsIgnoreCase("RegressionRun"))
					SendSMS.sendSMSAfterRegressionExecution(jobName, projectName, areaName, parentJobName, environment, buildTag);		
				else
					System.out.println("SMS feature bypassed for job name: " + parentJobName);
			}
		}
		//Code to run testcases on only 1 browser i.e. Firefox
		else
		{
			generateTestNGXmlAndRun.generateAndRunTestNGXml(testSuiteXml_AllTestcasesToRun, groupNameAndInterferingDetails, jobName, buildTag.trim(), projectName, environment, browser, isParallel, parallelType, runCompleteClass, mobileUAFlag, areaName, platformName, remoteAddress, browserVersion, platformRowNumber, runType , threadCount, parentJobName, parentBuildId);
			generateTestNGXmlAndRun.sendEmail(projectName, testLinkTestPlan, environment, sendEmailTo, buildTag.trim(), areaName, testerName, platformName, browser, browserVersion, platformRowNumber, remoteAddress, parentJobName, parentBuildId);
			//Send SMS function gets called only after send Email since there is dependency
			//Also, SMS is sent only when Regression job is triggered on Jenkins
			if(parentJobName.equalsIgnoreCase("RegressionRun"))
				SendSMS.sendSMSAfterRegressionExecution(jobName, projectName, areaName, parentJobName, environment, buildTag);
			else
				System.out.println("SMS feature bypassed for job name: " + parentJobName);
		}

		//Sometimes this code creates problem in exiting, so exiting forcefully
		System.exit(0);
	}


	@SuppressWarnings("rawtypes")
	public void generateAndRunTestNGXml(JSONArray testSuiteXml_AllTestcasesToRun, HashMap<String, String> groupNameAndInterferingDetails, String jobName, String buildTag, String projectName, String environment, String browser, String isParallel, String parallelType, String runCompleteClass, String mobileUAFlag, String areaName, String platformName, String remoteAddress, String browserVersion, String platformRowNumber, String runType , String threadCount , String parentJobName, String parentBuildId) throws IOException, JSONException, InstantiationException, IllegalAccessException
	{
		String resultPath = null;
		
		// Deciding Result Saving Path for 'Master-Slave' Run or 'Local' Run
		if (remoteAddress != null && !remoteAddress.isEmpty() && !remoteAddress.equalsIgnoreCase("null") && SendEMail.remoteIP!= null)
			resultPath = File.separator + File.separator + remoteAddress + File.separator + "RegressionResults" + File.separator;
		else if (!SendEMail.osType.startsWith("Window"))
			resultPath = "/Volumes/RegressionResults/";
		else
			resultPath = "C:\\RegressionResults\\";
		
		System.out.println("Setting Result Path as :" + resultPath);
		
		
		//<----------------- CREATING TESTNG.XML File -------------------->
		File file = new File("TestNG.xml");
		FileWriter writer = new FileWriter(file);
		if (debugMode)
			System.out.println("Create TestNG Xml file for defining TC to be run- " + file);

		// Create an instance on TestNG and XML Suite 
		TestNG myTestNG = new TestNG();
		XmlSuite mySuite = new XmlSuite();
		mySuite.setName(projectName + " Automation");

		// Check if suite is to be run in parallel
		if (isParallel.contentEquals("true"))
		{
			int threadCnt = Integer.parseInt(threadCount);
			mySuite.setParallel(parallelType);
			mySuite.setThreadCount(threadCnt);
		}

		// Set environment and browser as parameters in TestNG Xml
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("parallelType", parallelType);
		parameters.put("environment", environment);
		parameters.put("browser", browser);
		parameters.put("PlatformName", platformName);
		parameters.put("RemoteAddress", remoteAddress);
		parameters.put("BrowserVersion", browserVersion);

		// Setting value of 'MobileUAFlag' true for mobile cases
		parameters.put("MobileUAFlag", mobileUAFlag);
		if (areaName.contains("Mobile-Web")|| areaName.contains("LayoutTesting-Mobile"))
			parameters.put("MobileUAFlag", "true");
		
		mySuite.setParameters(parameters);

		// Set listeners
		List<Class> listnerClasses = new ArrayList<Class>();
		listnerClasses.add(org.uncommons.reportng.HTMLReporter.class);
		listnerClasses.add(org.uncommons.reportng.JUnitXMLReporter.class);
		myTestNG.setListenerClasses(listnerClasses);
		// disable default listeners
		myTestNG.setUseDefaultListeners(false);

		
		//Put path of 'Result Directory' in RunTime for later uses
		String outputDir = resultPath + projectName + File.separator + "Results" + File.separator + buildTag;
		if(!platformRowNumber.equals("-1"))
		{
			if(!platformName.equals("NULL"))
				outputDir = outputDir + File.separator + platformName+"_"+ browser +"_"+browserVersion;
			else
				outputDir = outputDir + File.separator + browser;
		}

		if (!areaName.equalsIgnoreCase("Default"))
		{
			if (parentJobName.equalsIgnoreCase("RegressionRun"))
				outputDir = resultPath + projectName + File.separator + "Results" + File.separator + "Jenkins-" + parentJobName + "-" + parentBuildId  + File.separator + buildTag + "_" + areaName;
			else
				outputDir = outputDir + File.separator + areaName;
		}
		System.setProperty("testngOutputDir", outputDir);
		System.setProperty("BuildId", buildTag);
		

		//Creating the Result Directory where we can save automation results + screenshots + other data
		if (debugMode)
		{
			boolean status = new File(outputDir).mkdirs();
			if (status)
			{
				System.out.println("Created Result Output Directory - " + outputDir);
			}
			else
			{
				System.out.println("Error in Creating Result Output Directory - " + outputDir);
				System.out.println("<------ Exiting further Execution of Code as Result Output Directory is not created ------>");
				System.exit(0);	
			}
		}
		myTestNG.setOutputDirectory(outputDir);

		// Set ReportNG Properties
		System.setProperty("org.uncommons.reportng.title", "PayU " + projectName + " Test Report");
		System.setProperty("org.uncommons.reportng.escape-output", "false");

		// Overriding various value of Config.properties
		if (!(environment == null || environment.isEmpty()))
			System.setProperty("environment", environment);
		if (!(browser == null || browser.isEmpty()))
			System.setProperty("browser", browser);
		if (!(mobileUAFlag == null || mobileUAFlag.isEmpty()))
			System.setProperty("MobileUAFlag", mobileUAFlag);
		if (!(platformName == null || platformName.isEmpty()))
			System.setProperty("PlatformName", platformName);
		if (!(remoteAddress == null || remoteAddress.isEmpty()))
			System.setProperty("RemoteAddress", remoteAddress);
		if (!(browserVersion == null || browserVersion.isEmpty()))
			System.setProperty("BrowserVersion", browserVersion);
		if (!(runType== null || runType.isEmpty()))
			System.setProperty("RunType", runType);
		if (!(projectName == null || projectName.isEmpty()))
			System.setProperty("projectName", projectName);
		
		
		// Get all the class names to be run
		for (int i = 0; i < testSuiteXml_AllTestcasesToRun.length(); i++)
		{
			Iterator<?> groupItr = testSuiteXml_AllTestcasesToRun.getJSONObject(i).keys();
			JSONObject groupObj = testSuiteXml_AllTestcasesToRun.getJSONObject(i);

			List<XmlClass> myClasses = null;
			XmlClass testClass = null;
			XmlTest myTest = null;

			while (groupItr.hasNext())
			{
				String groupKey = groupItr.next().toString();
				if (debugMode)
					System.out.println("Checking group key - " + groupKey + " at index " + i);

				JSONArray classesObj = new JSONArray(groupObj.get(groupKey).toString());
				String isInterfering = groupNameAndInterferingDetails.get(groupKey);
				
				// Create an instance of XmlTest and assign a name for it.
				myTest = new XmlTest();
				myTest.setName(groupKey);
				myTest.setVerbose(0);
				if (isInterfering == null || isInterfering.contentEquals("TRUE"))
					myTest.setThreadCount(1);

				// Create a list which can contain the classes that you want to run.
				myClasses = new ArrayList<XmlClass>();

				for (int k = 0; k < classesObj.length(); k++)
				{
					// get a class json object
					JSONObject classObj = classesObj.getJSONObject(k);
					ArrayList<String> keys = new ArrayList<String>();
					Iterator<?> classItr = classObj.keys();
					
					// get the classnames
					while (classItr.hasNext())
					{
						String className = classItr.next().toString();
						if (debugMode)
							System.out.println("Adding class - " + className);
						keys.add(className);
					}
					 
					for (int l = 0; l < keys.size(); l++)
					{
						 String classKey = keys.get(l);
						 if (debugMode)
							 System.out.println("Creating Test node for class - " + classKey);

						 try
						 {
							 // Create an instance of XmlClass and assign a name for it.
							 testClass = new XmlClass(classKey);
						 }
						 catch (TestNGException e)
						 {
							 if (debugMode)
								 System.out.println("Exception is: " + ExceptionUtils.getStackTrace(e));
						 }

						 //For TestPlan Run adding particular methods (testcases) in the XML (Here not running full class)
						 if (runCompleteClass.contentEquals("false"))
						 {
							 ArrayList<XmlInclude> methodsToRun = new ArrayList<XmlInclude>();
							 String[] methods = null;

							 String m = classObj.getString(classKey);
							 if (m.substring(1, m.length() - 1).contains(","))
							 {
								 methods = m.substring(1, m.length() - 1).split(", ");

								 if (debugMode)
									 System.out.println("Including method - ");
								 for (int j = 0; j < methods.length; j++)
								 {
									 if (debugMode)
										 System.out.print("\t" + methods[j]);
									 methodsToRun.add(new XmlInclude(methods[j]));
								 }
								 if (testClass != null)
									 testClass.setIncludedMethods(methodsToRun);

								 if (debugMode)
									 System.out.println();
							 }
							 else
							 {
								 if (debugMode)
									 System.out.println("Including method - " + m.substring(1, m.length() - 1));
								 methodsToRun.add(new XmlInclude(m.substring(1, m.length() - 1)));
								 if (testClass != null)
									 testClass.setIncludedMethods(methodsToRun);

							 }
						 }
						 if (testClass != null)
							 myClasses.add(testClass);
					 }
				}
			}

			// Assign that to the XmlTest Object created earlier.
			myTest.setXmlClasses(myClasses);
			myTest.setSuite(mySuite);
			mySuite.addTest(myTest);
		}
		
		// Add the suite to the list And then Set the list of Suites to the testNG object we created earlier
		List<XmlSuite> mySuites = new ArrayList<XmlSuite>();
		mySuites.add(mySuite);
		myTestNG.setXmlSuites(mySuites);
		writer.write(mySuite.toXml());
		writer.close();
		if (debugMode)
			System.out.println("Created XML - \n" + mySuite.toXml());


		//<----------------- EXECUTING TESTNG.XML File -------------------->
		//This will run the classes and methods specified in TESTNG.Xml File
		myTestNG.run();
	}

	
	public void sendEmail(String projectName, String testPlanOrAreaName, String environment, String sendEmailTo, String buildTag, String areaName, String testerName, String platformName, String browserName, String browserVersion, String platformRowNumber, String remoteAddress, String parentJobName, String parentBuildId)
	{
		String resultPath = projectName + "/Results/" + buildTag + "/html";
		String xmlPath = projectName + "/Results/" + buildTag + "/xml";
		
		if(!platformRowNumber.equals("-1"))
		{
			if(!platformName.equals("NULL"))
			{
				resultPath= projectName + "/Results/" + buildTag + "/" + platformName+"_"+ browserName +"_" + browserVersion+ "/html";	
				xmlPath= projectName + "/Results/" + buildTag + "/" + platformName+"_"+ browserName +"_" + browserVersion+ "/xml";	
			}
			else
			{
				resultPath= projectName + "/Results/" + buildTag + "/" + browserName + "/html";
				xmlPath= projectName + "/Results/" + buildTag + "/" + browserName + "/xml";
			}	
		}
		if (!areaName.equalsIgnoreCase("Default"))
		{
			testPlanOrAreaName = areaName;
			if (parentJobName.equalsIgnoreCase("RegressionRun"))
			{
				resultPath =  projectName + "/Results/" + "Jenkins-" + parentJobName + "-" + parentBuildId +"/" + buildTag + "_" + areaName + "/html";
				xmlPath =  projectName + "/Results/" + "Jenkins-" + parentJobName + "-" + parentBuildId +"/" + buildTag + "_" + areaName + "/xml";
			}
			else
			{
				resultPath = projectName + "/Results/" + buildTag + "/" + areaName + "/html";
				xmlPath = projectName + "/Results/" + buildTag + "/" + areaName + "/xml";
			}
		}		
		
		String IP_Address = null;
		try {
			IP_Address = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}

		// Deciding Remote IP for accessing Results:
		boolean remoteExecution = false;
		if(SendEMail.osType.startsWith("Window"))
		{
			if (remoteAddress != null && !remoteAddress.isEmpty() && !remoteAddress.equalsIgnoreCase("null"))
			{
				//determining the VPN IP for the given remote machine 
				switch(remoteAddress)
				{
					case "10.100.75.17":
						IP_Address = "10.50.9.165";
						break;
					case "10.100.76.116":
						IP_Address = "10.50.6.190";
						break;
					case "10.100.32.25":
						IP_Address = "10.50.9.253";
						break;
					default:
						IP_Address = "10.100.75.64";
				}
				remoteExecution = true;
			}
		}
		else
		{
			// Code to decide ISS IP in case of Code Execution on Macbook
			switch(projectName.toLowerCase())
			{
				case "product":
				case "bizmobileautomation":
					IP_Address = "10.50.6.190";
					break;
				case "money":
					IP_Address = "10.50.9.165";
					break;
				case "bankingproduct":
					IP_Address = "10.50.9.253";
					break;
			}
			// All the data is present locally in /Volumes/RegressionResults hence disabling remoteExecution for Mac
			remoteExecution = false;
		}
		try
		{			
			boolean emailStatus = SendEMail.emailAfterTLExecution(xmlPath, resultPath, IP_Address, projectName, testPlanOrAreaName, environment, sendEmailTo, testerName, platformName, browserName, browserVersion, platformRowNumber, remoteExecution,parentJobName,parentBuildId);
			if (emailStatus)
				System.out.println("Report Send Successfully on Email to: " + sendEmailTo);
			else
			{
				System.out.println("Value of resultPath : " + resultPath);
				System.out.println("Value of xmlPath : " + xmlPath);
				System.out.println("<----------AUTOMATION REPORT SENDING FAILED FOR :" + sendEmailTo +"---------->");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	/**
	 * This Method is used to find build id from parentJobName file stored at RegressionResults folder 
	 * @param filePath		 
	 * @param parentJobName
	 * @return		 
	 */
	public static String getParentBuildID(String parentJobName)
	{	 
		BufferedReader br = null;
		int buildId = 0;
		try  
		{
			String filePath = "";
			if(SendEMail.executionOnRemoteAddress)
				filePath = File.separator + File.separator + SendEMail.remoteIP + File.separator + "Users" + File.separator + "payu"+ File.separator + ".hudson" + File.separator + "jobs" + File.separator + parentJobName + File.separator + "nextBuildNumber";
			else
				filePath = System.getenv("JENKINS_HOME") + File.separator + "jobs" + File.separator + parentJobName + File.separator + "nextBuildNumber"; 
			
			br = new BufferedReader(new FileReader(filePath));
			String line = br.readLine();
			String nextBuildID = line.toString();
			buildId = Integer.parseInt(nextBuildID.trim())-1;
			br.close();
		} 
		catch (FileNotFoundException fe)
		{		 
			System.out.println("Build Id not found");
		}
		catch (Exception e)
		{	 
			e.printStackTrace();
		}		
		return Integer.toString(buildId);
	}
	
	
	private JSONArray genTestSuiteXmlObject(HashMap<String, ArrayList<String>> groupAndClasses_hash) throws JSONException
	{
		String groupName = null;
		JSONObject testDetEntry = null;
		JSONObject testSuiteEntry = new JSONObject();
		JSONArray testSuiteXmlObj = new JSONArray();

		for (Entry<String, ArrayList<String>> groupAndClassKeyValuePair : groupAndClasses_hash.entrySet())
		{
			testDetEntry = new JSONObject();
			groupName = groupAndClassKeyValuePair.getKey();
			String methodNames = "all";
			ArrayList<String> classNamesInGroup = groupAndClassKeyValuePair.getValue();
			for (String classNames : classNamesInGroup)
			{
				if (debugMode)
					System.out.println("Adding in group - " + groupName + " Class - " + classNames + " And method - " + methodNames);
				testDetEntry.put(classNames, methodNames);
			}

			if (groupName != null)
			{
				// consolidate all classes (along with their methods) that belong to groupName
				testSuiteEntry.append(groupName, testDetEntry);
				groupName = null;
			}
			testDetEntry = null;
		}
		Iterator<?> groupNames = testSuiteEntry.keys();
		JSONObject testSuiteEntryObj;
		int i = 0;
		
		while (groupNames.hasNext())
		{
			String groupname = groupNames.next().toString();
			JSONArray testClassesEntryArray = testSuiteEntry.getJSONArray(groupname);
			testSuiteEntryObj = new JSONObject();
			// excluding empty class array, i.e. groupname is there in excel but the corresponding class is not included in test plan
			if (!testClassesEntryArray.toString().contentEquals("{}"))
			{
				testSuiteEntryObj.put(groupname, testClassesEntryArray);
				System.out.println("Adding at index " + i + " in group - " + groupname);
				testSuiteXmlObj.put(i, testSuiteEntryObj);
				i++;
			}
		}

		return testSuiteXmlObj;
	}
	

	/**
	 * Util method to construct TestSuiteXml Object array
	 * 
	 * @throws JSONException
	 */
	private JSONArray genTestSuiteXmlObject(JSONObject groupNameAndClassesDetails_Json, JSONObject testClassAndMethodDetails_json) throws JSONException
	{
		String groupName = null;
		JSONArray testSuiteXmlObj = new JSONArray();
		Iterator<?> groupNames = groupNameAndClassesDetails_Json.keys();
		Iterator<?> testClasses = testClassAndMethodDetails_json.keys();
		JSONObject testClassMethodForGroup = null;
		JSONObject groupNameClassesAndMethods_json = new JSONObject();

		while (testClasses.hasNext())
		{
			testClassMethodForGroup = new JSONObject();
			String className = (String) testClasses.next();
			groupNames = groupNameAndClassesDetails_Json.keys();
			if (debugMode)
				System.out.println("Checking group name for class - " + className);

			while (groupNames.hasNext())
			{
				groupName = (String) groupNames.next();
				String classNamesInThatGroup = groupNameAndClassesDetails_Json.get(groupName).toString();

				// 'classNamesInThatGroup' is an arraylist of classes that belong to a group and 'className' is a single class picked from testlink
				if (!classNamesInThatGroup.isEmpty() && !className.isEmpty())
				{
					if (classNamesInThatGroup.equalsIgnoreCase(className))
					{
						if (className != null)
						{
							String methodNames = testClassAndMethodDetails_json.get(className).toString();
							if (debugMode)
								System.out.println("Adding in group - " + groupName + " Class - " + className + " And method - " + methodNames);
							testClassMethodForGroup.put(className, methodNames);
							className = null;
						}
						break;
					}
				}
			}
			if (groupName != null)
			{
				// consolidate all classes (along with their methods) that belong to groupName
				groupNameClassesAndMethods_json.append(groupName, testClassMethodForGroup);
				groupName = null;
			}
		}

		groupNames = groupNameClassesAndMethods_json.keys();

		// Construct a JSONArray , by splitting the testSuiteEntry
		int i = 0;
		while (groupNames.hasNext())
		{ 
			String groupname = groupNames.next().toString();
			JSONArray testClassesAndMethods_json = groupNameClassesAndMethods_json.getJSONArray(groupname);
			System.out.println("Checking group - " + groupname + " testClassesAndMethods " + testClassesAndMethods_json.toString());
		}

		testClasses = testClassAndMethodDetails_json.keys();

		// if there are no group names in excel sheet, even then add testcase to xml with group name as classname
		testClassMethodForGroup = new JSONObject();
		while (testClasses.hasNext())
		{
			String className = (String) testClasses.next();
			String methodNames = testClassAndMethodDetails_json.get(className).toString();
			boolean isPresent = false;
			int index = testSuiteXmlObj.length();
			for (i = 0; i < testSuiteXmlObj.length(); i++)
			{
				if (testSuiteXmlObj.get(i).toString().contains(className))
					isPresent = true;
			}
			if (isPresent == false)
			{
				String[] fullName = className.split("\\.");
				groupName = fullName[fullName.length - 1];
				if (!className.isEmpty() && !methodNames.isEmpty())
				{
					testClassMethodForGroup.put(className, methodNames);
					groupNameClassesAndMethods_json.append(groupName, testClassMethodForGroup);
					testClassMethodForGroup = new JSONObject();

					// Construct a JSONArray , by splitting the testSuiteEntry
					JSONArray testClassesEntryArray = groupNameClassesAndMethods_json.getJSONArray(groupName);
					JSONObject testSuiteEntryObj = new JSONObject();
					
					// excluding empty class array,i.e. groupName is there in excel but the corresponding class is not included in test plan
					if (!testClassesEntryArray.toString().contentEquals("{}"))
					{
						if (debugMode)
							System.out.println("Adding at index " + index + " in group - " + groupName + " Class - " + className + " And method - " + methodNames);
						testSuiteEntryObj.put(groupName, testClassesEntryArray);
						testSuiteXmlObj.put(index, testSuiteEntryObj);
						index++;
					}
				}
			}
		}
		return testSuiteXmlObj;
	}

	/**
	 * Util method that reads the excel sheet containing test details and
	 * creates a hash map which maps the Java class name with corresponding java
	 * method names
	 * @param buildTag
	 * @param jobName
	 * @return HashMap containing testcase details with key as classname and value as methods array list
	 */
	@SuppressWarnings("static-access")
	private HashMap<String, ArrayList<String>> getTestClassesAndMethodsReadFromTestLinkPlan(String jobName, String buildTag, String testLinkProjectName, String testLinkTestPlan, HashMap<String, ArrayList<String>> groupNameAndClassesDetails) throws IOException
	{
		//Read testlink for test casesto be executed in test plan and create excel sheet of test classes and methods
		String argsForGetTestCasesToRun[] = {jobName, buildTag, testLinkProjectName, testLinkTestPlan};
		GetTestCasesToRun getTestCasesToRun = new GetTestCasesToRun();
		getTestCasesToRun.main(argsForGetTestCasesToRun);

		//File generated by GetTestCasesToRun class
		String filePath = System.getProperty("user.home") + "/.hudson/jobs/" + jobName + "/workspace/" + buildTag + ".xls";

		// Stores the class name and the corresponding method list
		HashMap<String, ArrayList<String>> testClassAndMethodDetails = new HashMap<String, ArrayList<String>>();

		try
		{
			if (debugMode)
				System.out.println("Reading file - " + filePath);
			FileInputStream file = new FileInputStream(filePath);
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);

			int lastRow = sheet.getLastRowNum();

			for (int i = 1; i <= lastRow; i++)
			{
				// Read the class name in current row
				String currentClass = sheet.getRow(i).getCell(1).toString();
				if (!isClassNamePresent(groupNameAndClassesDetails, currentClass))
					continue;

				ArrayList<String> methods;

				// Check if current class is already added
				if (!testClassAndMethodDetails.containsKey(currentClass))
				{
					if (debugMode)
						System.out.println("Identifying methods to be run for class - " + currentClass);

					// This will hold all methods belonging to current class
					// name
					methods = new ArrayList<String>();
					methods.add(sheet.getRow(i).getCell(2).toString());
					// add all these methods against the current class name
					if (debugMode)
						System.out.println("Adding class - " + currentClass + " and methods - " + methods.toString());
					testClassAndMethodDetails.put(currentClass, methods);
				}
				else
				{
					methods = testClassAndMethodDetails.get(currentClass);
					methods.add(sheet.getRow(i).getCell(2).toString());
					if (debugMode)
						System.out.println("Adding class - " + currentClass + " and methods - " + methods.toString());
					testClassAndMethodDetails.put(currentClass, methods);
				}
			}
			file.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return testClassAndMethodDetails;
	}

	/**
	 * Checks if this className is present in groupNameAndClassesDetails or not.
	 * 
	 * @param groupDetails
	 * @param className
	 * @return
	 */
	private boolean isClassNamePresent(HashMap<String, ArrayList<String>> groupNameAndClassesDetails, String className)
	{
		for (Entry<String, ArrayList<String>> groupNameAndClassKeyValuePair : groupNameAndClassesDetails.entrySet())
		{
			ArrayList<String> currentClasses = groupNameAndClassKeyValuePair.getValue();
			if (currentClasses.contains(className))
				return true;
		}
		return false;
	}

	/**
	 * Util method that reads group names and interfering values for classes and
	 * creates a hashmap which maps group name with corresponding java classes
	 * and interfering value
	 */

	private ReadGroupExcelSheetRet2Values<HashMap<String, String>, HashMap<String, ArrayList<String>>> readGroupNamesExcelSheet(String jobName, String projectName, String areaName) throws IOException

	{
		String filePath = System.getenv("JENKINS_HOME") + File.separator + "jobs" + File.separator + jobName + File.separator + "workspace" + File.separator + projectName + File.separator + projectName + "GroupNames.xls";

		// Stores the group name and the corresponding classes list
		HashMap<String, ArrayList<String>> groupNameAndClassesDetails = new HashMap<String, ArrayList<String>>();
		// Stores the group name and corresponding interfering value
		HashMap<String, String> groupNameAndInterferingDetails = new HashMap<String, String>();

		try
		{
			if (debugMode)
				System.out.println("Reading file - " + filePath);
			FileInputStream file = new FileInputStream(filePath);
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);

			int lastRow = sheet.getLastRowNum();

			// This will hold all classes belonging to current group name
			String isInterfering = null;

			for (int i = 1; i <= lastRow; i++)
			{
				String currentGroupName = "";

				// If test plan execution is not there i.e. area wise execution
				if (!areaName.equalsIgnoreCase("Default"))
				{
					String[] groupAreaNameArray = sheet.getRow(i).getCell(5).toString().split(",");
					if (Arrays.asList(groupAreaNameArray).contains(areaName))
					{
						// Read the group name in current row
						currentGroupName = sheet.getRow(i).getCell(0).toString().trim();
					}
					else
						continue;
				}
				else
					// All the groups need to be considered since we are
					// doing test plan wise run
				{
					// read all entries in group details sheet one by one
					currentGroupName = sheet.getRow(i).getCell(0).toString();
				}

				ArrayList<String> classes;
				// Check if current group is already added
				if (!groupNameAndClassesDetails.containsKey(currentGroupName))
				{
					classes = new ArrayList<String>();
					isInterfering = sheet.getRow(i).getCell(2).toString();
					classes.add(sheet.getRow(i).getCell(1).toString());
					groupNameAndClassesDetails.put(currentGroupName, classes);
					groupNameAndInterferingDetails.put(currentGroupName, isInterfering);
				}
				else
				{
					classes = groupNameAndClassesDetails.get(currentGroupName);
					classes.add(sheet.getRow(i).getCell(1).toString());
					groupNameAndClassesDetails.put(currentGroupName, classes);
				}
			}
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return new ReadGroupExcelSheetRet2Values<HashMap<String, String>, HashMap<String, ArrayList<String>>>(groupNameAndInterferingDetails, groupNameAndClassesDetails);
	}

}