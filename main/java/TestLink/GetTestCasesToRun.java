package TestLink;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;

/**
 * Program that reads test case class and method names and test link test case
 * ids(from test link project) and writes them to an excel sheet named after
 * hudson build tag(used as timestamp). This excel sheet is read by
 * Utils.GenerateTestNGXmlAndRun class to generate the testng.xml file
 * 
 * @author vidya.priyadarshini
 */
public class GetTestCasesToRun
{
	
	public static void main(String[] args) throws IOException
	{
		// hudson Job Name - args[0]
		// hudson Build Tag - args[1]
		// projectName - args[2]
		// testPlanName - args[3]
		String projectName = args[2];
		String testPlanName = args[3];
		//String filePath = System.getProperty("user.home") + "/.hudson/jobs/" + args[0] + "/workspace/" + args[1] + ".xls";
		String filePath = System.getenv("JENKINS_HOME") + "/jobs/" + args[0] + "/workspace/" + args[1] + ".xls";
		File f = new File(filePath);
		
		if (!f.exists())
		{
			System.out.println("File - " + filePath + " does not exists.");
			System.out.println("Creating file - " + filePath);
			FileOutputStream fileout = new FileOutputStream(filePath);
			
			System.out.println("Writing header row - TCId, JavaClassName, JavaMethodName");
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("FirstSheet");
			
			HSSFRow rowhead = sheet.createRow((short) 0);
			rowhead.createCell(0).setCellValue("TCId");
			rowhead.createCell(1).setCellValue("JavaClassName");
			rowhead.createCell(2).setCellValue("JavaMethodName");
			
			workbook.write(fileout);
			
			fileout.flush();
			fileout.close();
		}
		
		final String DEV_KEY = "d5539eafe6572eaeddf07db1fc674cac";
		final String SERVER_URL = "http://payu-testlink.com/lib/api/xmlrpc/v1/xmlrpc.php";
		Integer projectId = ImportTestCases.getProjectId(SERVER_URL, DEV_KEY, projectName);
		URL url = new URL(SERVER_URL);
		TestLinkAPI api = new TestLinkAPI(url, DEV_KEY);
		
		FileInputStream fileInputStream = new FileInputStream(filePath);
		FileOutputStream fileout = null;
		HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
		HSSFSheet sheet = workbook.getSheet("FirstSheet");
		
		System.out.println("Reading test cases for Test Plan : " + testPlanName + "....");
		// Get all test cases for testPlan
		TestCase[] testCases = ImportTestCases.getTestCasesForTestPlan(SERVER_URL, DEV_KEY, testPlanName, projectName);
		
		for (TestCase testCase : testCases)
		{
			// get the current number of rows
			int numRows = sheet.getLastRowNum();
			String javaTestClassForTestCase = api.getTestCaseCustomFieldDesignValue(testCase.getId(), 1, testCase.getVersion(), projectId, "JavaTestClass", null).getValue();
			String javaTestMehodForTestClass = api.getTestCaseCustomFieldDesignValue(testCase.getId(), 1, testCase.getVersion(), projectId, "JavaTestMethod", null).getValue();
			// check if test case is automated and JavaTestClass is not empty,
			// add entry in excel sheet
			if (!javaTestClassForTestCase.isEmpty())
			{
				String id = testCase.getId().toString();
				System.out.println("Writing row num : " + numRows + " for test cases : " + javaTestMehodForTestClass + " id:-" + id + " + class:-" + javaTestClassForTestCase);
				HSSFRow rowhead = sheet.createRow((short) numRows + 1);
				rowhead.createCell(0).setCellValue(id);
				rowhead.createCell(1).setCellValue(javaTestClassForTestCase);
				rowhead.createCell(2).setCellValue(javaTestMehodForTestClass);
			}
			fileout = new FileOutputStream(filePath);
			workbook.write(fileout);
		}
		fileInputStream.close();
		fileout.flush();
		fileout.close();
	}
}