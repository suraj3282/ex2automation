package TestLink;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.testng.annotations.Test;
import Utils.TestBase;

public class HostTry extends TestBase
{	
	@Test()
	public void TestCasesForVerifyBankTDRRulesForOneTransaction()
	{
		
			String environment = "PPAutomation";
			try {
				boolean is_hostEntrySetCorrect = setHostEntry(environment);
			if (is_hostEntrySetCorrect)
				System.out.println("Host entries have been properly set for environment "+environment);
			else
				System.out.println("Unable to set host entries properly for environment "+ environment);
			} catch (IOException e) {
				System.out.println("Error in creating hosts file on the machine.");;
			}
		}

		private static boolean setHostEntry(String environment) throws IOException 
		{
			String text = null;
			switch(environment)
			{
			case "PPReplicaRelease": 
				text = "## ENTRIES FOR REPLICA \n10.50.23.63  misc.payu.in \n10.50.23.62	 static.payu.in \n10.50.23.60	 admin.payu.in \n10.50.23.60	 cron.payu.in \n10.50.23.59	 secure.payu.in \n10.50.23.58	 info.payu.in \n10.50.23.57	 www.payu.in";;
				break;
			case "PPAutomation":
				text = "## Entries for Dynamic Switching Environment \n10.50.23.143   info.payu.in  \n10.50.23.142   www.payu.in"
				+ "\n10.50.23.141  api.payu.in \n10.50.23.150   developer.payu.in"
				+ "\n10.50.23.149  static.payu.in \n10.50.23.148   ivr.payu.in \n10.50.23.147   api.payu.in"
				+ "\n10.50.23.146   test.payu.in \n10.50.23.145   admin.payu.in \n10.50.23.145   cron.payu.in \n10.50.23.144  secure.payu.in";
				break;
			default:
				text = "## ENTRIES FOR REPLICA \n10.50.23.63  misc.payu.in \n10.50.23.62	 static.payu.in \n10.50.23.60	 admin.payu.in \n10.50.23.60	 cron.payu.in \n10.50.23.59	 secure.payu.in \n10.50.23.58	 info.payu.in \n10.50.23.57	 www.payu.in";;
			}
//			File file = new File("\\\\"+SendEMail.remoteIP + "\\Windows\\System32\\drivers\\etc\\hosts");
			String path = "C:" + File.separator + "Windows" + File.separator + "System32" + File.separator + "drivers" + File.separator + "etc" + File.separator + "hosts";;
            BufferedWriter out = new BufferedWriter(new FileWriter(path));
            out.write(text);
			out.close();
			return true;
		}
}
