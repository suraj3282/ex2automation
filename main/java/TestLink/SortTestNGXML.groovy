package in.payu.xmlparsing

import org.codehaus.groovy.ast.stmt.CatchStatement;

import groovy.xml.MarkupBuilder
import groovyjarjarcommonscli.ParseException;

class WritingUpdatedXML {

	/**
	 * This Method is used to Parse and Sort the data of XML on the basis of name attribute of test element.
	 * @param file path of xml file 
	 * @return Updated File File.
	 * @author shishir.dwivedi
	 */
	def sortXML(java.lang.String file) throws FileNotFoundException,ParseException{
		try
		{
		if(file==null) throw new FileNotFoundException("File  not Found");
		}
		catch(e){
		println (e);
		}
		
		def parser=new XmlParser();
		
		//Ignoring Doctype and DTD Declaration. Removing these two line will cause exception.
		parser.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
		parser.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		
		//Sort XML file on the basis of name attribute
		String fileContents = new File(file).text
		def data = parser.parseText(fileContents)
		data.children().sort(true) {it.'@name'}
		
		//Writing Sorted XML to XML File without doctype and DTD.
		new XmlNodePrinter(new PrintWriter(new FileWriter(file))).print(data)
		return file;
	}
	
}
