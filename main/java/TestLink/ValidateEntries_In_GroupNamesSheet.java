package TestLink;

/**
 * Author : mukesh.rajput
 *
 * This class in created to verify if all the TestClasses(entries) present in GroupNames Sheet do really exist in the TEST Package or not.
 *
 * Meaning of Console Messages :
 * 1. "<----------TestLink Suite Id MisMatch for this Test Class---------->" : This message will tell that TestLinkSuitId present in GroupNames sheet is not correct
 * 2. "<----------This class is not available in the Test Package---------->" : This message will tell that this TestClass is now not present in TEST Package, either it is renamed or deleted
 *
 */

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.HashMap;

import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;

public class ValidateEntries_In_GroupNamesSheet extends JFrame implements ActionListener
{
	
	public static void main(String args[]) throws ClassNotFoundException, IOException, InvocationTargetException, InterruptedException
	{
		// Use the event dispatch thread for Swing components
		EventQueue.invokeAndWait(new Runnable()
		{
			@Override
			public void run()
			{
				// create GUI frame
				try
				{
					new ValidateEntries_In_GroupNamesSheet().setVisible(true);
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * This function verifies that whether all the test classes mentioned in the
	 * GroupsName Sheet are present in the Test Package or not.
	 * 
	 * @param SERVER_URL
	 * @param DEV_KEY
	 * @param jobName
	 * @param projectName
	 * @throws IOException
	 */
	public static void verifyTestClassesInGroupNamesSheet(String SERVER_URL, String DEV_KEY, String jobName, String projectName) throws IOException
	{
		String filePath = "C:\\PayU\\" + projectName + "\\" + projectName + "GroupNames.xls";
		
		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);
		
		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet = workbook.getSheetAt(0);
		
		int lastRow = sheet.getLastRowNum();
		HashMap<String, Integer> groupsMap = new HashMap<String, Integer>();
		Integer projectId = ImportTestCases.getProjectId(SERVER_URL, DEV_KEY, projectName);
		
		for (int j = 1; j <= lastRow; j++)
		{
			if (sheet.getRow(j).getCell(4) != null)
				groupsMap.put(sheet.getRow(j).getCell(1).toString(), Integer.parseInt(sheet.getRow(j).getCell(4).toString()));
			else
				groupsMap.put(sheet.getRow(j).getCell(1).toString(), 0);
			
			String className = sheet.getRow(j).getCell(1).toString();
			String classSuiteId = sheet.getRow(j).getCell(4).toString();
			
			System.out.println("\nTest Class Name is : " + className);
			System.out.println("TestLink Suite Id  : " + classSuiteId);
			
			try
			{
				URL url = new URL(SERVER_URL);
				TestLinkAPI api = new TestLinkAPI(url, DEV_KEY);
				Class classObj = Class.forName(className);
				try
				{
					api.getTestCasesForTestSuite(Integer.parseInt(classSuiteId), null, null);
				}
				catch (Exception e)
				{
					System.out.println("<----------TestLink Suite Id MisMatch for this Test Class---------->");
				}
			}
			catch (ClassNotFoundException e)
			{
				System.out.println("<----------This class is not available in the Test Package---------->");
			}
		}
	}
	
	/**
	 * This function populate a Pop Up (UI Frame) to select the Project Name and
	 * then call the verifyTestClassesInGroupNamesSheet() function.
	 * 
	 * @throws IOException
	 */
	public ValidateEntries_In_GroupNamesSheet() throws IOException
	{
		final String DEV_KEY = "d5539eafe6572eaeddf07db1fc674cac";
		final String SERVER_URL = "http://payu-testlink.com/lib/api/xmlrpc/v1/xmlrpc.php";
		
		// Using a standard Java icon
		Icon optionIcon = UIManager.getIcon("FileView.computerIcon");
		String[] projectType = { "Product", "Paisa", "Money" };
		
		// make sure the program exits when the frame closes
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		
		// This will center the JFrame in the middle of the screen
		setLocationRelativeTo(null);
		
		// Combo box of project types
		JComboBox projectList = new JComboBox(projectType);
		projectList.addActionListener(this);
		System.out.println("Select your Project from the dropdown appearing in the Pop Up");
		String projectName = (String) JOptionPane.showInputDialog(this, "Pick a Name:", "ComboBox Dialog", JOptionPane.QUESTION_MESSAGE, optionIcon, projectType, projectType[0]);
		System.out.println("Project Name entered : " + projectName);
		
		String pathName = "C:\\PayU\\" + projectName + "\\bin\\Test\\";
		String packageName = "Test";
		final String jobName = "RunTestLink";
		
		verifyTestClassesInGroupNamesSheet(SERVER_URL, DEV_KEY, jobName, projectName);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		// TODO Auto-generated method stub
	}
}
