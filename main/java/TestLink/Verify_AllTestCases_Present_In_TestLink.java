package TestLink;

/**
 * Author : mukesh.rajput
 *
 * This class in created to verify:
 * 1. if all the TestClasses/TestMethods present in the TEST Package are linked with the Testlink or not. 
 * 2. Find out the TestPlan name of all the testcases (Right Now we are just checking if testcase is present in "Regression" testPlan or not)
 * 3. To find out Data of custom fields like "AutomatedBy", "JavaTestClass", "JavaTestMethod", "Automation_Status" for every TestCase.
 *
 * Meaning of Console Messages :
 * 1. "----------> Test Case not added in TestLink is " : This message will tell that this TestCase is present in TEST Package but is not added in the TestLink
 * 2. "<----------Test Class not added in TestLink---------->" : This message will tell that this TestClass is present in TEST Package but is not added in the TestLink
 * 3. "<----------TestLink Suite Id MisMatch for this Test Class---------->" : This message will tell that TestLinkSuitId present in GroupNames sheet is not correct
 * 4. <----------Method name : testMethodName dont exist in the TestPlan : Regression----------> : This message will tell that this 'testMethodName' dont exist in the TestPlan i.e "Regression"
 *
 */

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.testng.annotations.Test;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ResponseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.CustomField;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;

public class Verify_AllTestCases_Present_In_TestLink extends JFrame implements ActionListener
{
	
	/**
	 * This function is created to get the values of custom fields like
	 * "AutomatedBy", "JavaTestClass", "JavaTestMethod", "Automation_Status" for
	 * every TestCase.
	 * 
	 * @param SERVER_URL
	 * @param testCaseExternalId
	 * @param DEV_KEY
	 * @param testCaseId
	 * @param projectId
	 * @param customFieldName
	 * @param details
	 */
	public static void getTestCaseCustomFieldValuesData(String SERVER_URL, int testCaseExternalId, String DEV_KEY, Integer testCaseId, Integer projectId, String customFieldName, ResponseDetails details)
	{
		URL url = null;
		try
		{
			url = new URL(SERVER_URL);
			TestLinkAPI api = new TestLinkAPI(url, DEV_KEY);
			CustomField custom = api.getTestCaseCustomFieldDesignValue(testCaseId, testCaseExternalId, 1, projectId, customFieldName, details);
			System.out.println(customFieldName + " is : " + custom.getValue());
			
		}
		catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This function verifies if given "testcase" is present in "Regression"
	 * testPlan or not
	 * 
	 * @param SERVER_URL
	 * @param DEV_KEY
	 * @param testPlanName
	 *            - "Regression" etc
	 * @param projectName
	 *            - PayU/Paisa
	 * @param methodName
	 *            - name of testcase to be verified
	 * @param testcaseId
	 *            - testcaseId (testlinkId)
	 * @param testCases
	 *            - all the testcases of "Regrassion" testPlan
	 */
	public static void getTestPlanNames_ForThis_Testcase(String SERVER_URL, String DEV_KEY, String testPlanName, String projectName, String methodName, int testCaseId, TestCase[] testCases)
	{
		Boolean presentInTestPlan = false;
		// TestCase[] testCases =
		// ImportTestCases.getTestCasesForTestPlan(SERVER_URL,
		// DEV_KEY,testPlanName, projectName);
		for (TestCase testCase : testCases)
			if (testCase.getId().equals(testCaseId))
				presentInTestPlan = true;
		
		if (presentInTestPlan == false)
			System.out.println("<----------Method name : " + methodName + " dont exist in the TestPlan : " + testPlanName + "---------->");
	}
	
	public static void main(String args[]) throws ClassNotFoundException, IOException, InvocationTargetException, InterruptedException
	{
		// Use the event dispatch thread for Swing components
		EventQueue.invokeAndWait(new Runnable()
		{
			@Override
			public void run()
			{
				// create GUI frame
				try
				{
					new Verify_AllTestCases_Present_In_TestLink().setVisible(true);
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * This function is created to find out all the TestClasses/TestCases which
	 * are present in Test Package but not present in TestLink
	 * 
	 * @param SERVER_URL
	 * @param DEV_KEY
	 * @param className
	 *            - name of testClass to be verified
	 * @param jobName
	 *            - "RunTestLink"
	 * @param projectName
	 *            - PayU/Paisa
	 * @param authorName
	 *            - Name of author who automate the code
	 * @param testCases
	 *            - all the testcases of "Regrassion" testPlan
	 * @throws IOException
	 */
	public static void verifyTestSuites(String SERVER_URL, String DEV_KEY, String className, String jobName, String projectName, String authorName, TestCase[] testCases) throws IOException
	{
		String filePath = "C:\\PayU\\" + projectName + "\\" + projectName + "GroupNames.xls";
		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);
		
		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet = workbook.getSheetAt(0);
		
		int lastRow = sheet.getLastRowNum();
		HashMap<String, Integer> groupsMap = new HashMap<String, Integer>();
		Integer projectId = ImportTestCases.getProjectId(SERVER_URL, DEV_KEY, projectName);
		
		for (int i = 1; i <= lastRow; i++)
		{
			if (sheet.getRow(i).getCell(4) != null)
				groupsMap.put(sheet.getRow(i).getCell(1).toString(), Integer.parseInt(sheet.getRow(i).getCell(4).toString()));
			else
				groupsMap.put(sheet.getRow(i).getCell(1).toString(), 0);
		}
		
		String className1 = className;
		String[] classSuites = className.split("\\.");
		StringBuffer createdPart = new StringBuffer();
		String notCreatedPart = null;
		// Boolean isEnd = false;
		String groupClassName = null;
		Integer matchCnt = 0;
		Integer drpcnt = 0;
		List<Integer> classSuiteId = new ArrayList<Integer>();
		Boolean isPresent = false;
		String groupClassName1 = null;
		
		for (int i = 0; i < classSuites.length; i++)
		{
			// iterate through groups to compare with classSuites[i]
			Set<String> itr = groupsMap.keySet();
			Iterator<String> iterator = itr.iterator();
			
			while (iterator.hasNext())
			{
				String key = iterator.next().toString();
				groupClassName = key;
				String[] groupSuites = groupClassName.split("\\.");
				if (groupSuites.length > i)
				{
					
					if (classSuites[i].contentEquals(groupSuites[i]))
					{
						isPresent = true;
						// remove old entry
						if (!classSuiteId.isEmpty())
							classSuiteId.remove(0);
						classSuiteId.add(groupsMap.get(groupClassName));
						groupClassName1 = groupClassName;
						createdPart = createdPart.append(groupSuites[i]);
						matchCnt = i + 1;
						drpcnt = classSuites.length - matchCnt;
						if (i != classSuites.length - 1)
							createdPart.append(".");
						break;
					}
				}
			}
			if (!isPresent)
				break;
			isPresent = false;
		}
		
		System.out.println("Created Part is : " + createdPart);
		System.out.println("Class Suite Id of " + groupClassName1 + " is " + classSuiteId.get(0));
		
		try
		{
			URL url = new URL(SERVER_URL);
			TestLinkAPI api = new TestLinkAPI(url, DEV_KEY);
			Class classObj = Class.forName(className);
			TestCase[] presentTestCasesInTL;
			
			/*
			 * //Uncomment it to know the TestLinkSuitId of your Project Folder
			 * then TestFolder and then other folders inside TEST folder //In
			 * order to create a new MoneyGroupNames sheet you have to pass
			 * TestLinkSuitId(in the TestLinkSuitId column) of all the folders
			 * listed inside the TEST folder. TestProject Project_Id =
			 * api.getTestProjectByName(projectName); TestSuite[] Test_Id =
			 * api.getFirstLevelTestSuitesForTestProject(15216);// Money=15216;
			 * Test=15236 TestSuite[] IdsOfFoldersUnderTest =
			 * api.getTestSuitesForTestSuite(15236);
			 */
			
			if (createdPart.length() == className1.length())
			{
				// suite has already been created for class. So need to only add
				// new testcases for newly created test methods
				Integer suiteId = classSuiteId.get(0);
				try
				{
					presentTestCasesInTL = api.getTestCasesForTestSuite(suiteId, null, null);
				}
				catch (Exception e)
				{
					presentTestCasesInTL = null;
					System.out.println("<----------TestLink Suite Id MisMatch for this Test Class---------->");
				}
				
				for (Method method : classObj.getMethods())
				{
					Annotation annotation = method.getAnnotation(Test.class);
					Boolean isTestCasePresent = false;
					Test test = (Test) annotation;
					if (method.isAnnotationPresent(Test.class) && (presentTestCasesInTL != null))
					{
						String methodName = method.getName();
						int testCaseId = 0;
						for (int i = 0; i < presentTestCasesInTL.length; i++)
						{
							if (presentTestCasesInTL[i].getName().contentEquals(methodName))
							{
								isTestCasePresent = true;
								testCaseId = presentTestCasesInTL[i].getId();
							}
						}
						
						if (!isTestCasePresent)
							System.out.println("----------> Test Case not added in TestLink : " + methodName);
						else
						{
							getTestPlanNames_ForThis_Testcase(SERVER_URL, DEV_KEY, "Regression", projectName, methodName, testCaseId, testCases);
							
							System.out.println("<----------For TestCase : " + methodName + "---------->");
							getTestCaseCustomFieldValuesData(SERVER_URL, 1, DEV_KEY, testCaseId, projectId, "AutomatedBy", null);
							getTestCaseCustomFieldValuesData(SERVER_URL, 1, DEV_KEY, testCaseId, projectId, "JavaTestClass", null);
							getTestCaseCustomFieldValuesData(SERVER_URL, 1, DEV_KEY, testCaseId, projectId, "JavaTestMethod", null);
							getTestCaseCustomFieldValuesData(SERVER_URL, 1, DEV_KEY, testCaseId, projectId, "Automation_Status", null);
							System.out.println("TestCaseId is : " + testCaseId);
						}
					}
				}
			}
			
			else
			{
				notCreatedPart = (String) className1.subSequence(createdPart.length(), className1.length());
				System.out.println("Not created part is : " + notCreatedPart);
				
				// Get number of times new packages have to be created
				String[] notCreated = notCreatedPart.split("\\.");
				ArrayList<String> finalNotCreated = new ArrayList<String>();
				for (int i = 0; i < notCreated.length; i++)
					if (!notCreated[i].isEmpty())
						finalNotCreated.add(notCreated[i]);
				
				System.out.println("<----------Test Class not added in TestLink---------->");
				
				/*
				 * //If class is not added in testlink and you want to add it
				 * directly then uncomment this code and change the author name
				 * etc accordingly String groupName = "";
				 * if((!className.contains
				 * ("MoneyTestHelper"))||(!className.contains
				 * ("deleteAllMerchantTools"))) { groupName =
				 * className.substring(className.lastIndexOf(".")+1);
				 * ImportTestCases.addOnlyNewSuites(SERVER_URL,
				 * DEV_KEY,groupName
				 * ,className,"FALSE",jobName,projectName,"mukesh.rajput",""); }
				 */}
		}
		catch (MalformedURLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * This function populate a Pop Up (GUI Frame) to select the Project Name
	 * and then call the verifyTestSuites() function for every TestClass.
	 * 
	 * @throws IOException
	 */
	public Verify_AllTestCases_Present_In_TestLink() throws IOException
	{
		final String DEV_KEY = "d5539eafe6572eaeddf07db1fc674cac";
		final String SERVER_URL = "http://payu-testlink.com/lib/api/xmlrpc/v1/xmlrpc.php";
		
		// Using a standard Java icon
		Icon optionIcon = UIManager.getIcon("FileView.computerIcon");
		String[] projectType = { "Product", "Money" };
		
		// make sure the program exits when the frame closes
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500, 300);
		
		// This will center the JFrame in the middle of the screen
		setLocationRelativeTo(null);
		
		// Combo box of project types
		JComboBox projectList = new JComboBox(projectType);
		projectList.addActionListener(this);
		System.out.println("Select your Project from the dropdown appearing in the Pop Up");
		String projectName = (String) JOptionPane.showInputDialog(this, "Pick a Name:", "ComboBox Dialog", JOptionPane.QUESTION_MESSAGE, optionIcon, projectType, projectType[0]);
		System.out.println("Project Name entered : " + projectName);
		
		String pathName = "C:\\PayU\\" + projectName + "\\target\\classes\\Test\\";
		String packageName = "Test";
		final String jobName = "RunTestLink";
		try
		{
			List<Class> classes = new ArrayList<Class>();
			classes = ImportTestCases.findClasses(new File(pathName), packageName);
			
			// Used to verify the test Plan (Here storing all the testcases of
			// "Regression" testPlan into testCases Array and passing it into
			// the function)
			System.out.println("Loading...");
			TestCase[] testCases = ImportTestCases.getTestCasesForTestPlan(SERVER_URL, DEV_KEY, "Regression", projectName);
			Object[] obj = classes.toArray();
			for (int i = 0; i < obj.length; i++)
			{
				String className = obj[i].toString().substring(6);
				System.out.println("\nVerifying Test Class : " + className);
				verifyTestSuites(SERVER_URL, DEV_KEY, className, jobName, projectName, "mukesh.rajput", testCases);
			}
			
		}
		catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		// TODO Auto-generated method stub
	}
}
