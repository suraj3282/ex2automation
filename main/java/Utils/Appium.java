package Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import ru.yandex.qatools.allure.annotations.Attachment;

public class Appium
{
	
	private static String localAppiumAddress;
	private static String localappiumport;
	public static AppiumDriver driver;
	
	public static void adbListDevices(Config testConfig)
	{
		Process process = null;
		Runtime runtime = Runtime.getRuntime();
		try
		{
			process = runtime.exec(testConfig.ADBLISTDEVICES);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			Thread.sleep(5000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (process != null)
		{
			
			testConfig.logComment("Listed ADB Devices");
		}
	}
	
	/**Closes application/browser opened
	 * @param testConfig
	 */
	public static void close(Config testConfig)
	{
		try
		{
			String browser = testConfig.getRunTimeProperty("Browser");
			if (browser.equalsIgnoreCase("android_web"))
			{
				testConfig.logComment("Closing Browser");
				testConfig.appiumDriver.close();
			}
			else
			{
				testConfig.logComment("Closing Application");
				testConfig.appiumDriver.closeApp();
			}
		}
		catch(Exception e)
		{
			testConfig.logComment("Unable to close App. Seems it already closed.");
		}
		
		try
		{
			testConfig.appiumDriver.quit();
			testConfig.appiumDriver = null;
			testConfig.driver = null;
		}
		catch(Exception e)
		{
			testConfig.appiumDriver = null;
			testConfig.driver = null;
		}
	}
	
	public static void closeBrowser(Config testConfig)
	{
		testConfig.logComment("Closing Browser");
		testConfig.appiumDriver.close();
		testConfig.appiumDriver.quit();
	}
	
	public static void killAdbServer(Config testConfig)
	{
		Process process = null;
		Runtime runtime = Runtime.getRuntime();
		try
		{
			process = runtime.exec(testConfig.ADBSERVERKILL);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (process != null)
		{
			
			testConfig.logComment("Killed Adb servers");
		}
	}
	
	public static void killAllActiveNodeServers(Config testConfig)
	{
		Process process = null;
		Runtime runtime = Runtime.getRuntime();
		try
		{
			process = runtime.exec(testConfig.NODESERVERKILL);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (process != null)
		{
			
			testConfig.logComment("All Node Servers killed");
		}
	}
	
	public static AppiumDriver openApplication(Config testConfig)
	{
		DesiredCapabilities capabilities = new DesiredCapabilities();
		String platFormName = testConfig.getRunTimeProperty("platformName");
		String deviceName = null;
		switch(platFormName)
		{
			case "Android":
				String apkEnvironment = testConfig.getRunTimeProperty("Environment");
				testConfig.putRunTimeProperty("apkEnvironment", apkEnvironment.toUpperCase());
				String apkUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, testConfig.getRunTimeProperty("APKUrl"));
				String platformName = testConfig.getRunTimeProperty("platformName");
		        deviceName = testConfig.getRunTimeProperty("deviceName");
		        String platformVersion = testConfig.getRunTimeProperty("platformVersion");
		        String appPackage = testConfig.getRunTimeProperty("appPackage");
		        String appActivity = testConfig.getRunTimeProperty("appActivity");
		        String timeout = testConfig.getRunTimeProperty("newCommandTimeout");
		        String appWaitActivity = testConfig.getRunTimeProperty("appWaitActivity");
		        String uninstallApk = testConfig.getRunTimeProperty("UninstallApk");
		        
		        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
		        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		        if (!deviceName.equalsIgnoreCase("device") && testConfig.remoteExecution)
		        	capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, deviceName);
		        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION,platformVersion);
		        capabilities.setCapability(MobileCapabilityType.APP,apkUrl);
		        capabilities.setCapability(MobileCapabilityType.APP_PACKAGE, appPackage);
		        capabilities.setCapability(MobileCapabilityType.APP_ACTIVITY, appActivity);
		        capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, timeout);
		        if(uninstallApk!=null&&uninstallApk.equalsIgnoreCase("true"))
		        {
		        	capabilities.setCapability("fullReset", true);
		        }
		        if(appWaitActivity != null)
		        	capabilities.setCapability(MobileCapabilityType.APP_WAIT_ACTIVITY, appWaitActivity);
				break;
			case "iOS":
				deviceName = testConfig.getRunTimeProperty("deviceName");
		        String udid = testConfig.getRunTimeProperty("udid");
		        String bundleid = testConfig.getRunTimeProperty("bundleid");
		        String ipa = testConfig.getRunTimeProperty("ipa");
		        capabilities.setCapability("deviceName",deviceName);
		        capabilities.setCapability("udid", udid);
		        capabilities.setCapability("bundleId", bundleid);
		        capabilities.setCapability("ipa", ipa);
				break;
			default:
				testConfig.logFail("Platform Type"+ platFormName +" Not define");
		}

		driver = getAppiumDriver(testConfig, capabilities);
		return driver;
	}
	
	private static AppiumDriver getAppiumDriver(Config testConfig, DesiredCapabilities capabilities)
	{
		int tries = 10;
		try
		{			
			if (testConfig.remoteExecution)
			{
				String remoteAddress = testConfig.getRunTimeProperty("RemoteAddress");
				String remotePort = testConfig.getRunTimeProperty("RemoteHostPort");
				testConfig.logComment("Start Execution on Remote : http://" + remoteAddress + ":" + remotePort);
				String platFormName = testConfig.getRunTimeProperty("platformName");
				switch(platFormName)
				{
					case "Android":
						driver = new AndroidDriver(new URL("http://"+ remoteAddress+":"+remotePort+"/wd/hub"), capabilities);
						break;
					case "iOS":
						driver = new IOSDriver(new URL("http://"+ remoteAddress+":"+remotePort+"/wd/hub"), capabilities); 
						break;
					default:						
						driver= null;
						testConfig.logFail("Platform Type"+ platFormName +" Not define");
				}
			}
			else
			{

				localAppiumAddress = testConfig.getRunTimeProperty("appiumAddress");
				localappiumport= testConfig.getRunTimeProperty("appiumPort");
				testConfig.logComment("Start Execution on Local Machine: http://" + localAppiumAddress + ":" + localappiumport);

				String platFormName = testConfig.getRunTimeProperty("PlatformName");
				while (tries > 0)
				{
					try
					{
						switch(platFormName)
						{
							case "Android":
								startAppiumServer(testConfig,localAppiumAddress,localappiumport);
								driver = new AndroidDriver(new URL("http://" + localAppiumAddress + ":" + localappiumport+"/wd/hub"), capabilities);
								break;
							case "iOS":
								// We are not Starting Appium Server for iOS local Execution on Mac beacuse there is a issue for which solution is yet to be found
								driver = new IOSDriver(new URL("http://" + localAppiumAddress + ":" + localappiumport+"/wd/hub"), capabilities);
								break;
							default:
								driver= null;
								testConfig.logFail("Platform Type Not define");
								
						}
						tries = 0;
					}
					catch (org.openqa.selenium.SessionNotCreatedException e)
					{
						testConfig.logWarning("Session not created because:" + e.getMessage());
						Browser.wait(testConfig, 10);
						tries--;
					}	
				}
			}
			if (driver == null)
			{
				testConfig.logFail("Appium Session Not Created");
				testConfig.endTest(null);
			}

			testConfig.putRunTimeProperty("MobileUAFlag", "true");
			//attachDeviceInformation(testConfig,driver);
			testConfig.appiumDriver = driver;

			return driver;
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			return null;
		}
		catch(Exception e)
		{
			testConfig.logComment(e.getMessage());
			return null;
		}
	}
	
	//TODO When on 1.4+ appium, capabilities return device id
	//which can be used to identify device
	@Attachment(value="Device Capabilities")
	private static String attachDeviceInformation(Config testConfig,AppiumDriver driver)
	{
		Capabilities capabilities = driver.getCapabilities();
		testConfig.logComment(capabilities.toString());
		return capabilities.toString();
	}

	public static AppiumDriver openBrowser(Config testConfig)
	{
		// Getting values from the application's config file
		String platformName = testConfig.getRunTimeProperty("platformName");
		String deviceName = testConfig.getRunTimeProperty("deviceName");
		String platformVersion = testConfig.getRunTimeProperty("platformVersion");
		String browserName = testConfig.getRunTimeProperty("mobileBrowser");
		
		// set up appium		
		DesiredCapabilities capabilities = DesiredCapabilities.android();
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, platformName);
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, browserName);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceName);
		capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, platformVersion);
		
		driver = getAppiumDriver(testConfig, capabilities); 
		return driver;
	}
	
	public static void startAdbServer(Config testConfig)
	{
		Process process = null;
		Runtime runtime = Runtime.getRuntime();
		try
		{
			process = runtime.exec(testConfig.ADBSERVERSTART);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (process != null)
		{
			
			testConfig.logComment("ADB server started");
		}
	}
	
	public static void startAppiumServer(Config testConfig,String address,String port)
	{
		String platformName = testConfig.getRunTimeProperty("platformName");
		String platformVersion = testConfig.getRunTimeProperty("platformVersion");
		String osType = System.getProperty("os.name");
		if (platformName.equalsIgnoreCase("Android"));
		{
			// Kill all node servers
			if(Config.fileSeparator.equals("\\"))
				killAllActiveNodeServers(testConfig);
			// Restart adb server
			killAdbServer(testConfig);
			startAdbServer(testConfig);
			adbListDevices(testConfig);
			//uninstallApk(testConfig);
			clearAPKData(testConfig);
		}
		Runtime runtime = Runtime.getRuntime();
		try
		{
			if (!osType.startsWith("Window"))
			{
				// Start Appium Server on MAC OSX
				// Path for Apium has been hard coded for Mac as all the Softwares are installed in a single location
				CommandLine command = new CommandLine("/Applications/Appium.app/Contents/Resources/node/bin/node");
				command.addArgument("/Applications/Appium.app/Contents/Resources/node_modules/appium/bin/appium.js", false);
				command.addArgument("--address", false);
				command.addArgument(address);
				command.addArgument("--port", false);
				command.addArgument(port);
				command.addArgument("--command-timeout",false);
				command.addArgument("7200");
				command.addArgument("--platform-version",false);
				command.addArgument(platformVersion);
				command.addArgument("--platform-name",false);
				command.addArgument(platformName);
				command.addArgument("--show-ios-log",false);
				command.addArgument("--default-device",false);
				command.addArgument("--no-reset", false);
				DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
				DefaultExecutor executor = new DefaultExecutor();
				executor.setExitValue(1);
				executor.execute(command, resultHandler);
			}
			else
			{
				// Start Appium Server on Windows Machine
				String appium_path = System.getenv("APPIUM_HOME");
				if (appium_path == null)
				{
					testConfig.logComment("Appium Home Has not been set");
					testConfig.appiumServer = runtime.exec(testConfig.APPIUMSERVERSTART+" -a "+address+" -p "+port);
				}
				else
				{
					String commandToExecute = "\"" + appium_path + "/node.exe\" \"" + appium_path + "/node_modules/appium/bin/appium.js\" ";
					commandToExecute += " --log " + System.getProperty("user.dir") + "\\Appium.log ";// Log
																										// file
					commandToExecute += " -p " + localappiumport;// Port for appium to
															// listen on
					commandToExecute += " -a " + localAppiumAddress;// address for appium port
					// commandToExecute += " -U " + "015d4b10091bee15";// To get a
					// particular device
					
					testConfig.logComment("Running Command " + commandToExecute);
					testConfig.appiumServer = runtime.exec(commandToExecute);
				}
			}
			
		}
		catch (IOException e)
		{
			e.printStackTrace();
			testConfig.logComment("Error in Starting Appium Server: " + e.getMessage());
		}
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
			testConfig.logComment("Error in Starting Appium Server: " + e.getMessage());
		}
		if (testConfig.appiumServer != null && osType.startsWith("Window"))
		{
			
			testConfig.logComment("Appium server started");
		}
	}
	
	public static void stopAppiumServer(Config testConfig)
	{
		try
		{
			if (testConfig.appiumServer != null)
			{
				testConfig.appiumServer.destroy();
				try
				{
					// Wait for some time till process gets destroyed
					Thread.sleep(15000);

				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
			testConfig.logComment("Appium server stopped");
		}
		catch(Exception e)
		{
			testConfig.logComment("Unable to stop Appium server.");
		}
	}
	
	/**
	 * This function uninstalls the Payusdk apk.
	 * @param testConfig
	 */
	public static void uninstallApk(Config testConfig)
	{
		Process process = null;
		Runtime runtime = Runtime.getRuntime();
		try
		{
			process = runtime.exec(testConfig.ADBUNINSTALLAPK);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (process != null)
		{
			
			testConfig.logComment("APK Uninstalled");
		}
	}
	
	/**
	 * Clear APK data
	 * @param testConfig
	 * @return true/false
	 */
	public static boolean clearAPKData(Config testConfig)
	{
		Process process = null;
		Runtime runtime = Runtime.getRuntime();
		boolean apkDataCleared = false;
		try
		{
			process = runtime.exec(testConfig.ADBCLEARDATAAPK);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		try
		{
			Thread.sleep(3000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		if (process != null)
		{
			if(getProcessResponce(process).equalsIgnoreCase("Success"))
			{
				testConfig.logComment("APK Data cleared");
				apkDataCleared = true;
			}
			else
			{
				testConfig.logComment("APK Data could not cleared");
				apkDataCleared = false;
			}
		}
		
		return apkDataCleared;
	}
	
	/**
	 * Get CMD Process Responce
	 * @param process
	 * @return responce
	 */
	public static String getProcessResponce(Process process)
	{
		BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		//BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));

		String allResponce = "";
		String responce = null;
		try
		{
			while ((responce = stdInput.readLine()) != null)
			{
				allResponce = allResponce + responce;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return allResponce;
		/*
		// read any errors from the attempted command
		System.out.println("Here is the standard error of the command (if any):\n");
		try 
		{
			while ((responce = stdError.readLine()) != null)
			{
				allResponce = allResponce + responce;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}*/
	}
}
