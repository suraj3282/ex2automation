package Utils;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import Utils.Config.Project;
import Utils.DataBase.DatabaseType;

public class ForAccuracy
{
	public static String accuracyPercent = "";
	/**
	 * This function is used to parse txt file containing all the known testcase names and convert to HashMap
	 * @param locationOfTxtFile - Location of Txt File
	 * @param projectName - Money/Project
	 * @return - HashMap containing GroupName and count of number of testcases failed due to known issues in that GroupName
	 * @throws IOException
	 */
	private static HashMap<String, Integer> getGroupNamesOfKnownFailingTestcases(String locationOfTxtFile, String projectName) throws IOException
	{
		//Code to Read Txt file and save all details in HashMap
		HashMap<String, Integer> failureData = new HashMap<String, Integer>();
		BufferedReader br = null;
		try 
		{
			br = new BufferedReader(new FileReader(locationOfTxtFile));
		    StringBuilder sb = new StringBuilder();
		    String line = br.readLine();

		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    
		    String[] testcases = sb.toString().split(",");
		    String testclass = "";
		    
		    
		    for(int i=0; i<testcases.length-1; i++)
		    {
			    int counter = 0;
			    testclass = testcases[i].substring(0, testcases[i].lastIndexOf("."));
		    	
		    	if(failureData.containsKey(testclass))
		    		counter = failureData.get(testclass);
		    	
		    	failureData.put(testclass, ++counter);
		    }
		    br.close();
		} 
		catch(FileNotFoundException e)
		{
			System.out.println("<----- File containing Known Issues not found, so assuming no testcase failed due to Existing Issue ----->");
			failureData.put("No Data Found", 0);
		}
		
		//Code to read GroupNames sheet and get the Group Names of testcases present in Hashmap 
		//and then saving these details in another HashMap
		String filePath = System.getenv("JENKINS_HOME") + File.separator + "jobs" + File.separator + "InternalExecution"+ File.separator + "workspace" + File.separator + projectName + File.separator + projectName + "GroupNames.xls";
		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);
		
		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet = workbook.getSheetAt(0);
		
		int lastRow = sheet.getLastRowNum();
		HashMap<String, Integer> groupsMap = new HashMap<String, Integer>();
		
		for (int j = 1; j <= lastRow; j++)
		{
			String className = sheet.getRow(j).getCell(1).toString();
			String groupName = sheet.getRow(j).getCell(0).toString();
			
			if(failureData.containsKey(className))
				groupsMap.put(groupName, failureData.get(className));
		}
		
		int totalCases = 0;
		for (HashMap.Entry<String,Integer> entry : groupsMap.entrySet())
	    {
	    	  String key = entry.getKey();
	    	  int value = entry.getValue();
	    	  totalCases = totalCases + value;
	    	  System.out.println(key +" : "+ value);
	    }
		groupsMap.put("Total", totalCases);
		return groupsMap;
	}
	
	/**
	 * This function is used to create the "FailureIssues" and "Accuracy" columns in the existing email.
	 * @param emailContent - Content of existing email
	 * @param projectName - Money/Product
	 * @return - Updated content of email
	 */
	public static String addAccuracyColumns(String emailContent, String projectName)
	{
		try
		{
			//Code to getGroupNamesOfKnownFailingTestcases in HashMap format
			String locationOfTxtFile = System.getProperty("testngOutputDir") + "/KnownFailingTestCases.txt";
			//String locationOfTxtFile = "C:\\RegressionResults\\Money\\Results\\jenkins-InternalExecution-388\\MobileApplication\\KnownFailingTestCases.txt";
			HashMap<String, Integer> knownFailures = ForAccuracy.getGroupNamesOfKnownFailingTestcases(locationOfTxtFile, projectName);
			
			// Modifying Email Body Message by adding 2 more columns
			emailContent = emailContent.replaceAll("colspan=\"6\"","colspan=\"8\"");
			emailContent = emailContent.replaceAll("<th>Pass Rate</th>", "<th>Pass Rate</th>\n<th>KnownFailures</th>\n<th>Accuracy</th>");
			String temp = emailContent;
			Document doc2 = Jsoup.parse(temp);
			
		    Document doc1 = Jsoup.parse(emailContent);
		    Elements tableElements = doc1.select("table");
		    Elements tableRowElements = tableElements.select(":not(thead) tr");
		    int totalCases = 0;
		    for (int i = 2; i < tableRowElements.size(); i++) 
		    {
		            Element row = tableRowElements.get(i);
		            Elements rowItems = row.select("td");
		            String groupName = rowItems.get(0).text();
		            
		            //String accuracyPercent = null;
		            String passPercent = null;
		            Integer knownFailedCases = knownFailures.get(groupName);
		            if(knownFailedCases != null)
		            {
		            	int success = 0;
		            	int skip = 0;
		            	int fail = 0;
			            if(i != tableRowElements.size()-1)
			            {
			            	success = Integer.parseInt(rowItems.get(2).text());
				            skip = Integer.parseInt(rowItems.get(3).text());
				            fail = Integer.parseInt(rowItems.get(4).text());
				            passPercent = rowItems.get(5).text();
			            }
			            //Code to handle Last Line of email('Total' of all columns)
			            else
			            {
			            	success = Integer.parseInt(rowItems.get(1).text());
				            skip = Integer.parseInt(rowItems.get(2).text());
				            fail = Integer.parseInt(rowItems.get(3).text());
				            passPercent = rowItems.get(4).text();
			            }
			            totalCases = success + skip + fail;
			            
			            try
			            {
			            	double accuracy = ((success+knownFailedCases)*100.00)/totalCases;
			            	DecimalFormat df = new DecimalFormat("0.00");
			            	accuracyPercent = df.format(accuracy)+"%";
			            }
			            catch(ArithmeticException e)
			            {
			            	accuracyPercent = "0.00%";
			            	System.out.println("<----- Divide by Zero exception, looks like TotalTestCases run are ZERO ----->");
			            }
		            }
		            else
		            {
		            	knownFailedCases = 0;
		            	passPercent = rowItems.get(5).text();
		            	
		            	try//To handle 'N/A case'
		            	{
		            		passPercent = passPercent.split("%")[0];
		            		double temp2 = Double.parseDouble(passPercent);
		            		DecimalFormat df = new DecimalFormat("0.00");
			            	passPercent = df.format(temp2);
			            	accuracyPercent = passPercent+"%";
		            	}
		            	catch(NumberFormatException e)
		            	{
		            		accuracyPercent = passPercent;
		            	}
		            }
		            
		            System.out.println("\nGroup Name : "+groupName);
		            System.out.println("Known Failures : "+knownFailedCases);
		            System.out.println("Accuracy : "+accuracyPercent);
		            
		    		DecimalFormat df = new DecimalFormat("0.##");
		    		String knownCases = df.format(knownFailedCases);
				    Element knownFailuresColumn = doc2.createElement("td").addClass("zero number").html("<center>"+knownCases+"</center>");
				    Element accuracyColumn = doc2.createElement("td").addClass("passRate").html(accuracyPercent);
				    
				    Elements tableElements2 = doc2.select("table");
				    Elements tableRowElements2 = tableElements2.select(":not(thead) tr");
		            Element row2 = tableRowElements2.get(i);
		            row2 = row2.appendChild(knownFailuresColumn);
		            row2 = row2.appendChild(accuracyColumn);
		     }
		    emailContent = doc2.toString();
		}
		catch (IOException e)
		{
			System.out.println("<----- Unable to parse email content ----->");
        	System.out.println("**************Email Content*****************");
       		System.out.println(emailContent);
       		System.out.println("********************************************");
			e.printStackTrace();
		}
		return emailContent;
	}
	
	public static String verifyInAccuracyDB(Config testConf, String actualExceptionMessage)
	{
		String redmineId = "";
		String actualFailureReason = actualExceptionMessage.replaceAll("\n", " ").replaceAll("\r", "");
		String testDataSheetName = testConf.getRunTimeProperty("TestDataSheet");
		try
		{
			testConf.putRunTimeProperty("testName", testConf.testName);
			
			testConf.update_TestDataSheet_value(Project.Common);
			Map<String, String> mapdata = null;
			
			ResultSet resultSet = DataBase.executeSelectQuery(testConf, 1, DatabaseType.AccuracyDB);
			
			while (resultSet.next())
			{
				mapdata = DataBase.addToRunTimeProperties(testConf, resultSet);
				
				if(mapdata != null)
				{
					String temp = compareMessages(mapdata, actualFailureReason);
					if(temp != null)
						redmineId = temp;
				}
				else
				{
					if(Config.genericErrors.isEmpty())
						Config.genericErrors = DataBase.executeSelectQuery(testConf, DatabaseType.AccuracyDB, 2);
					
					if(!Config.genericErrors.isEmpty())
					{
						for(int i=1; i<=Config.genericErrors.size(); i++)
						{
							mapdata = Config.genericErrors.get(i);
							String temp = compareMessages(mapdata, actualFailureReason);
							if(temp != null)
								redmineId = redmineId.concat(temp +",");
						}
					}
				}
			}
		}
		catch(Exception e)
		{
			testConf.logComment("<----- Exception appeared in verifyInAccuracyDB Code ----->");
			testConf.logException(e);
		}
		testConf.putRunTimeProperty("TestDataSheet", testDataSheetName);
		return redmineId;
	}
	
	private static String compareMessages(Map<String, String> mapdata, String actualMessage)
	{
		String redmineId = null;
		String expectedMessage_partial = "";
		
		int beginIndex = Integer.parseInt(mapdata.get("startCounter"));
		int endIndex = Integer.parseInt(mapdata.get("endCounter"));
		String expectedMessage = mapdata.get("FailureReason").replaceAll("\n", " ").replaceAll("\r", "");
		
		if(beginIndex == 0 && endIndex == -1 && actualMessage.equals(expectedMessage))
			redmineId = mapdata.get("RedmineId");
		
		else if(beginIndex != 0 && endIndex == -1)
			expectedMessage_partial = expectedMessage.substring(beginIndex);
		else if(endIndex != -1)
			expectedMessage_partial = expectedMessage.substring(beginIndex, endIndex);

		if(!expectedMessage_partial.equals("") && actualMessage.contains(expectedMessage_partial))
			redmineId = mapdata.get("RedmineId");
		
		return redmineId;
	}
}