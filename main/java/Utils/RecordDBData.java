package Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.mysql.jdbc.ResultSetMetaData;

import Utils.Config.Project;
import Utils.DataBase.DatabaseType;

public class RecordDBData  
{
	
	/** Create HTML file for Table's records of DataBase
	 * 	
	 * @param testConfig
	 * @param hmp contains SQL rowNum of Common Excel-sheet and Corresponding value of DatabaseType enum constant
	 * @return URL of HTML file for  Instance DB records 
	 */
	public static void createHtmlFileWithDBData(Config testConfig)
	{
	    StringBuilder htmlData =new StringBuilder();
	    HashMap<Integer,Integer> hmp = new HashMap<Integer,Integer>();
		
		// FilePath contains URL where HTML file is created
		String filePath = new String(testConfig.getRunTimeProperty("ResultsDir") + File.separator + "DBData" + File.separator + testConfig.getTestName() + ".html");
	    
	    //HTMLTemplate of HTML file
	    String htmlTemplate ="<!DOCTYPE html PUBLIC  \"" +
	            "\"http://www.w3.org/TR/html4/loose.dtd\">\n" +
	            "<html>\n" +
	            "<head>\n" +
	            "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n" +
	            "<title>DB Data For Failed TestCase</title>\n" +
	            "<style>" +
	            "hr{ margi\n-top: 2em;margin-bottom: 2em;}" +
	            "table { width:100%;}"+
	            "table, th, td {   " +
	            "border: 1px solid black;" +
	            "border-collapse: collapse; }" +
	            "th, td {   padding: 5px;    text-align: left; }" +
	            "table#t01 td:nth-child(even) {  background-color: #eee; }" + 
	            "table#t01 td:nth-child(odd) {    background-color:#fff;  }" +
	            "table#t01 th	{   background-color: GreenYellow ;  color: black;}" +
	            "</style>" +
	            "</head>\n" +
	            "<body>\n" + 
	            "<h1 style=\"color:blue;font-size: 1.5em;margin-top: 1em;margin-bottom: 1em;margin-left: 0;margin-right: 0;font-weight: bold;align = \"Top\">DATA FOR TESTCASE : " +
	            testConfig.testName.substring(testConfig.testName.lastIndexOf(".")+1) + " </h1>";
	   
	  htmlData.append(htmlTemplate);
	  hmp = RecordDBData.getListOfSQLQuery(testConfig);
	  
	    //Update testConfig property to pick SQL row from Common sheet
		testConfig.update_TestDataSheet_value(Project.Common);
		String sqlSheetName = "SQLForMoney";
		String projectName = testConfig.getRunTimeProperty("ProjectName");
	  
		//Putting data needed in RunTime for Money Project like : userId, merchantId and txnId 
		if(projectName.equalsIgnoreCase("Money"))
		{
			//Table from where data needs to be fetched, will be decided on the basis of value of current environment
			String env = testConfig.getRunTimeProperty("environment");
			if(env.contains("Production"))
				testConfig.putRunTimeProperty("envInitial", "production");
			else if(env.equalsIgnoreCase("pp9"))
				testConfig.putRunTimeProperty("envInitial", "pp9");
			else if(env.equalsIgnoreCase("pp5"))
				testConfig.putRunTimeProperty("envInitial", "pp5");
			else
				testConfig.putRunTimeProperty("envInitial", "pp");
			
			testConfig.removeRunTimeProperty("userId");
			if (testConfig.customerId != null)
			{
				testConfig.putRunTimeProperty("id", testConfig.customerId);
				
				ResultSet userData = DataBase.executeSelectQuery(testConfig, 16, DatabaseType.PayuMoneyAutomation, sqlSheetName);
				try
				{
					userData.absolute(1);
					testConfig.putRunTimeProperty("userId", userData.getString("payumoneyuserid"));
				}
				catch(NullPointerException | SQLException e)
				{
					testConfig.logWarning("Entry not found in UserManagement Table for this customer");
				}
			}
			else
			{
				System.out.println("Value of testConfig.customerId is NULL");
			}
			
			testConfig.removeRunTimeProperty("merchantId");
			if (testConfig.merchantId != null)
			{
				testConfig.putRunTimeProperty("id", testConfig.merchantId);
				ResultSet merchantData = DataBase.executeSelectQuery(testConfig, 17, DatabaseType.PayuMoneyAutomation, sqlSheetName);
				try
				{
					merchantData.absolute(1);
					testConfig.putRunTimeProperty("merchantId", merchantData.getString("payumoneyuserid"));
				}
				catch(NullPointerException | SQLException e)
				{
					testConfig.logWarning("Entry not found in UserManagement Table for this merchant");
				}
			}
			else
			{
				System.out.println("Value of testConfig.merchantId is NULL");
			}
			
			//Putting data in runtime for Transaction table in PayU
			try
			{
				ResultSet paymentData = null;
				try
				{
					paymentData = DataBase.executeSelectQuery(testConfig, 5, DatabaseType.MoneyAuthDB, sqlSheetName);
				}
				catch(NullPointerException e)
				{
					testConfig.logWarning("PaymentId not found in RunTime Variables");
				}
				
				if(paymentData != null)
				{
					paymentData.absolute(1);
					String paymentTransactionId = paymentData.getString("paymentTransactionIds");
					if(paymentTransactionId.contains(","))
						paymentTransactionId = paymentTransactionId.substring(paymentTransactionId.lastIndexOf(",")+1);
					testConfig.putRunTimeProperty("txnid", paymentTransactionId);
				}
			}
			catch(SQLException e)
			{
				testConfig.logWarning("PaymentId not found in RunTime Variables");
			}
			catch(NullPointerException e)
			{
				System.out.println("Payment not reached till PayU Processing page...");
			}
		}
		else
		{
			sqlSheetName = "SQLForProduct";
		}
	  	
	  //Execute Queries one by one and results are concatenated in string
	  for(Object a:hmp.entrySet())
	    { 
		  Map.Entry<Integer, Integer> m = (Map.Entry<Integer, Integer>) a;
		  
	      try 
	      {
	           //Execute query     	  
	    	  	ResultSet rs = DataBase.executeSelectQuery(testConfig,(int)(m.getKey()),RecordDBData.getDBType(m.getValue()), sqlSheetName);
	    	  	ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
	       
	            //Getting Column's Name 
	    	  	htmlData.append("<table id = \"t01\"><caption style=\"text-align:left\"> <b>" +
	            rsmd.getTableName(1) +
	    	  			" Table</b>  </caption><tr>");	       
	             for(int i=1 ; i<= rsmd.getColumnCount() ; i++)
	             {
	            	 	 htmlData.append("<th>" + rsmd.getColumnName(i) + "</th>");
	             }
	              htmlData.append("\n" + "</tr>");
	
	             //Getting  column's value
	             if(rs.next())
	             {
	            	 do{
	            		 htmlData.append("<tr>");          
	            		 for(int i=1 ; i<= rsmd.getColumnCount(); i++)
	            		 {
	            			 htmlData.append("<td>" + rs.getString(i) + "</td>"); 
	            		 }
	            		 htmlData.append("\n" + "</tr>");              
	            	 	}while(rs.next());
	             }
	             else 
	             {
	            	 htmlData.append("<tr><td colspan=5> <b> No Data Found </b></td><td></td><td></td><td></td><td></td><td></td></tr>");
	             }
	            htmlData.append("</table><br><br>");
	             
	          	}
	      catch (SQLException ex) 
	      {
	          	System.out.println(ex);
	      }
	      catch (NullPointerException ex) 
	      {
	    	  TestDataReader sqlData = testConfig.getCachedTestDataReaderObject(sqlSheetName);
	    	  String selectQuery = sqlData.GetData((int)(m.getKey()), "Query");
	    	  htmlData.append("<P style=\"BACKGROUND-COLOR: Silver; PADDING-LEFT: 2em; PADDING-RIGHT: 2em\" align=leftr><FONT size=4><FONT color=#FF0000><STRONG><BR>Value of RunTime variable/s missing for this Query :    <b>" + selectQuery+"</STRONG></FONT></FONT> <BR><BR></p>" );
	      }
	    }
	  	htmlData.append("</body></html>");
	
	    //Write Data of  htmlData String in File
	  	try 
	  	{
	    	  File file = new File(filePath);
	    	  file.getParentFile().mkdirs();
	    	  	
	    	  BufferedWriter bf = new BufferedWriter(new FileWriter(file));
	    	  bf.append(htmlData);
	    	  bf.close();
	  	}
	  	catch(IOException e)
	  	{
	    	  e.printStackTrace();
	  	} 
	  	
	  	testConfig.logComment("<B>DB Data URL</B>:- <a href=" + Browser.getResultsURLOnRunTime(testConfig, filePath) + " target='_blank' >" + "Click Here - to View DataBase entries at the time of Failure" + "</a>");
	}
		
	
	/** Convert from integer values to corresponding DatabaseType enum Constant
	 * 
	 * @param 
	 * @return DatabaseType enum Constant
	 */
	public static DatabaseType getDBType(int i)
	{
		DatabaseType databaseType;
		
		switch(i)
		{
			case 1 : databaseType=DatabaseType.MoneyAsyncDB;
					break;
			
			case 2 : databaseType=DatabaseType.MoneyAuthDB;
					break;
					
			case 3 : databaseType=DatabaseType.MoneyCmsDB;
					break;
					
			case 4 : databaseType=DatabaseType.MoneyVaultDB;
					break;
			
			case 5 : databaseType=DatabaseType.MoneyWebServiceDB;
					break;
			
			case 6 : databaseType=DatabaseType.Offline;
					break;
			
			case 7 : databaseType=DatabaseType.Online;
					break;
			
			case 8 : databaseType=DatabaseType.PayuMoneyAutomation;
					break;
			
			case 9 : databaseType=DatabaseType.PerformanceDB;
					break;
			
			case 10 : databaseType=DatabaseType.AccuracyDB;
					break;
			
			case 11 : databaseType=DatabaseType.OfflineCrypto;
					break;
			
			case 12 : databaseType=DatabaseType.AutomationDB;
					break;
			default : databaseType=null;
		}
		return databaseType;
	}
	
	/** provides list of SQL queries 
	 * 
	 * @param testConfig
	 * @return HashMap of sqlRownNum and corresponding value of DatabaseType enum constant 
	 */
	public static HashMap<Integer,Integer> getListOfSQLQuery(Config testConfig)
	{
		HashMap<Integer,Integer> hashMap=new HashMap<Integer,Integer>();
		String projectName = testConfig.getRunTimeProperty("ProjectName");
		switch(projectName)
		{
			case "money" :  //Money's Project SQL  query
				hashMap.put(1, DatabaseType.MoneyAuthDB.values);  
				hashMap.put(2, DatabaseType.MoneyAuthDB.values);  
				hashMap.put(3, DatabaseType.MoneyVaultDB.values);
	         	hashMap.put(4, DatabaseType.Offline.values);
	         	hashMap.put(5, DatabaseType.MoneyAuthDB.values);  
		        hashMap.put(6, DatabaseType.MoneyAuthDB.values);  
		       	hashMap.put(7, DatabaseType.MoneyAuthDB.values);  
		       	hashMap.put(8, DatabaseType.MoneyAuthDB.values);  
	         	hashMap.put(9, DatabaseType.MoneyAuthDB.values); 
	         	hashMap.put(10, DatabaseType.MoneyAuthDB.values); 
	         	hashMap.put(11, DatabaseType.MoneyVaultDB.values);
	         	hashMap.put(12, DatabaseType.MoneyAuthDB.values); 
	         	hashMap.put(13, DatabaseType.MoneyAuthDB.values); 
		       	break;
			
			case "product" : //Add SQL Query for Product's Project 
				hashMap.put(1, DatabaseType.Offline.values);
				break;
			
			default :
				break;
		}
		return hashMap;
	}
}