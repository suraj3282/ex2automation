package Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import Utils.Config.Project;
import Utils.DataBase.DatabaseType;

public class User
{
	public String billingAddress;
	public String billingCity;
	public String billingPincode;
	public String billingState;
	
	public String city;
	public String confirmPassword;
	public String displayName;
	
	public String environment;
	public String id;
	public String key;
	
	public String markFreeSqlRow;
	public String mobile;
	public String newPassword;
	public String password;
	public String payUMoneyUserId;
	public String pinCode;
	
	public String shippingAddress;
	public String shippingName;
	public String state;
	
	public String userName;
	public String rememberMeCustomer;
	public String rememberMeMerchant;
	public String rememberMeAdmin;
	public String rememberMeAggregator;
	public String Authorization;
	public String customerType;
	
	public static List<String> merchantList = new ArrayList<String>();
	public static List<String> customerList = new ArrayList<String>();
	
	public User()
	{
		
	}
	
	public User(Map<String, String> userRow)
	{
		for (String column : userRow.keySet())
		{
			switch (column)
			{
				case "billingAddress":
					billingAddress = userRow.get(column);
					break;
				case "city":
					city = userRow.get(column);
					break;
				case "environment":
					environment = userRow.get(column);
					break;
				case "id":
					id = userRow.get(column);
					break;
				case "key":
					key = userRow.get(column);
					break;
				case "displayName":
					displayName = userRow.get(column);
					break;
				case "mobile":
					mobile = userRow.get(column);
					break;
				case "newPassword":
					newPassword = userRow.get(column);
					break;
				case "confirmPassword":
					confirmPassword = userRow.get(column);
					break;
				case "password":
					password = userRow.get(column);
					break;
				case "payumoneyuserid":
					payUMoneyUserId = userRow.get(column);
					break;
				case "pinCode":
					pinCode = userRow.get(column);
					break;
				case "shippingAddress":
					shippingAddress = userRow.get(column);
					break;
				case "shippingName":
					shippingName = userRow.get(column);
					break;
				case "state":
					state = userRow.get(column);
					break;
				case "userName":
					userName = userRow.get(column);
					break;
				case "rememberMeCustomer":
					rememberMeCustomer = userRow.get(column);
					break;
				case "rememberMeMerchant":
					rememberMeMerchant = userRow.get(column);
					break;
				case "rememberMeAdmin":
					rememberMeAdmin = userRow.get(column);
					break;
				case "rememberMeAggregator":
					rememberMeAggregator = userRow.get(column);
					break;
				case "Authorization":
					Authorization = userRow.get(column);
					break;
				case "customerType":
					customerType = userRow.get(column);
			}
		}
	}
	
	/**
	 * Marks the specified admin as free in PayUMoneyAutomation db
	 * 
	 * @param id
	 */
	public void markAdminFree(Config testConfig, String adminId)
	{
		markUserFree(testConfig, adminId, 270);
	}
	
	/**
	 * Marks the specified Aggregator as free in PayUMoneyAutomation db
	 * 
	 * @param id
	 */
	public void markAggregatorFree(Config testConfig, String aggregatorId)
	{
		markUserFree(testConfig, aggregatorId, 274);
	}
	
	/**
	 * Marks the specified customer as free in PayUMoneyAutomation db
	 * 
	 * @param id
	 */
	public void markCustomerFree(Config testConfig, String customerId)
	{
		markUserFree(testConfig, customerId, 262);
		removeFromCustomerList(customerId);
	}
	
	/**
	 * Marks the specified merchant as free in PayUMoneyAutomation db
	 * 
	 * @param id
	 */
	public void markMerchantFree(Config testConfig, String merchantId)
	{
		markUserFree(testConfig, merchantId, 266);
		removeFromMerchantList(merchantId);
	}
	
	/**
	 * Marks the specified user as free in PayUMoneyAutomation db
	 * 
	 * @param id
	 */
	private void markUserFree(Config testConfig, String userID, int markFreeSqlRow)
	{
		// Set the TestData as MoneyTestData.xls
		testConfig.update_TestDataSheet_value(Project.Money);
		testConfig.putRunTimeProperty("id", userID);
		testConfig.putRunTimeProperty("testcasename", testConfig.getTestName());
		int rowsAffected = DataBase.executeUpdateQuery(testConfig, markFreeSqlRow, DatabaseType.PayuMoneyAutomation);
		Helper.compareTrue(testConfig, "User marked as Free", 1 == rowsAffected);
	}
	
	/**
	 * This function maintains the list of already assigned merchants and also return true if user is already in list else return false
	 * @param id
	 * @return
	 */
	public static synchronized boolean presentInMerchantList(String id)
	{
		if(merchantList.contains(id))
		{
			return true;
		}
		
		try
		{
			merchantList.add(id);
		}
		catch(Exception e)
		{
			System.out.println("Exception appeared while adding Merchant to the List : " + id);
			e.printStackTrace();
		}
		System.out.println("Current merchant list : "+ merchantList);
		return false;
	}
	
	public static void removeFromMerchantList(String id)
	{
		if(merchantList.contains(id))
		{
			try
			{
				merchantList.remove(id);
			}
			catch(Exception e)
			{
				System.out.println("Exception appeared while removing Merchant from the List : " + id);
				e.printStackTrace();
			}
			System.out.println("Remaining merchant list : "+ merchantList);
		}
		else
		{
			//System.out.println("Unable to remove id:"+id+" from merchant list : "+ merchantList);
		}
	}
	
	/**
	 * This function maintains the list of already assigned customers and also return true if user is already in list else return false
	 * @param id
	 * @return
	 */
	public static synchronized boolean presentInCustomerList(String id)
	{
		if(customerList.contains(id))
		{
			return true;
		}
		
		try
		{
			customerList.add(id);
		}
		catch(Exception e)
		{
			System.out.println("Exception appeared while adding Customer to the List : " + id);
			e.printStackTrace();
		}
		
		System.out.println("Current customer list : "+ customerList);
		return false;
	}
	
	public static void removeFromCustomerList(String id)
	{
		if(customerList.contains(id))
		{
			try
			{
				customerList.remove(id);
			}
			catch(Exception e)
			{
				System.out.println("Exception appeared while removing Customer from the List : " + id);
				e.printStackTrace();
			}
			System.out.println("Remaining customer list : "+ customerList);
		}
		else
		{
			//System.out.println("Unable to remove id:"+id+" from customer list : "+ customerList);
		}
	}
}
