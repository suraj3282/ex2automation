package Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Stack;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolException;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lorecraft.phparser.SerializedPhpParser;
import org.testng.Assert;

import Utils.Config.Project;
import Utils.DataBase.DatabaseType;

/**
 * @author vidya.priyadarshini
 */
public class WebService
{
	/**
	 * Enum For Product Web Services
	 *
	 */
	public enum ProductWebServices {
	    // Define API and row Numbers here
		verify_payment(4), 
		check_payment(1),
		delete_user_card(25),
		get_settlement_details(31),
		get_user_cards(10),
		getAllRefundsFromTxnIds(32),
		getPaymentGateways(33),
		refundDateAmount(34),
		validateCardNumber(35),
		checkNewActiveMerchants(36),
		refundTransaction(3),
		refundStatus(37),
		updateMerUtr(38),
		offerSyncingFromPayuMoneyToPayu(39),
		getIssuingBankDownBins(40),
		caseWithPayuidAsChargeBack(41),
		getIssuingBankStatus(43),
		eligibleBinsForEMI(44),
		merchantLoginDetails(46),
		checkOfferDetails(47),
		check_action_status(48);

		
		private int row;
	    // Constructor
	    private ProductWebServices(final int row) {
	        this.row = row;
	    }
	 
	    //return enum value
	    public int getRowNumber() {
	        return row;
	    };
	}
	
	
	public static JSONObject Execute(Config testConfig, TestDataReader data, int webServiceRow)
	{
		JSONObject jObject = null;
		URL url;
		HttpURLConnection connection;
		String type = data.GetData(webServiceRow, "Type").trim().toLowerCase();
		String comments = data.GetData(webServiceRow, "Comments").trim();
		String method = data.GetData(webServiceRow, "Method").trim();
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		String parameters = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Parameters").trim());
		String Url = null;
		
		Url = baseUrl + command + parameters;
		
		try
		{
			url = new URL(Url);
			testConfig.logComment("Executing '" + comments + "' on '" + Url + "'");
			connection = (HttpURLConnection) url.openConnection();
			connection.setDoOutput(true);
			// connection.setDoInput(true);
			connection.setInstanceFollowRedirects(false);
			connection.setRequestMethod(method);
			// connection.setUseCaches (false);
			int httpResponseCode = connection.getResponseCode();
			String line;
			String response = "";
			if ((httpResponseCode == 200 || httpResponseCode == 302) && !method.contentEquals("DELETE"))
			{
				InputStream responseStream = connection.getInputStream();
				BufferedReader rd = new BufferedReader(new InputStreamReader(responseStream));
				while ((line = rd.readLine()) != null)
				{
					response += line;
				}
				try
				{
					jObject = new JSONObject(response);
					if (null != jObject)
					{
						return jObject;
					}
				}
				catch (JSONException e)
				{
					testConfig.logException(e);
				}
			}
			else
				if (httpResponseCode == 504)
				{
					Browser.wait(testConfig, 90);
					try
					{
						jObject = new JSONObject("{status: 504}");
					}
					catch (JSONException e)
					{
						testConfig.logException(e);
					}
				}
				else
					if (httpResponseCode == 505)
					{
						Browser.wait(testConfig, 30);
						try
						{
							jObject = new JSONObject("{status: 505}");
						}
						catch (JSONException e)
						{
							testConfig.logException(e);
						}
					}
					else
						if (!method.contentEquals("DELETE"))
						{
							try
							{
								// get error stream in case of error
								BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
								while ((line = rd.readLine()) != null)
								{
									response += line;
								}
								String message = StringUtils.substringBetween(response.toString().trim(), "<h1>", "</h1>");
								String description = StringUtils.substringBetween(response.toString().trim(), "description</b> <u>", "</u>");
								String exception = message + "\n\n" + description;
								Assert.fail(exception);
							}
							catch (Exception e)
							{
								testConfig.logException(e);
							}
						}
			connection.disconnect();
		}
		catch (MalformedURLException e)
		{
			testConfig.logException(e);
		}
		catch (IOException e)
		{
			testConfig.logException(e);
		}
		testConfig.endExecutionOnfailure = false;
		return jObject;
		
	}
	
	public static JSONObject ExecutePaisaAuthGet(Config testConfig, TestDataReader data, int webServiceRow, String referrer, String authorization, ArrayList<NameValuePair> parameters)
	{
		
		DefaultHttpClient client = new DefaultHttpClient();
		JSONObject result = null;
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		URIBuilder urlBuilder = new URIBuilder().setScheme("http").setHost(baseUrl.split("//")[1]).setPath(command);
		for (NameValuePair i : parameters)
		{
			String name = i.getName();
			String value = i.getValue();
			urlBuilder.setParameter(name, value);
		}
		
		URI url = null;
		try
		{
			url = urlBuilder.build();
		}
		catch (URISyntaxException e1)
		{
			e1.printStackTrace();
		}
		HttpGet httpget = new HttpGet(url);
		httpget.addHeader("Accept", "application/json");
		httpget.addHeader("Accept-Encoding", "gzip, deflate");
		httpget.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0");
		// httpget.addHeader("X-Requested-With", "XMLHttpRequest");
		httpget.addHeader("Referer", referrer);
		httpget.addHeader("Host", testConfig.getRunTimeProperty("Host"));
		httpget.addHeader("authorization", authorization);
		/*
		 * GetMethod getM = new GetMethod(); getM.setURI(url);
		 * getM.setRequestHeader("Authorization", authorization);
		 */
		// HttpContext context = new BasicHttpContext();
		try
		{
			HttpResponse response = client.execute(httpget);
			StatusLine httpResponseCode = response.getStatusLine();
			int responseCode = httpResponseCode.getStatusCode();
			String line;
			String responseText = "";
			if (responseCode == 200)
			{
				InputStream responseStream = response.getEntity().getContent();
				BufferedReader rd = new BufferedReader(new InputStreamReader(responseStream));
				while ((line = rd.readLine()) != null)
				{
					responseText += line;
				}
			}
			result = new JSONObject(responseText);
		}
		catch (ClientProtocolException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public static DefaultHttpClient ExecutePaisaAuthLogin(Config testConfig, TestDataReader data, int webServiceRow, boolean redirectURL)
	{
		
		JSONObject jsonobject = null;
		testConfig.endExecutionOnfailure = true;
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		String Url = null;
		
		Url = baseUrl + command;
		final DefaultHttpClient client = new DefaultHttpClient();
		client.setRedirectStrategy(new DefaultRedirectStrategy()
		{
			@Override
			public boolean isRedirected(HttpRequest request, HttpResponse response, HttpContext context)
			{
				boolean isRedirect = false;
				try
				{
					isRedirect = super.isRedirected(request, response, context);
				}
				catch (ProtocolException e)
				{
					e.printStackTrace();
				}
				if (!isRedirect)
				{
					int responseCode = response.getStatusLine().getStatusCode();
					if (responseCode == 301 || responseCode == 302)
					{
						return true;
					}
				}
				return false;
			}
		});
		try
		{
			HttpPost httpPost = new HttpPost(Url);
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			System.out.println("\nExecuting request : " + httpPost.getURI());
			// add parameters
			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("username", testConfig.getRunTimeProperty("Username")));
			postParameters.add(new BasicNameValuePair("password", testConfig.getRunTimeProperty("Password")));
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			httpPost.setEntity(formEntity);
			// Create a response handler
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = client.execute(httpPost, responseHandler);
			List<Cookie> cookies = client.getCookieStore().getCookies();
			// maintain cookie to get access token
			client.setCookieStore(client.getCookieStore());
			if (cookies.isEmpty())
			{
				System.out.println("None");
			}
			else
			{
				for (int i = 0; i < cookies.size(); i++)
				{
					if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
						System.out.println("- " + cookies.get(i).toString());
					testConfig.putRunTimeProperty("cookie", cookies.get(i).toString());
				}
			}
			if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
				System.out.println("Post response : " + responseBody);
			
			String url = testConfig.getRunTimeProperty("authredirectURL");
			HttpGet httpget = new HttpGet(url);
			httpget.addHeader("Accept", "application/json");
			httpget.addHeader("Accept-Encoding", "gzip, deflate");
			httpget.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0");
			httpget.addHeader("X-Requested-With", "XMLHttpRequest");
			httpget.addHeader("Referer", "https://www.payumoney.com/");
			httpget.addHeader("Host", "www.payumoney.com");
			HttpContext context = new BasicHttpContext();
			// HttpResponse response = client.execute(httpget, context);
			HttpResponse response = client.execute(httpget);
			
			/*
			 * System.out.println("redirect url is" +
			 * response.getFirstHeader("Location").getValue()); String
			 * redirectUrl = response.getFirstHeader("Location").getValue();
			 */
			
			if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
				System.out.println("\nSending 'GET' request to URL : " + url);
			
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			
			String result = rd.readLine();
			String accessToken = result.split(",")[0].split(":")[1];
			testConfig.putRunTimeProperty("accessToken", accessToken.split("\"")[1].split("\"")[0]);
			// System.out.println("GET call response is " + result.toString());
			
			// String accessToken = redirectUrl.split("=")[1].split("&")[0];
			
			// testConfig.putRunTimeProperty("accessToken", accessToken);
			
			jsonobject = new JSONObject(responseBody);
			if (null != jsonobject)
			{
				try
				{
					if ((Integer) jsonobject.get("status") == 0)
					{
						testConfig.putRunTimeProperty("userId", jsonobject.getJSONObject("result").get("adminMerchantId").toString().trim());
						
					}
					else
					{
						testConfig.logFail("Error : " + jsonobject.get("message").toString());
					}
				}
				catch (JSONException e)
				{
					testConfig.logException(e);
				}
			}
		}
		catch (HttpException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			testConfig.logException(e);
		}
		
		testConfig.endExecutionOnfailure = false;
		client.getConnectionManager().shutdown();
		return client;
		
	}
	
	public static JSONObject ExecutePaisaAuthLoginReleasePayment(Config testConfig, TestDataReader data, int webServiceRow, String paymentStatus)
	{
		JSONObject jsonobject = null;
		testConfig.endExecutionOnfailure = true;
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		String Url = null;
		
		Url = baseUrl + command;
		final DefaultHttpClient client = new DefaultHttpClient();
		client.setRedirectStrategy(new DefaultRedirectStrategy()
		{
			@Override
			public boolean isRedirected(HttpRequest request, HttpResponse response, HttpContext context)
			{
				boolean isRedirect = false;
				try
				{
					isRedirect = super.isRedirected(request, response, context);
				}
				catch (ProtocolException e)
				{
					e.printStackTrace();
				}
				if (!isRedirect)
				{
					int responseCode = response.getStatusLine().getStatusCode();
					if (responseCode == 301 || responseCode == 302)
					{
						return true;
					}
				}
				return false;
			}
		});
		try
		{
			HttpPost httpPost = new HttpPost(Url);
			httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			System.out.println("\nExecuting request : " + httpPost.getURI());
			// add parameters
			ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("username", testConfig.getRunTimeProperty("Username")));
			postParameters.add(new BasicNameValuePair("password", testConfig.getRunTimeProperty("Password")));
			UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
			httpPost.setEntity(formEntity);
			// Create a response handler
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = client.execute(httpPost, responseHandler);
			List<Cookie> cookies = client.getCookieStore().getCookies();
			// maintain cookie to get access token
			client.setCookieStore(client.getCookieStore());
			if (cookies.isEmpty())
			{
				System.out.println("None");
			}
			else
			{
				for (int i = 0; i < cookies.size(); i++)
				{
					System.out.println("- " + cookies.get(i).toString());
					testConfig.putRunTimeProperty("cookie", cookies.get(i).toString());
				}
			}
			if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
				System.out.println("Post response : " + responseBody);
			
			String url = testConfig.getRunTimeProperty("authredirectURL");
			HttpGet httpget = new HttpGet(url);
			HttpContext context = new BasicHttpContext();
			HttpResponse response = client.execute(httpget, context);
			System.out.println("redirect url is" + response.getFirstHeader("Location").getValue());
			String redirectUrl = response.getFirstHeader("Location").getValue();
			String accessToken = redirectUrl.split("=")[1].split("&")[0];
			testConfig.putRunTimeProperty("accessToken", accessToken);
			DefaultHttpClient client1 = new DefaultHttpClient();
			// maintain cookie to initiate payment
			client1.setCookieStore(client.getCookieStore());
			JSONObject TransObj = null;
			JSONObject resultObj = null;
			String sourceAmountMap = "{PAYU:45.0}";
			postParameters = new ArrayList<NameValuePair>();
			postParameters.add(new BasicNameValuePair("sourceAmountMap", sourceAmountMap));
			
			HashMap<String, String> postHeaders = new HashMap<String, String>();
			postHeaders.put("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
			postHeaders.put("Authorization", "Bearer " + testConfig.getRunTimeProperty("accessToken"));
			webServiceRow = 33;
			baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
			command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
			
			if (postParameters != null)
				Url = baseUrl + command;
			try
			{
				httpPost = new HttpPost(Url);
				// set request header
				for (Entry<String, String> entry : postHeaders.entrySet())
				{
					String key = entry.getKey();
					String value = entry.getValue();
					httpPost.addHeader(key, value);
				}
				System.out.println("\nExecuting request : " + httpPost.getURI());
				if (postParameters != null)
				{
					formEntity = new UrlEncodedFormEntity(postParameters);
					httpPost.setEntity(formEntity);
				}
				// Create a response handler
				responseHandler = new BasicResponseHandler();
				responseBody = client1.execute(httpPost, responseHandler);
				System.out.println("Initiate payment response is ---> " + responseBody);
				
				TransObj = new JSONObject(responseBody);
				// verify initiated message
				Helper.compareExcelEquals(testConfig, "initiated message", "Payment Initiated", TransObj.getString("message"));
				// verify status is initiated
				Helper.compareEquals(testConfig, "transaction status", "Initiated", TransObj.getJSONObject("result").getJSONObject("payment").getString("status"));
				String sourceReferenceId = TransObj.getJSONObject("result").getJSONObject("sourceMap").getJSONObject("PAYU").getJSONObject("paymentTransaction").getString("sourceReferenceId");
				testConfig.putRunTimeProperty("sourceReferenceId", sourceReferenceId);
				String paymentTransactionId = TransObj.getJSONObject("result").getJSONObject("sourceMap").getJSONObject("PAYU").getJSONObject("paymentTransaction").getString("paymentTransactionId");
				testConfig.putRunTimeProperty("paymentTransactionId", paymentTransactionId);
				String Txnid = TransObj.getJSONObject("result").getJSONObject("payment").getString("paymentTransactionIds");
				testConfig.putRunTimeProperty("Txnid", Txnid);
				// update transaction status to success
				webServiceRow = 35;
				DefaultHttpClient client3 = new DefaultHttpClient();
				// maintain cookie for update transaction
				client3.setCookieStore(client1.getCookieStore());
				baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
				command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
				
				Url = baseUrl + command;
				httpPost = new HttpPost(Url);
				// set request header
				for (Entry<String, String> entry : postHeaders.entrySet())
				{
					String key = entry.getKey();
					String value = entry.getValue();
					httpPost.addHeader(key, value);
				}
				// add parameters for update transaction
				// query database to get id of initiated transaction
				testConfig.putRunTimeProperty("merchantId", "5840");
				Map<String, String> map = DataBase.executeSelectQuery(testConfig, 1, 1, DatabaseType.Online);
				if (map != null)
				{
					String payUId = map.get("id");
					testConfig.putRunTimeProperty("payUId", payUId);
				}
				
				postParameters = new ArrayList<NameValuePair>();
				postParameters.add(new BasicNameValuePair("status", paymentStatus));
				postParameters.add(new BasicNameValuePair("mode", "CC"));
				postParameters.add(new BasicNameValuePair("amount", "45.0"));
				postParameters.add(new BasicNameValuePair("txnid", testConfig.getRunTimeProperty("paymentTransactionId")));
				postParameters.add(new BasicNameValuePair("bank_ref_no", "refno"));
				postParameters.add(new BasicNameValuePair("mihpayid", testConfig.getRunTimeProperty("payUId")));
				
				formEntity = new UrlEncodedFormEntity(postParameters);
				httpPost.setEntity(formEntity);
				System.out.println("\nExecuting request : " + httpPost.getURI());
				// Create a response handler
				responseHandler = new BasicResponseHandler();
				responseBody = client3.execute(httpPost, responseHandler);
				System.out.println("update transaction response is ---> " + responseBody);
				TransObj = new JSONObject(responseBody);
				// verify update transaction message
				Helper.compareExcelEquals(testConfig, "payment updated message", "Payment Status Updated", TransObj.getString("message"));
				// verify status is set to paymentStatus
				Helper.compareEquals(testConfig, "transaction status", paymentStatus, TransObj.getJSONObject("result").getString("status"));
				
				// release payment
				webServiceRow = 34;
				DefaultHttpClient client2 = new DefaultHttpClient();
				// maintain cookie to release payment
				client2.setCookieStore(client1.getCookieStore());
				baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
				command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
				
				Url = baseUrl + command;
				httpPost = new HttpPost(Url);
				// set request header
				for (Entry<String, String> entry : postHeaders.entrySet())
				{
					String key = entry.getKey();
					String value = entry.getValue();
					httpPost.addHeader(key, value);
				}
				System.out.println("\nExecuting request : " + httpPost.getURI());
				// Create a response handler
				responseHandler = new BasicResponseHandler();
				responseBody = client2.execute(httpPost, responseHandler);
				System.out.println("Release Payment response is ---> " + responseBody);
				resultObj = new JSONObject(responseBody);
				// verify release transaction message
				Helper.compareExcelEquals(testConfig, "payment release message", "Result Is", resultObj.getString("message"));
				// verify status is release payment
				Helper.compareEquals(testConfig, "transaction status", "Release Payment", resultObj.getJSONObject("result").getString("status"));
				
				if (null != resultObj)
				{
					return resultObj;
				}
				
			}
			catch (HttpException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (JSONException e)
			{
				e.printStackTrace();
			}
			
		}
		catch (ClientProtocolException e1)
		{
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			e1.printStackTrace();
		}
		finally
		{
			testConfig.endExecutionOnfailure = false;
			client.getConnectionManager().shutdown();
		}
		return jsonobject;
	}
	
	public static JSONObject ExecutePaisaAuthPOSTWebServices(Config testConfig, TestDataReader data, int webServiceRow, ArrayList<NameValuePair> postParameters, HashMap<String, String> postHeaders)
	{
		JSONObject jObject = null;
		testConfig.endExecutionOnfailure = true;
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		String parameters = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Parameters").trim());
		String Url = null;
		if (postParameters != null)
			Url = baseUrl + command;
		else
			Url = baseUrl + command + parameters;
		final DefaultHttpClient client = new DefaultHttpClient();
		
		try
		{
			HttpPost httpPost = new HttpPost(Url);
			// set request header
			for (Entry<String, String> entry : postHeaders.entrySet())
			{
				String key = entry.getKey();
				String value = entry.getValue();
				httpPost.addHeader(key, value);
			}
			//System.out.println("\nExecuting request : " + httpPost.getURI());
			testConfig.logComment("Executing request : " + httpPost.getURI());
			
			
			if (postParameters != null)
			{
				UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
				httpPost.setEntity(formEntity);
			}			
			
			String params = "?";
			for (NameValuePair entry : postParameters){		    
				params += entry.getName() + "=" + entry.getValue() + "&";	
			}
			
			testConfig.logComment("Parameters : " + params.substring(0, params.length()-1));			
			testConfig.logComment("Header: " + httpPost.getFirstHeader("Authorization"));
			
			// Create a response handler
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = client.execute(httpPost, responseHandler);
			
			if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
				System.out.println(responseBody);
			
			jObject = new JSONObject(responseBody);
			client.getConnectionManager().shutdown();
			if (null != jObject)
			{
				testConfig.endExecutionOnfailure = false;
				return jObject;
			}
		}
		catch (HttpException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			testConfig.logException(e);
		}
		
		testConfig.endExecutionOnfailure = false;
		return jObject;
		
	}
	
	public static JSONObject ExecutePaisaAuthPOSTWebServices(Config testConfig, TestDataReader data, int webServiceRow, ArrayList<NameValuePair> postParameters, HashMap<String, String> postHeaders, DefaultHttpClient loginClient)
	{
		JSONObject jObject = null;
		testConfig.endExecutionOnfailure = true;
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		String parameters = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Parameters").trim());
		
		String Url = null;
		if (postParameters != null)
			Url = baseUrl + command;
		else
			Url = baseUrl + command + parameters;
		final DefaultHttpClient client = new DefaultHttpClient();
		
		try
		{
			HttpPost httpPost = new HttpPost(Url);
			// set request header
			for (Entry<String, String> entry : postHeaders.entrySet())
			{
				String key = entry.getKey();
				String value = entry.getValue();
				httpPost.addHeader(key, value);
			}
			// maintain cookie from loginclient
			client.setCookieStore(loginClient.getCookieStore());
			// httpPost.addHeader("Content-Type",
			// "application/x-www-form-urlencoded;charset=UTF-8");
			System.out.println("\nExecuting request : " + httpPost.getURI());
			if (postParameters != null)
			{
				UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
				httpPost.setEntity(formEntity);
			}
			// Create a response handler
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = client.execute(httpPost, responseHandler);
			
			if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
				System.out.println(responseBody);
			
			jObject = new JSONObject(responseBody);
			client.getConnectionManager().shutdown();
			if (null != jObject)
			{
				return jObject;
			}
		}
		catch (HttpException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			testConfig.logException(e);
		}
		
		testConfig.endExecutionOnfailure = false;
		return jObject;
		
	}
	
	
	public static JSONObject ExecutePaisaAuthPOSTWebServices(Config testConfig, TestDataReader data, int webServiceRow, HashMap<String, String> apiPostParameters, HashMap<String, String> postHeaders)
	{
		JSONObject jObject = null;
		
		ArrayList<NameValuePair> postParameters = new ArrayList();		
		for (Entry<String, String> entry : apiPostParameters.entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();
			postParameters.add(new BasicNameValuePair(key, value));
		}
		
		jObject = ExecutePaisaAuthPOSTWebServices(testConfig, data, webServiceRow, postParameters, postHeaders);
		return jObject;		
	}
	
	public static JSONObject ExecutePaisaAuthGetWebServicesWithoutAuthorization(Config testConfig, TestDataReader data, int webServiceRow, HashMap<String, String> apiPostParameters, HashMap<String, String> postHeaders)
	{
		JSONObject jObject = null;
		
		ArrayList<NameValuePair> postParameters = new ArrayList();		
		for (Entry<String, String> entry : apiPostParameters.entrySet())
		{
			String key = entry.getKey();
			String value = entry.getValue();
			postParameters.add(new BasicNameValuePair(key, value));
		}
		String authorization = null;
		jObject = ExecutePaisaAuthGet(testConfig, data, webServiceRow, null, authorization, postParameters);
		return jObject;		
	}
	
	public static PaisaPOSTAPIReturnValues<JSONObject, DefaultHttpClient> ExecutePaisaAuthPOSTWebServicesRet2Values(Config testConfig, TestDataReader data, int webServiceRow, ArrayList<NameValuePair> postParameters, HashMap<String, String> postHeaders)
	{
		JSONObject jObject = null;
		testConfig.endExecutionOnfailure = true;
		String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "BaseUrl").trim());
		String command = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Command").trim());
		String parameters = Helper.replaceArgumentsWithRunTimeProperties(testConfig, data.GetData(webServiceRow, "Parameters").trim());
		
		String Url = null;
		if (postParameters != null)
			Url = baseUrl + command;
		else
			Url = baseUrl + command + parameters;
		final DefaultHttpClient client = new DefaultHttpClient();
		
		try
		{
			HttpPost httpPost = new HttpPost(Url);
			// set request header
			for (Entry<String, String> entry : postHeaders.entrySet())
			{
				String key = entry.getKey();
				String value = entry.getValue();
				httpPost.addHeader(key, value);
			}
			// httpPost.addHeader("Content-Type",
			// "application/x-www-form-urlencoded;charset=UTF-8");
			System.out.println("\nExecuting request : " + httpPost.getURI());
			if (postParameters != null)
			{
				UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(postParameters);
				httpPost.setEntity(formEntity);
			}
			// Create a response handler
			ResponseHandler<String> responseHandler = new BasicResponseHandler();
			String responseBody = client.execute(httpPost, responseHandler);
			
			if (testConfig.getObjectRunTimeProperty("DebugMode").equals("true"))
				System.out.println(responseBody);
			
			jObject = new JSONObject(responseBody);
			client.getConnectionManager().shutdown();
			if (null != jObject)
			{
				return new PaisaPOSTAPIReturnValues<JSONObject, DefaultHttpClient>(jObject, client);
			}
		}
		catch (HttpException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (JSONException e)
		{
			testConfig.logException(e);
		}
		
		testConfig.endExecutionOnfailure = false;
		return new PaisaPOSTAPIReturnValues<JSONObject, DefaultHttpClient>(jObject, client);
		
	}
	
	/*
	 * public static JSONObject ExecutePaisaWebService(Config testConfig,
	 * TestDataReader data, int webServiceRow) { String comments =
	 * data.GetData(webServiceRow, "Comments").trim().toLowerCase(); String
	 * method = data.GetData(webServiceRow, "Method").trim().toLowerCase();
	 * String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig,
	 * data.GetData(webServiceRow, "BaseUrl").trim()); String command =
	 * Helper.replaceArgumentsWithRunTimeProperties(testConfig,
	 * data.GetData(webServiceRow, "Command").trim()); String parameters =
	 * Helper.replaceArgumentsWithRunTimeProperties(testConfig,
	 * data.GetData(webServiceRow, "Parameters").trim()); String url = null; url
	 * = baseUrl + command + parameters; ClientResource resource = new
	 * ClientResource(url); Representation entity = null;
	 * testConfig.logComment("Executing '" + comments + "' on '" + url + "'");
	 * switch(method) { case "get": entity =
	 * resource.get(MediaType.APPLICATION_JSON); break; case "post": entity =
	 * resource.post(null,MediaType.APPLICATION_JSON); break; }
	 * JsonRepresentation represent = null; try { represent = new
	 * JsonRepresentation(entity); } catch (IOException e) {
	 * testConfig.logFail(e.toString()); } JSONObject jObject = null; try {
	 * jObject = represent.getJsonObject(); } catch (JSONException e1) {
	 * testConfig.logFail(e1.toString()); } return jObject; }
	 * TODO : This Function is not used any where as of now. We will delete this Code if we didn't find its usage by the end of our WenService Automation
	 */
	public static Object ExecuteProductWebService(Config testConfig, TestDataReader transactionData, int transactionRow, TestDataReader webServiceData, int webServiceRow)
	{
		Object returnOb = null;
		try
		{
			System.setProperty("https.protocols", "SSLv3");
			String comments = webServiceData.GetData(webServiceRow, "Comments");
			String method = webServiceData.GetData(webServiceRow, "Method").toLowerCase();
			String baseUrl = Helper.replaceArgumentsWithRunTimeProperties(testConfig, webServiceData.GetData(webServiceRow, "BaseUrl").trim());
			String command = webServiceData.GetData(webServiceRow, "Command");
			String parameters = Helper.replaceArgumentsWithRunTimeProperties(testConfig, webServiceData.GetData(webServiceRow, "Parameters").trim());
			
			// Set the base URL of request
			URL url = new URL(baseUrl);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			switch (method)
			{
				case "get":
					con.setRequestMethod("GET");
					break;
				case "post":
					con.setRequestMethod("POST");
					break;
			}
			con.setDoOutput(true);
			PrintStream ps = new PrintStream(con.getOutputStream());
			
			String key = transactionData.GetCurrentEnvironmentData(transactionRow, "key");
			String salt = transactionData.GetCurrentEnvironmentData(transactionRow, "salt");
			
			// Compute Hash
			String hashInput = key + "|" + command;
			String[] params = parameters.split("&");
			String varValue = "";
			for (String param : params)
			{
				varValue = param.split("=")[1];
				hashInput = hashInput + "|" + varValue;
			}
			hashInput = hashInput + "|" + salt;
			String hash = GetHash(testConfig, hashInput);
			
			String request = "hash=" + hash + "&key=" + key + "&command=" + command + "&" + parameters;
			ps.print(request);
			ps.close();
			
			// Execute the WebService
			testConfig.logComment("Executing '" + comments + "' web service - '" + baseUrl + "?" + request + "'");
			con.connect();
			if (con.getResponseCode() == HttpURLConnection.HTTP_OK)
			{
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String output = "";
				String line;
				while ((line = br.readLine()) != null)
				{
					output = output + line;
				}
				br.close();
				
				// convert PHP response to Java Object
				SerializedPhpParser parser = new SerializedPhpParser(output);
				returnOb = parser.parse();
			}
			con.disconnect();
		}
		catch (Exception e)
		{
			testConfig.logException(e);
		}
		
		return returnOb;
	}
	
	/**
	 * Will create a request Format for Web Service,Send the Request to Web
	 * Server and return response in JSON format
	 * 
	 * @param testConfig
	 * @param webServiceRow
	 *            WebService Row in WebService Sheet to know which API is going
	 *            to Execute
	 * @param key
	 *            Merchant's Key
	 * @param salt
	 *            Merchant Salt
	 * @return Will Return JSON Object of Response
	 */
	public static Object executePayuProductWebService(Config testConfig, int webServiceRow, String key, String salt)
	{
		String testDataSheetName = testConfig.getRunTimeProperty("TestDataSheet");
		testConfig.update_TestDataSheet_value(Project.Product);
		
		// Reading WebService Sheet Data
		String val = "";
		Object returnObj = null;
		//JSONArray jsonArray =null;
		TestDataReader webServiceData = testConfig.getCachedTestDataReaderObject("WebServices");
		String excelParameters = webServiceData.GetData(webServiceRow, "Parameters").trim();
			
		String response = "";
		int responseCode = 0;

		//Getting Url to send Request
		String baseUrl = testConfig.getRunTimeProperty("PayUWebServiceUrlForApi");
		
		//Getting name of API
		String command = webServiceData.GetData(webServiceRow, "Command");
		//Replace run Time Arguments With Actual Values
		 String parameters = Helper.replaceArgumentsWithRunTimeProperties(testConfig, excelParameters);
		//Getting Var1 to generate hash from Run Time Properties
		String var1 = excelParameters.substring(excelParameters.indexOf('$') + 1, excelParameters.indexOf('}'));
		if (testConfig.getRunTimeProperty(var1) != "")
			val = testConfig.getRunTimeProperty(var1);
		else
			testConfig.logComment("Value of var1: "+ var1 + " not found at runtime");
		
		// Creating String for Calculating Hash
		String postString = key + "|" + command + "|" + val + "|" + salt;
		
		// Calling Hash Generation function
		String hash = Helper.calculateHash(postString);
		
		// creating request to hit url
		String request = "key=" + key + "&command=" + command + "&hash=" + hash + "&" + parameters;
		
		try
		{
			URL url = new URL(baseUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write(request);
			wr.flush();
			responseCode = conn.getResponseCode();
			testConfig.logComment("Received response code: "+responseCode);
			
			// Get the response
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			
			String line;
			
			while ((line = rd.readLine()) != null)
			{
				response += line;
			}
			testConfig.logComment(response);
			wr.close();
			try
			{
				// Converting String In JSON object
				returnObj = new JSONObject(response);
			}
			catch (JSONException e)
			{
				try
				{
					returnObj=new JSONArray(response);
				}
				catch (JSONException e1)
				{
					returnObj=(Object)response;
					// TODO Auto-generated catch block
				}			
			}	
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
			testConfig.logFail("malformed URL has occurred. Either no legal protocol could be found in a specification string or the string could not be parsed.");
		}
		catch (IOException e)
		{
				testConfig.logFail("Not Able to create Connection for executing API");
		}
		
		testConfig.putRunTimeProperty("TestDataSheet", testDataSheetName);
		return returnObj;
		
	}
	
	/**
	 * Create A Map Of key Value Out Of JSONObject If there is JSONObject as a
	 * Value .it Will insert those JSONObject Values in Same Map
	 * 
	 * @param jobject
	 *            JSONObject
	 * @return Return a Map<String,String> with Entries Filled
	 * @throws JSONException
	 */	
	public static HashMap<String, Object> jsonParser(Object jobject)
	{
		if(jobject==null)

			return null;
		int count = 0;
		int flag = 0;
		HashMap<String, Object> jsonMap = new HashMap<String, Object>();
		Stack<Object> stack = new Stack<Object>();
		JSONObject obj1=null;
		try
		{
			// pushing the main Jason Object in the stack
			if(jobject.getClass().getName().equals("org.json.JSONArray"))
			{
				JSONArray jsonobj =(JSONArray)(jobject);
				obj1=jsonobj.getJSONObject(0);			
			}
			else if(jobject.getClass().getName().equals("org.json.JSONObject"))
			{
				obj1=(JSONObject)(jobject);
			}
			stack.push(obj1);		
			while (!stack.isEmpty())
			{
				if (flag == 1)
				{
					count++;
					flag = 0;
				}
				JSONObject obj =(JSONObject) stack.pop();
				// Getting Iterator Of Json Object Keys
				Iterator<Object> keys = obj.keys();
				while (keys.hasNext())
				{
					String key = (String) keys.next();
					// 	If key's Value is Json Object Itself ,if true than push it in
					// 	the Stack else Put it in the Map
					if (obj.get(key) instanceof JSONObject)
						stack.push((JSONObject) obj.get(key));
					else
						if(obj.get(key) instanceof JSONArray)
						{
							for(int i=0;i<((JSONArray)(obj.get(key))).length();i++)
							{
								stack.push(((JSONArray)(obj.get(key))).getJSONObject(i));
							}
						}
						else
						{
							String value = obj.get(key).toString();
							// 	If Key is Already In the Map Than do the Numbering of
							// 	Keys like (status ,status1,status2)
							if (jsonMap.containsKey(key))
							{
								key = key + Integer.toString(count);
								flag = 1;
							}
							jsonMap.put(key, value);
						}
				}
			}
		}
		catch(JSONException e)
		{
			jsonMap = null;
		}
		return jsonMap;
	}
	
	/**
	 * This Function will take JsonObject and a key and Return
	 * Count of Json object in Json array corresponding to specifickey in JSON object return by WebService API. 
	 * @param jobject
	 * @param key
	 * @return
	 * @throws JSONException
	 */
	public static int jsonArrayCount(Object object ,String key) throws JSONException
	{
		JSONObject jobject=(JSONObject)object;
		Iterator<Object> keys = ((JSONObject)jobject).keys();
		int count=-1;
		while(keys.hasNext())
		{
			String jsonkey = (String) keys.next();
			if(jsonkey.equalsIgnoreCase(key))
			{
				count=((JSONArray)jobject.get(jsonkey)).length();
			}
		
		}
		return count;
	}

	
	
	
	/**
	 * Will Execute the API and the return Map<String,String> as response 
	 * If there are more than one Matching key in Map than do numbering them starting with 0
	 * @param testConfig
	 * @param webServiceRow
	 * @param key
	 * @param salt
	 * @return
	 * @throws JSONException
	 */
	public static HashMap<String, Object> executeAPIandReturnResponse(Config testConfig, int webServiceRow, String merchantLoginDetailsRow)
	{
		TestDataReader merchantLoginDetails=testConfig.getCachedTestDataReaderObject("MerchantLoginDetails");
		String key=merchantLoginDetails.GetData(Integer.parseInt(merchantLoginDetailsRow), "key");
		String salt=merchantLoginDetails.GetData(Integer.parseInt(merchantLoginDetailsRow), "salt");
		return (jsonParser(WebService.executePayuProductWebService(testConfig, webServiceRow, key, salt)));
	}
	
	/**
	 * This Function Will execute the WebService
	 *  Return Count of Json object in Json array corresponding to specifickey in JSON object return by WebService API. 
	 * 
	 * @param testConfig
	 * @param webServiceRow
	 * @param merchantLoginDetailsRow
	 * @param specifickey  --key which has value as Json Array 
	 * @return
	 * @throws JSONException
	 */
	public static int executeAPIandVerifyArrayObjectCount(Config testConfig, int webServiceRow, String merchantLoginDetailsRow,String specifickey) throws JSONException
	{
		TestDataReader merchantLoginDetails=testConfig.getCachedTestDataReaderObject("MerchantLoginDetails");
		String key=merchantLoginDetails.GetData(Integer.parseInt(merchantLoginDetailsRow), "key");
		String salt=merchantLoginDetails.GetData(Integer.parseInt(merchantLoginDetailsRow), "salt");
		return (jsonArrayCount((WebService.executePayuProductWebService(testConfig, webServiceRow, key, salt)),specifickey));
	}
	
	/**
	 * This Function Will Excute The Payu product Web Services And Return Object response
	 * @param testConfig
	 * @param webServiceAPI	
	 * 					Enum for different APIs
	 * @param key	
	 * 					Merchant Key		
	 * @param salt
	 * 					Merchant Salt
	 * @param array
	 * 					Variable length Array for variable which use in corresponding API 
	 * @return			
	 * 					JSONObject of webService Response
	 */
	public static Object executePayuProductWebService(Config testConfig,ProductWebServices webServiceAPI,String key,String salt,String ... parameters)
	{
		Object obj=null;
		//String response = null;
		switch (webServiceAPI)
		{
			case verify_payment:
				testConfig.putRunTimeProperty("transactionId",parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.verify_payment.getRowNumber(), key, salt);
				break;
			case check_payment:
				testConfig.putRunTimeProperty("mihpayid",parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.check_payment.getRowNumber(), key, salt);
				break;
				//Using this just Send UserCredentials (Example:-ra:ra )and cardToken  
			case delete_user_card :
				testConfig.putRunTimeProperty("userCredentials", parameters[0]);
				testConfig.putRunTimeProperty("cardToken", parameters[1]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.delete_user_card.getRowNumber(), key, salt);
				break;
			case get_settlement_details :
				testConfig.putRunTimeProperty("date", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.get_settlement_details.getRowNumber(), key, salt);
				break;	
			case get_user_cards :
				testConfig.putRunTimeProperty("user_credentials", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.get_user_cards.getRowNumber(), key, salt);
				break;
			case getAllRefundsFromTxnIds :
				testConfig.putRunTimeProperty("transactionId", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.getAllRefundsFromTxnIds.getRowNumber(), key, salt);
				break;
			case getPaymentGateways :
				testConfig.putRunTimeProperty("transactionId", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.getPaymentGateways.getRowNumber(), key, salt);
				break;
			case refundDateAmount :
				testConfig.putRunTimeProperty("mihpayid", parameters[0]);
				testConfig.putRunTimeProperty("token", parameters[1]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.refundDateAmount.getRowNumber(), key, salt);
				break;
			case validateCardNumber:
				testConfig.putRunTimeProperty("cardno", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.validateCardNumber.getRowNumber(), key, salt);
				break;
			case checkNewActiveMerchants:
				testConfig.putRunTimeProperty("date", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.checkNewActiveMerchants.getRowNumber(), key, salt);
				break;	
			case refundTransaction:
				testConfig.putRunTimeProperty("mihpayid", parameters[0]);
				testConfig.putRunTimeProperty("token", parameters[1]);
				testConfig.putRunTimeProperty("amount", parameters[2]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.refundTransaction.getRowNumber(), key, salt);
				break;	
			case refundStatus:
				testConfig.putRunTimeProperty("transactionId", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.refundStatus.getRowNumber(), key, salt);
				break;
			case updateMerUtr:
				testConfig.putRunTimeProperty("merchantid", parameters[0]);
				testConfig.putRunTimeProperty("utr", parameters[1]);
				testConfig.putRunTimeProperty("txnData", parameters[2]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.updateMerUtr.getRowNumber(), key, salt);
				break;
			case offerSyncingFromPayuMoneyToPayu:
				testConfig.putRunTimeProperty("paisamid", parameters[0]);
				testConfig.putRunTimeProperty("cashback_max_value", parameters[1]);
				testConfig.putRunTimeProperty("cashback_percent", parameters[2]);
				String response= (String) executePayuProductWebService(testConfig, ProductWebServices.offerSyncingFromPayuMoneyToPayu.getRowNumber(), key, salt);
				
				// One additional escape character is coming in the response in order to handle below line of code has written
				/**
				 * "{\"status\":1,\"msg\":\"cashback_percent updated successfully. cashback_max_value updated successfully. 	}"
				 *  {\status\:1,\msg\:\cashback_percent updated successfully. cashback_max_value updated successfully. \}
				 *  {"status":1,"msg":"cashback_percent updated successfully. cashback_max_value updated successfully. "}
				 */
				response = response.replaceAll("\\\"", "");
				response = response.replaceAll("\\\\", "\\\"");
				//converting string to json object for parsing later
				JSONObject jObject = null;
				try {
					jObject = new JSONObject(response);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				obj = jObject;
				break;		
			case getIssuingBankDownBins:
				testConfig.putRunTimeProperty("bankname", parameters[0]);
				testConfig.putRunTimeProperty("getPartialDownIb", parameters[1]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.getIssuingBankDownBins.getRowNumber(), key, salt);
				break;	
			case caseWithPayuidAsChargeBack:
				testConfig.putRunTimeProperty("chargebacktype", parameters[0]);
				testConfig.putRunTimeProperty("payuids", parameters[1]);
				testConfig.putRunTimeProperty("amount", parameters[2]);
				testConfig.putRunTimeProperty("refnum", parameters[3]);
				testConfig.putRunTimeProperty("force", parameters[4]);
				testConfig.putRunTimeProperty("approvalstatus", parameters[5]);
				int rownbr = 0;
				if(parameters[0].equalsIgnoreCase("chargeBack"))
				{
					testConfig.putRunTimeProperty("debittype", parameters[6]);
					rownbr =  ProductWebServices.caseWithPayuidAsChargeBack.getRowNumber();
				}
				else if((parameters[0].equalsIgnoreCase("chargeBackReversal")))
				{
					rownbr = 42;
				}
				else
				{
					testConfig.logFail("Invalid Var 1");
				}
				obj=executePayuProductWebService(testConfig, rownbr, key, salt);
				break;
			case getIssuingBankStatus:
				testConfig.putRunTimeProperty("cardNo", parameters[0]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.getIssuingBankStatus.getRowNumber(), key, salt);
				break;	
				
			case eligibleBinsForEMI:
				testConfig.putRunTimeProperty("bank",parameters[0]);
				testConfig.putRunTimeProperty("bankname",parameters[1]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.getIssuingBankStatus.getRowNumber(), key, salt);
				break;
			case merchantLoginDetails:
				testConfig.putRunTimeProperty("startdate",parameters[0]);
				testConfig.putRunTimeProperty("enddate",parameters[1]);
				obj=executePayuProductWebService(testConfig, ProductWebServices.merchantLoginDetails.getRowNumber(), key, salt);
				break;
			case check_action_status:
				testConfig.putRunTimeProperty("idnumber", parameters[0]);
			 	testConfig.putRunTimeProperty("idname", parameters[1]);
			 	obj=executePayuProductWebService(testConfig, ProductWebServices.check_action_status.getRowNumber(), key, salt);
			 	break;
			default:
				testConfig.logFail("No API Found by this Name");
				break;
		}
		return obj;
	}
	
	/**
	 * Generating hash for given Input String and Return Hash in String Format
	 * 
	 * @param testConfig
	 *            Config Class Object
	 * @param inputString
	 *            Given String
	 * @return
	 */
	public static String GetHash(Config testConfig, String inputString)
	{
		String hash = null;
		try
		{
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] mdbytes = md.digest(inputString.getBytes());
			// convert the byte to hex format method
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < mdbytes.length; i++)
			{
				sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			hash = sb.toString();
			testConfig.logComment("'" + inputString + "' hash is-" + hash);
		}
		catch (Exception e)
		{
			testConfig.logException(e);
		}
		return hash;

	}		
	
	
	}

