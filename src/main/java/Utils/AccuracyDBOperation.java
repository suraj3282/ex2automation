package Utils;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.Icon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import Utils.DataBase.DatabaseType;

/**
 * Utility to update accuracy DB or add new row of known failure test cases in accuracy DB
 * 
 * @author nitish.kumar
 *
 */
public class AccuracyDBOperation extends JFrame implements ActionListener {
	
	String tableName = null;
	String testCaseName = null;
	String failureReason = null;
	
	public enum QueryTypeToExecute
	{
			Update, Insert, Delete
	}
	
	// constructor 
	public AccuracyDBOperation() throws IOException
	{
		// Using a standard Java icon
		Icon optionIcon = UIManager.getIcon("Tree.leafIcon");
		
		// make sure the program exits when the frame closes
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// This will center the JFrame in the middle of the screen
		setLocationRelativeTo(null);
		
		getQueryAndExecuteForAccuracyDB(optionIcon);
	}
	
	/**
	 * Method to get query from CommonTestData - SQL sheet and execute the query 
	 * 
	 * @param optionIcon
	 * @throws IOException
	 */
	private void getQueryAndExecuteForAccuracyDB(Icon optionIcon) throws IOException
	{
	
		String queryType = takeCommonFieldsInputFromUserForProductAndMoney(optionIcon);
		
		if(queryType.equalsIgnoreCase("Insert"))
		{

			// Input dialog with a text field
			failureReason = getFailureReasonFromUser();
			
			// Input dialog with a text field
			String redmineId = JOptionPane.showInputDialog(this, "Enter redmine id ");
			System.out.println("Redmine id is : " + redmineId);
			
			if(redmineId == null)
			{
				System.out.println("User cancelled operation ... so exiting");
				System.exit(0);
			}	
			while(redmineId.equals(""))
			{
				JOptionPane.showMessageDialog(this, "Please enter redmine id, it is mandatory field");
				redmineId = JOptionPane.showInputDialog(this, "Enter redmine id ");
				System.out.println("Redmine Id is : " + redmineId);
			}

			
			// Input dialog with a text field
			String startCounter = JOptionPane.showInputDialog(this, "                  Enter start counter(*integer) (optional(Leave blank for default)).\n "
					+ "'StartCounter is the count of number of characters from where we want to start\n comparing FailureReason."
					+ "If we want to compare full FailureReason then pass it 0' \n");
			System.out.println("Start counter is : " + startCounter);
			
			//Add default value for optional field
			if(startCounter == null || startCounter.isEmpty())
				startCounter = "0";
			
			// Input dialog with a text field
			String endCounter = JOptionPane.showInputDialog(this, "                   Enter end counter(*integer) (optional(Leave blank for default)). \n"
					+ "'EndCounter is the count of number of characters upto which we want to compare\n  the FailureReason."
					+ "If we want to compare full FailureReason then pass it -1' \n");
			System.out.println("End counter is : " + endCounter);
			
			//Add default value for optional field
			if(endCounter == null || endCounter.isEmpty() )
				endCounter = "-1";
	
			insertIntoAccuracyDB(tableName, testCaseName, failureReason, redmineId, startCounter, endCounter);
		}
		
		else if(queryType.equalsIgnoreCase("Update"))
		{

			failureReason = getFailureReasonFromUser();
			
			updateAccuracyDB(tableName, testCaseName, failureReason);
		}
		
		else
		{
			deleteFromAccuracyDB(tableName, testCaseName);
		}
		
		System.exit(0);
	}
	
	/**
	 * Getting failure reason input from the user
	 * 
	 * @return
	 */
	private String getFailureReasonFromUser()
	{
		
		// Input dialog with a text field
		failureReason = JOptionPane.showInputDialog(this, "Enter failure reason for test case ");
		System.out.println("Failure reason entered : " + failureReason);
			
		//In case user cancels operation
		if(failureReason == null)
		{
			System.out.println("User cancelled operation ... so exiting");
			System.exit(0);
		}
						
		while(failureReason.equals(""))
		{
			JOptionPane.showMessageDialog(this, "Please enter failure reason, it is mandatory field");
			failureReason = JOptionPane.showInputDialog(this, "Enter failure reason for test case ");
			System.out.println("Test Case Name is : " + failureReason);
		}
		return failureReason.replaceAll("'", "''");
	}

	
	

	/**
	 * Method to get and execute delete query for accuracy db
	 * 
	 * @param tableName
	 * @param testCaseName
	 * @throws IOException
	 */
	private void deleteFromAccuracyDB(String tableName, String testCaseName) throws IOException
	{
		String query = queryToExecute(QueryTypeToExecute.Delete);
		
		String deleteQueryToExecute = query.replace("tablename",tableName).replace("testcasename", testCaseName);
		System.out.println("Delete Query:  " + deleteQueryToExecute);
	
		//DBHelperForAccuracyDB dbHelperForAccuracyDB = new DBHelperForAccuracyDB();
		//dbHelperForAccuracyDB.executeQuery(deleteQueryToExecute, DatabaseType.AccuracyDB);
	}

	/**
	 * Method to get and execute update query for accuracy db
	 * 
	 * @param tableName
	 * @param testCaseName
	 * @param failureReason
	 * @throws IOException
	 */
	private void updateAccuracyDB(String tableName, String testCaseName, String failureReason) throws IOException 
	{
				
		String query = queryToExecute(QueryTypeToExecute.Update);
		
		String updateQueryToExecute = query.replace("tablename",tableName).replace("testcasename", testCaseName).replace("reasonOfFailure", failureReason);
	
		System.out.println("Update Query:  " + updateQueryToExecute);
		
		//DBHelperForAccuracyDB dbHelperForAccuracyDB = new DBHelperForAccuracyDB();
		//dbHelperForAccuracyDB.executeQuery(updateQueryToExecute, DatabaseType.AccuracyDB);
	}

	/**
	 * Method to get and execute insert query for accuracy db
	 * 
	 * @param tableName
	 * @param testCaseName
	 * @param failureReason
	 * @param redmineId
	 * @param startCounter
	 * @param endCounter
	 * @throws IOException
	 */
	private void insertIntoAccuracyDB(String tableName, String testCaseName,String failureReason, String redmineId, String startCounter,String endCounter) throws IOException
	{
		
		String query = queryToExecute(QueryTypeToExecute.Insert);
		
		String insertQueryToExecute = query.replace("tablename",tableName).replace("testcasename", testCaseName).replace("reasonOfFailure", failureReason).replace("redmineid", redmineId).
				replace("startcounter", startCounter).replace("endcounter", endCounter);
		
		System.out.println("Insert Query: " + insertQueryToExecute);
		
		//DBHelperForAccuracyDB dbHelperForAccuracyDB = new DBHelperForAccuracyDB();
		//dbHelperForAccuracyDB.executeQuery(insertQueryToExecute, DatabaseType.AccuracyDB);
	}
	
	
	/**
	 * create query to execute
	 * @param queryType
	 * @return
	 * @throws IOException
	 */
	private String queryToExecute(QueryTypeToExecute queryType) throws IOException {
		
		String query = null;
		String filePath = System.getProperty("user.dir") + File.separator + "Parameters" + File.separator + "CommonTestData.xls";
		
		System.out.println("Reading file - " + filePath);
		
		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);
		
		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet = workbook.getSheet("SQL");
		
		switch (queryType) {
		case Insert:
			query = sheet.getRow(3).getCell(2).toString();
			break;
		case Update:
			query = sheet.getRow(4).getCell(2).toString();
			break;
		case Delete:
			query = sheet.getRow(5).getCell(2).toString();
			break;
		default:
			break;
		}
		
		return query;
	}


	/**
	 * Getting common data required for insert,delete and update queries
	 * 
	 * @param optionIcon
	 * @return
	 */
	private String takeCommonFieldsInputFromUserForProductAndMoney(Icon optionIcon)
	{
		String queryTypes[] = { "Insert", "Update", "Delete"};
		String[] projectType = { "Product", "Money" };
		String projectName = null;
		
		JComboBox query = new JComboBox(queryTypes);
		query.addActionListener(this);
		String queryType = (String) JOptionPane.showInputDialog(this, "Select action to perform", "Combox Dialog", JOptionPane.QUESTION_MESSAGE, optionIcon, queryTypes, queryTypes[0]);
		System.out.println("Action selected is : " + queryType);
		
		//if user cancels the action, exit him
		if(queryType == null)
		{
			System.out.println("User cancelled operation ... so exiting");
			System.exit(0);
		}
		
		// Combo box of project types
		JComboBox tableList = new JComboBox(projectType);
		tableList.addActionListener(this);
		projectName = (String) JOptionPane.showInputDialog(this, "Select Project Name(Product/Money) ", "ComboBox Dialog", JOptionPane.QUESTION_MESSAGE, optionIcon, projectType, projectType[0]);
		System.out.println("Project selected is : " + projectName);
			
		//if user cancels the action, exit him
		if(projectName == null)
		{
			System.out.println("User cancelled operation ... so exiting");
			System.exit(0);
		}
		
		//Select table to update based on Project name
		if(projectName.equalsIgnoreCase("Product"))
			tableName = "knownissues_product";
		else
			tableName = "knownissues_money";
		
		System.out.println("Table name is :" + tableName);
		
		// Input dialog with a text field
		testCaseName = JOptionPane.showInputDialog(this, "Enter fully qualified name of test case eg : Test.NewMerchantPanel.Analytics.TestFailures");
		System.out.println("Test Case Name is : " + testCaseName);
		
		//if user cancels the action, exit him
		if(testCaseName == null)
		{
			System.out.println("User cancelled operation ... so exiting");
			System.exit(0);
		}	
		while(testCaseName.equals(""))
		{
			JOptionPane.showMessageDialog(this, "Please enter test case name, it is mandatory field");
			testCaseName = JOptionPane.showInputDialog(this, "Enter fully qualified name of test case eg : Test.NewMerchantPanel.Analytics.TestFailures ");
			System.out.println("Test Case Name is : " + testCaseName);
		}
		
		return queryType;
	}

	
	public static void main(String args[]) throws IOException
	{		
		try
		{
			 new AccuracyDBOperation();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
