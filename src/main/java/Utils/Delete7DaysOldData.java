package Utils;

import java.io.File;
import java.sql.Date;
import java.util.concurrent.TimeUnit;

public class Delete7DaysOldData
{
	public static boolean deleteDirectory(File directory, int days)
	{
		try
		{
			if (directory.exists())
			{
				File[] files = directory.listFiles();
				if (null != files)
				{
					for (int i = 0; i < files.length; i++)
					{
						Date lastModified = new Date(files[i].lastModified());
						System.out.println("Last Updation Date of "+files[i].toString()+" is : " + lastModified);
						
						java.util.Calendar cal = java.util.Calendar.getInstance();
						java.util.Date utilDate = cal.getTime();
						java.sql.Date sqlDate = new Date(utilDate.getTime());
						
						long diff = sqlDate.getTime() - lastModified.getTime();
						int difference = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
						
						if (difference >= days)
						{
							deleteDirectory(files[i], days);
							System.out.println("Folder deleted succesfully!!");
						}
						else
							System.out.println("Folder is not older than " + days + " days, so not deleted!!");
					}
				}
			}
			else
			{
				System.out.println("NO folder/directory found at specified Path!!");
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (directory.delete());
	}
	
	public static void main(String args[])
	{
		String directoryUrl = "";// Either Specify your Folder/Directory URL here or pass it as Parameter
		String numberOfDays = "15";
		
		try
		{
			System.out.println("Folder/Directory URL Passed is: " + args[0]);
			if (args[0] != null || !args[0].equals(""))
				directoryUrl = args[0];
			else
				System.out.println("URL Passed is Incorrect!!");
			
			System.out.println("Number of days passed are: " + args[1]);
			if (args[1] != null || !args[1].equals(""))
				numberOfDays = args[1];
			else
				System.out.println("Number of days passed are Incorrect!!");
			
			
		}
		catch (Exception e)
		{
		}
		
		File dir = new File(directoryUrl);
		int days = Integer.parseInt(numberOfDays);
		deleteDirectory(dir, days);
	}
}