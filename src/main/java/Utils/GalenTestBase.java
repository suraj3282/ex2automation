package Utils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;

import net.mindengine.galen.api.Galen;
import net.mindengine.galen.config.GalenConfig;
import net.mindengine.galen.reports.GalenTestInfo;
import net.mindengine.galen.reports.HtmlReportBuilder;
import net.mindengine.galen.reports.TestReport;
import net.mindengine.galen.reports.model.LayoutReport;
import net.mindengine.galen.validation.ValidationResult;

/**
 * Base Class for Galen contains various config values,Reporters , listeners  and checklayout method which will verify spec against page.
 * @author shishir.dwivedi
 *
 */
public class GalenTestBase extends TestBase
{
	// Creating a list of tests
	protected static List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();
	protected static ThreadLocal<TestReport> report = new ThreadLocal<TestReport>();
	
	private static String resultsDir = null;
	
	@BeforeMethod
	public void initReport(Method method)
	{
		GalenTestInfo testInfo = GalenTestInfo.fromMethod(method);
		tests.add(testInfo);
		report.set(testInfo.getReport());
	}
	
	@AfterMethod(alwaysRun = true)
	public void tearDown(ITestResult result)
	{
		tearDownHelper(result,false);
	}
	
	@AfterSuite(alwaysRun = true)
	public void generateGalenReport()
	{
		Reporter.log("GENERTATING GALEN HTML REPORT");
		List<GalenTestInfo> tests = GalenTestBase.tests;
		
			try {
				new HtmlReportBuilder().build(tests,resultsDir);
			} catch (IOException e) {
				e.printStackTrace();
			}
		
		
	}
	/**
	 * This is verification method to verify all given spec with actual layout of the Page
	 * @param testConfig object of Config class
	 * @param specPath path of specification file.
	 * @param tagname name of tag in spec file which you want to check.
	 * @author shishir.dwivedi
	 */
	public void checkLayout(Config testConfig, String specPath,String tagname)
	{   int  totalError=0;
		List<String> tag=new ArrayList<String>();
		if(resultsDir == null) resultsDir = testConfig.getRunTimeProperty("ResultsDir") + "/galen-html-reports";
		if(tagname.equalsIgnoreCase("firefox")) tag.add("firefox");
		else if(tagname.equalsIgnoreCase("chrome")) tag.add("chrome");
		else if(tagname.equalsIgnoreCase("ie")) tag.add("ie");
		else{
			tag.add(tagname.toLowerCase());
		}
		//Setting config value true for screen shot.
		GalenConfig.getConfig().setProperty(GalenConfig.SCREENSHOT_FULLPAGE, "true");
		
		String title = "Check layout " + specPath;
		LayoutReport layoutReport = null;
		try
		{
			layoutReport = Galen.checkLayout(testConfig.driver, specPath, null, null, new Properties(), null);
		}
		catch (IOException e)
		{
			testConfig.logException(e);
		}
		
		report.get().layout(layoutReport, title);
		
		if (layoutReport.errors() > 0)
		{  totalError =layoutReport.errors();
			testConfig.logFail("Incorrect layout: " + title);
			
			List<ValidationResult> validationResult = layoutReport.getValidationErrorResults();
			for (int i = 0; i < validationResult.size(); i++)
			{
				//Logging Error message
				Log.FailWithoutPageInfoLogging("ERROR:" + validationResult.get(i).getError().getMessages().get(0), testConfig);
			}
			testConfig.logFail("TOTAL FAILED SPEC :"+totalError);
		}
		
			
	}
	
}
