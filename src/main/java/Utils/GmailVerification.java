package Utils;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import Utils.Element.How;

public class GmailVerification
{
	
	@FindBy(css = ".adn.ads>div.gs>div:nth-child(6)")
	private WebElement checkEmail;
	
	@FindBy(name = "nvp_bu_tssb")
	private WebElement clickTestSearch;
	
	@FindBy(linkText = "Create a filter")
	private WebElement createFilter;
	
	@FindBy(name = "cf1_has")
	private WebElement hasthewords;
	
	@FindBy(xpath = "//tr[4]/td[2]/a")
	private WebElement invoiceLink;
	
	@FindBy(id = "gbqfb")
	private WebElement searchButton;
	
	@FindBy(css = "div.UJ div.Cp div table tbody tr")
	private WebElement selectEmail;
	
	@FindBy(name = "cf1_subj")
	private WebElement subJect;
	
	private Config testConfig;
	
	public GmailVerification(Config testConfig)
	{
		this.testConfig = testConfig;
		PageFactory.initElements(this.testConfig.driver, this);
		Browser.waitForPageLoad(testConfig, searchButton);
	}
	
	public void FilterEmail(String subject, String has_the_words)
	{
		
		// Creating a filter
		Element.click(testConfig, createFilter, "Create Filter");
		Element.enterData(testConfig, subJect, subject, "Subject");
		Element.enterData(testConfig, hasthewords, has_the_words, "Has the words");
		Element.click(testConfig, clickTestSearch, "Click on Test Search");
	}
	
	public void searchMail(String searchkey)
	{
		Element.enterData(testConfig, Element.getPageElement(testConfig, How.id, "gbqfq"), searchkey + Keys.ENTER, "search for txnid");
		Element.click(testConfig, searchButton, "Click on Search Button");
	}
	
	public void SelectEmail()
	{
		Element.click(testConfig, selectEmail, "Select the Email");
	}
	
	public void selectEmailWithDynamicTxnId(String txnid)
	{
		Element.click(testConfig,Element.getPageElement(testConfig, How.xPath,"(//span[contains(text(),'"+txnid+"')])[last()]" ) , "open email with specified txnid");
	}
	
	public String verifyAndGetEmailContent(String has_the_word)
	{
		String emailContent = checkEmail.getText();
		Helper.compareContains(testConfig, "Has_the_word", has_the_word, emailContent);
		return emailContent;
	}
	
	public void verifyEmailContent(String has_the_word)
	{
		String emailContent = checkEmail.getText();
		Helper.compareContains(testConfig, "Has_the_word", has_the_word, emailContent);
	}
	
	public void verifyEmailText(String subject)
	{
		String emailText = selectEmail.getText();
		Helper.compareContains(testConfig, "Subject", subject, emailText);
	}
}
