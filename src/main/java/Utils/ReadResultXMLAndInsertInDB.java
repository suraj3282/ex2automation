package Utils;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//import Reporting.SendEMail;
import Utils.DataBase.DatabaseType;

/**
 * Code to read the resulted xml files and get the required data to insert into database
 * 
 * @author nitish.kumar
 *
 */
public class ReadResultXMLAndInsertInDB {

	public static enum QueryTypeFor{getKnownFailureCount,insertClassLevelDataInDB}

	/**
	 * Method to read the resulted xml files and get the required data to insert into database
	 * 
	 * @param filePath
	 * @param areaName
	 * @param dbTableName
	 * @param testerName
	 * @param parentJobName
	 * @param parentBuildId
	 */
	public void readFileAndPerformInsertion(String filePath, String areaName, String dbTableName, String testerName, String parentJobName, String parentBuildId)
	{
		try 
		{
			File xmlFileFolderPath = null;
			/*if(SendEMail.executionOnRemoteAddress)
			{
				xmlFileFolderPath = new File("\\\\"+SendEMail.remoteIP + "\\RegressionResults\\"+filePath);
			}
			else if (!SendEMail.osType.startsWith("Window"))
			{
				xmlFileFolderPath = new File("/Volumes/RegressionResults/"+filePath);
			}
			else
			{
				xmlFileFolderPath = new File("C:\\RegressionResults\\"+filePath);
			}*/
			System.out.println("XML file's Folder is " + xmlFileFolderPath);
			String accuracyDbTableName = "";

			if(dbTableName.contains("product"))
			{
				accuracyDbTableName = "knownissues_product";
			}
			else
			{
				accuracyDbTableName = "knownissues_money";
			}

			File[] listOfFiles = xmlFileFolderPath.listFiles();

			for (File file : listOfFiles)
			{
				if (file.isFile()) 
				{
					String knownFailureCount = "";

					String className = file.getName();
					className = className.replace(".xml", "");
					
					//Get xml file path for class name
					String fileNameArray[] = className.split("\\.");
					className = fileNameArray[fileNameArray.length-1].replace("_results", ".java");

					String fullQualifiedClassName = file.getName().replace("_results.xml", "");

					//get SQL query to execute based upon querytype
					String queryToExecute = decideSQLQueryToExecute(QueryTypeFor.getKnownFailureCount);

					queryToExecute = queryToExecute.replace("tableName", accuracyDbTableName).replace("fullQualifiedClassName", fullQualifiedClassName);

					//ResultSet rs = DBHelperForAccuracyDB.executeSelectQuery(queryToExecute, DatabaseType.AccuracyDB);

					//Find known failure for class
					//knownFailureCount = getKnownFailureCountFromAccuracyDB(rs);
					
					
					//Get xml file for which data is required
					File resulXmlFile = new File(xmlFileFolderPath+"//"+file.getName());
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document document = dBuilder.parse(resulXmlFile);

					document.getDocumentElement().normalize();

					NodeList nodeList = document.getElementsByTagName("testsuite");

					for (int temp = 0; temp < nodeList.getLength(); temp++)
					{
						Node node = nodeList.item(temp);

						if (node.getNodeType() == Node.ELEMENT_NODE)
						{
							Element nodeElement = (Element) node;
							String groupName = nodeElement.getAttribute("name");
							String totalTestCaseCount = nodeElement.getAttribute("tests");
							String failureTestCasesCount = nodeElement.getAttribute("failures");
							String skippedTestCasesCount = nodeElement.getAttribute("skipped");
							int passedTestCasesCount = Integer.parseInt(totalTestCaseCount) - Integer.parseInt(skippedTestCasesCount) - Integer.parseInt(failureTestCasesCount);

							DecimalFormat df = new DecimalFormat("###.##");
							double passPer = (passedTestCasesCount * 100.00)/(Integer.parseInt(totalTestCaseCount)) ;

							try{
								passPer = Double.parseDouble(df.format(passPer));
							} catch (NumberFormatException e) {
								//Set default value of 0.00 in case of Number format exception
								passPer = 0.00;
							}
							String accuracyPercent = "";
							double accuracy = ((passedTestCasesCount+Integer.parseInt(knownFailureCount))*100.00)/(Integer.parseInt(totalTestCaseCount));
							
							try{
								accuracyPercent = df.format(accuracy)+"%";

							} catch (NumberFormatException e) {
								//Set default value of 0.00 in case of Number format exception
								accuracyPercent = 0.00+"%";
							}
							
							String query = decideSQLQueryToExecute(QueryTypeFor.insertClassLevelDataInDB);
							query = query.replace("tableName", dbTableName).replace("suiteName", areaName).replace("groupName", groupName).replace("className", fullQualifiedClassName)
									.replace("passCount", String.valueOf(passedTestCasesCount)).replace("skippedCount", skippedTestCasesCount).replace("failedCount", failureTestCasesCount)
									.replace("passPer", String.valueOf(passPer)).replace("areaChange", "").replace("knownFailures", knownFailureCount)
									.replace("accuracy", accuracyPercent).replace("testerName", testerName).replace("buildId", parentBuildId).replace("parentJobName", parentJobName);

							//DBHelperForAccuracyDB.executeQuery(query, DatabaseType.AutomationDB);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * Get known failure count from accuracy db 
	 * 
	 * @param rs
	 * @return
	 */
	private String getKnownFailureCountFromAccuracyDB(ResultSet rs) {

		int count = 0;
		try {

			while(rs.next())
			{
				count++;
			}

		} catch (Exception e) {
			System.out.println("Empty result set.");
			e.printStackTrace();
		}
		return String.valueOf(count);

	}


	/**
	 * Getting query to be executed :- either insert to enter today's result or select previous day results 
	 * 	
	 * @param queryType
	 * @return
	 * @throws IOException
	 */
	public static String decideSQLQueryToExecute(QueryTypeFor queryType) throws IOException {

		String query = null;
		String filePath = "";
		/*if(SendEMail.executionOnRemoteAddress)
		{
			filePath = "\\\\"+SendEMail.remoteIP + "\\Users\\payu\\"+"."+"hudson\\jobs\\InternalExecution\\workspace\\Common" + "\\" + "Parameters\\" + "CommonTestData.xls";
		}
		else
		{
			filePath = System.getenv("JENKINS_HOME") + File.separator +"jobs" + File.separator + "InternalExecution" + File.separator+"workspace" + File.separator+"Common" + File.separator + "Parameters"+ File.separator + "CommonTestData.xls";
		}*/

		InputStream input = new BufferedInputStream(new FileInputStream(filePath));
		POIFSFileSystem fs = new POIFSFileSystem(input);

		HSSFWorkbook workbook = new HSSFWorkbook(fs);
		HSSFSheet sheet = workbook.getSheet("SQL");

		switch (queryType) {
		case insertClassLevelDataInDB:
			query = sheet.getRow(10).getCell(2).toString();
			break;

		case getKnownFailureCount: 
			query = sheet.getRow(11).getCell(2).toString();
			break;

		default:
			break;
		}
		return query;
	}	

}